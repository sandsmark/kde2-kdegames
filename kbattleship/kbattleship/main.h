#ifndef MAIN_H
#define MAIN_H

#include <klocale.h>

static const char *clientName = I18N_NOOP("KBattleship");
static const char *clientVersion = "1.0";
static const char *clientDescription = I18N_NOOP("The KDE Battleship clone");
static const char *protocolVersion = "0.1.0";

#endif
