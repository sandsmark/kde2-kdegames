#define VERSION	     "2.1.0b"
#define LONG_VERSION "2.1.0b (19 November 2001)"
#define COPYLEFT     "(c) 1996-2001, Nicolas Hadacek"
#define EMAIL        "hadacek@kde.org"

#define HOMEPAGE          "http://kmines.sourceforge.net"
#define WORLD_WIDE_HS_URL "http://kmines.sourceforge.net/"
