/* Yo Emacs, this is -*- C++ -*- */
/*
 *   ksame 0.4 - simple Game
 *   Copyright (C) 1997,1998  Marcus Kreutzberger
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#ifndef _HighScoreWIDGET
#define _HighScoreWIDGET

#include <qdialog.h>
#include <qstring.h>

#define HS_MAXENTRY 10

class HighScore {

public:
    HighScore(QWidget *mainWindow);
    ~HighScore();
    void add(int board, int score,int colors);
    void showScore(bool forceNew=false);
protected:
    void add(int board, int score,int colors,const QString &name);
    void loadScore();
    void loadScore(KConfig *cfg);
    void storeScore();

private:
    struct HSEntry {
        int board;
        int score;
        int colors;
        QString name;
    };

    HSEntry hiscore[HS_MAXENTRY];
    int hiscore_used;

    QString playername;

    QDialog *hs_dlg;
    QWidget *m_mainWindow;
};

#endif




