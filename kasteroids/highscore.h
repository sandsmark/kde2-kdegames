/*
 *	--- highscore.h ---
 * Highscore list for kasteroids
 *
 * Written by Krzysztof "Kristoff" Godlewski (krzygod@kki.net.pl)
 * Hmm, I feel bad about it, because this is almost copied from ksame...ah well....
 */

#ifndef __HIGHSCORE_H__
#define __HIGHSCORE_H__

#include <qdialog.h>
#include <qlabel.h>
#include <qlist.h>

#define HS_PLACES 10

class KHighscore;

class HighscoreList : public QDialog
{
  Q_OBJECT
public:
  HighscoreList( QWidget *parent );
  ~HighscoreList();

  void addEntry( int _score, int _level, bool _show );

private slots:
  void slotClose();

signals:
  void dialogClosed();
  
private:
  struct HScore
  {
    QString name;
    int score;
    int level;
  };
  HScore hiscores[HS_PLACES];

  void addEntry( const QString &_name, int _score, int _level );
  void showList();
  void readList();
  /**
   * Kept for compatibility. Reads in the highscore list in the old format,
   * saves it into the new format (using @ref KHighscore) and reads it again.
   * See @ref readList
   **/
  void readOldList();
  void saveList();

  QString playername;
  int places;	// number of places in Hiscore list used
  QLabel *label;

  KHighscore* highscoreTable;
};

#endif	// __HIGHSCORE_H__
