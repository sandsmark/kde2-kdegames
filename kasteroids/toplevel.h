/*
 * KAsteroids - Copyright (c) Martin R. Jones 1997
 *
 * Part of the KDE project
 */

#ifndef __KAST_TOPLEVEL_H__
#define __KAST_TOPLEVEL_H__

#include <kmainwindow.h>
#include <kaccel.h>
#include <qdict.h>
#include <qmap.h>

#include "view.h"
#include "highscore.h"

//#define KA_ENABLE_SOUND

class KALedMeter;
class QLCDNumber;

class KAstTopLevel : public KMainWindow
{
    Q_OBJECT
public:
    KAstTopLevel();
    virtual ~KAstTopLevel();

private:
    void initKAction();
    void playSound( const char *snd );
    void readSoundMapping();
    void doStats();

protected:
    virtual void keyPressEvent( QKeyEvent *event );
    virtual void keyReleaseEvent( QKeyEvent *event );
    virtual void focusInEvent( QFocusEvent *event );
    virtual void focusOutEvent( QFocusEvent *event );

private slots:
    void readSettings(); // it is useful as a slot
    void slotNewGame();
    void slotQuit();

    void slotShipKilled();
    void slotRockHit( int size );
    void slotRocksRemoved();

    void slotUpdateVitals();

    void slotKeyConfig();
    void slotPref();
    void slotShowHighscores();

private:
    KAsteroidsView *view;
    QLCDNumber *scoreLCD;
    QLCDNumber *levelLCD;
    QLCDNumber *shipsLCD;

    QLCDNumber *teleportsLCD;
//    QLCDNumber *bombsLCD;
    QLCDNumber *brakesLCD;
    QLCDNumber *shieldLCD;
    QLCDNumber *shootLCD;
    KALedMeter *powerMeter;

    HighscoreList *highscore;

    bool   sound;
    QDict<QString> soundDict;

    // waiting for user to press Enter to launch a ship
    bool waitShip;
    bool isPaused;

    int shipsRemain;
    int score;
    int level;
    bool showHiscores;

    enum Action { Launch, Thrust, RotateLeft, RotateRight, Shoot, Teleport,
                    Brake, Shield, Pause };

    KAccel *accel;
    QMap<QString,Action> actions;
};

#endif

