/*
 *	--- highscore.cpp ---
 *
 * Written by Krzysztof "Kristoff" Godlewski (krzygod@kki.net.pl)
 *
 */

#include "highscore.h"
#include "highscore.moc"

#include <qlayout.h>
#include <kapp.h>
#include <qpushbutton.h>
#include <qkeycode.h>
#include <qlineedit.h>
#include <kconfig.h>
#include <klocale.h>
#include <khighscore.h>
#include <stdio.h>
#include <stdlib.h>                                                           

HighscoreList::HighscoreList( QWidget *parent )
    : QDialog( parent, "HighscoreList" )
{
  setCaption( kapp->makeStdCaption(i18n("Highscores")) );

  highscoreTable = new KHighscore(this);

  QVBoxLayout *vb = new QVBoxLayout( this, 10 );

  label = new QLabel( this );
  vb->addWidget( label );
  
  QPushButton *bclose = new QPushButton( i18n("&Close"), this );
  vb->addWidget( bclose );
  bclose->setDefault( true );
  bclose->setFocus();
  connect( bclose, SIGNAL( clicked() ), this, SLOT( slotClose() ) );

  if (highscoreTable->hasTable()) {
      readList();
  } else {
      readOldList();
  }
}  // end construcor

HighscoreList::~HighscoreList()
{
}

void HighscoreList::addEntry( int score, int level, bool _show )
{
  if ( score < 100 )
    return;
  if ( places && places == HS_PLACES && score <= hiscores[places - 1].score )
    return;
  // get player name
  QDialog *d = new QDialog( parentWidget(), "get name", true );
  d->setCaption(kapp->makeStdCaption( i18n( "Well done!" )) );

  QVBoxLayout *vb = new QVBoxLayout( d, 10 );

  QLabel *l = new QLabel(
   i18n( "Congratulations!\nYou made it to the highscores list!\nPlease enter your name:\n" ), d );
  vb->addWidget( l );

  QLineEdit *ename = new QLineEdit( d, "ename" );
  vb->addWidget( ename );
  ename->setFocus();
  ename->setText( playername );
  ename->selectAll();

  QPushButton *bok = new QPushButton( i18n( "&OK" ), d );
  vb->addWidget( bok );
  bok->setDefault( true );

  connect( bok, SIGNAL( clicked() ), d, SLOT( accept() ) );
  connect( ename, SIGNAL( returnPressed() ), d, SLOT( accept() ) );

  if ( d->exec() )
  {
    addEntry( ename->text(), score, level );
    if ( _show )
    {
        show();
    }
  }

  delete d;
}

void HighscoreList::addEntry( const QString &name, int score, int level )
{
  int i;

  playername = name.simplifyWhiteSpace();

  if (playername.isEmpty())
    playername = i18n("No Name");

  for ( i = places; i > 0; i-- )
  {
    if ( score <= hiscores[i-1].score )
      break;
    if ( i < HS_PLACES )
      hiscores[i] = hiscores[i-1];
  }

  if ( i < HS_PLACES )
  {
    hiscores[i].name = playername;
    hiscores[i].score = score;
    hiscores[i].level = level;
    if ( places <  HS_PLACES )
      places++;
    saveList();
    showList();
  }
}

void HighscoreList::showList()
{
    QString text = "<qt><table><tr><td width=50%><b>" + i18n("Name") +
                 "</b></td><td width=30%><b>" + i18n("Score") +
                 "</b></td><td width=20%><b>" + i18n("Level") +
                 "</b></td></tr>";

    for ( int x = 0; x < places; x ++ )
    {
        text += "<tr><td>" + hiscores[x].name + "</td><td>" +
                QString::number(hiscores[x].score) + "</td><td>" +
                QString::number(hiscores[x].level) + "</td></tr>";
    }

    text += "</table></qt>";
    label->setText( text );
    label->setMinimumSize( label->sizeHint() );
    adjustSize();
}

void HighscoreList::readList()
{
    QString score_id("Score");
    QString level_id("Level");
    QString name_id("Name");

    if (highscoreTable)
    {
        places = 0;
        for ( int x = 0; x < HS_PLACES; x ++ )
	{
            if( ! highscoreTable->hasEntry(x, score_id ) )
                break;

            hiscores[x].score = highscoreTable->readNumEntry(x, score_id);
            hiscores[x].level = highscoreTable->readNumEntry(x, level_id);
            hiscores[x].name  = highscoreTable->readEntry(x, name_id);

            places++;
	}
    }

    showList();
}

void HighscoreList::readOldList()
{
// read the old format, save in new format and re-read the list
    KConfigBase *conf = kapp->config();
    QString score_id("Score_%1");
    QString level_id("Level_%1");
    QString name_id("Name_%1");

    if ( conf )
    {
        conf->setGroup( "Highscores" );
        places = 0;

        for ( int x = 0; x < HS_PLACES; x ++ )
        {
            if( ! conf->hasKey(score_id.arg(x) ) )
                break;

            hiscores[x].score = conf->readNumEntry(score_id.arg(x));
            hiscores[x].level = conf->readNumEntry(level_id.arg(x));
            hiscores[x].name  = conf->readEntry(name_id.arg(x));

            places++;
        }
    }
    
    // saves the list in the new format
    saveList();

    // re-read the list from the new format
    readList();
}

void HighscoreList::saveList()
{
  QString score_id("Score");
  QString level_id("Level");
  QString name_id("Name");

  QString str, val;

  if ( highscoreTable )
  {
    for( int x = 0; x < places; x++ )
    {
      highscoreTable->writeEntry(x, score_id, hiscores[x].score);
      highscoreTable->writeEntry(x, level_id, hiscores[x].level);
      highscoreTable->writeEntry(x, name_id, hiscores[x].name);
    }
  }
  highscoreTable->sync();

  if ( kapp->config() )
  {
    kapp->config()->setGroup( "Highscores" );
    kapp->config()->writeEntry( "lastName", playername );
    kapp->config()->sync();
  }
}

void HighscoreList::slotClose()
{
  accept();
  emit dialogClosed();
}
