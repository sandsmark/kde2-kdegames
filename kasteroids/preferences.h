/*
 *	--- preferences.h ---
 * Game preferences dialog for kasteroids
 *
 * Written by Krzysztof "Kristoff" Godlewski (krzygod@kki.net.pl)
 */

#ifndef __PREFERENCES_H__
#define __PREFERENCES_H__

#include <qdialog.h>
#include <qlabel.h>
#include <qlist.h>
#include <qcheckbox.h>
#include <qspinbox.h>

class PrefDialog : public QDialog
{
  Q_OBJECT

public:
  PrefDialog( QWidget *parent );
  virtual ~PrefDialog();

  void readConfig();

private slots:
  void slotOk();

private:
  QSpinBox *spinShips;
  QList<QCheckBox> *checkBoxes;
};

#endif	// __PREFERENCES_H__

