/*
 *	--- preferences.cpp ---
 * 
 * Written by Krzysztof "Kristoff" Godlewski (krzygod@kki.net.pl)
 */

#include <qlayout.h>
#include <qpushbutton.h>
#include <kconfig.h>
#include <klocale.h>
#include <kapp.h>

#include "preferences.h"
#include "preferences.moc"

PrefDialog::PrefDialog( QWidget *parent )
    : QDialog( parent, "PrefDialog", true )
{
  setCaption( i18n("KAsteroids Preferences") );

  QVBoxLayout *vb = new QVBoxLayout( this, 10 );
  QHBoxLayout *hb = new QHBoxLayout( vb );
  QLabel *label = new QLabel( i18n("Start new game with"), this );
  hb->addWidget( label );
  spinShips = new QSpinBox( 1, 5, 1, this );
  hb->addWidget( spinShips );
  label = new QLabel( i18n("ships"), this );
  hb->addWidget( label );

  checkBoxes = new QList<QCheckBox>;
  for( int i = 0; i < 2; i++ )
  {
    QCheckBox *cb = new QCheckBox( this );
    checkBoxes->append( cb );
    vb->addWidget( cb );
  }

  checkBoxes->at(0)->setText( i18n("Show highscores on Game Over") );
  checkBoxes->at(1)->setText( i18n("Player can destroy Powerups") );

  hb = new QHBoxLayout( vb );

  QPushButton *b = new QPushButton( i18n("&OK"), this );
  b->setDefault( true );
  b->setFocus();
  connect( b, SIGNAL( clicked() ), this, SLOT( slotOk() ) );
  hb->addWidget( b );

  b = new QPushButton( i18n("&Cancel"), this );
  connect( b, SIGNAL( clicked() ), this, SLOT( reject() ) );
  hb->addWidget( b );

  readConfig();
}	// --- end constructor ---

PrefDialog::~PrefDialog()
{
}

void PrefDialog::readConfig()
{
  KConfigBase *conf = kapp->config();

  if( conf )
  {
    conf->setGroup( "Preferences" );

    spinShips->setValue( conf->readNumEntry( "numShips", 3 ) );

    checkBoxes->at(0)->setChecked( conf->readBoolEntry( "showHiscores" , false ) );
    checkBoxes->at(1)->setChecked( conf->readBoolEntry( "canDestroyPowerups", true ) );
  }
}

void PrefDialog::slotOk()
{
  KConfigBase *conf = kapp->config();

  if( conf )
  {
    conf->setGroup( "Preferences" );
    conf->writeEntry( "numShips", spinShips->value() );
    conf->writeEntry( "showHiscores", checkBoxes->at(0)->isChecked() );
    conf->writeEntry( "canDestroyPowerups", checkBoxes->at(1)->isChecked() );

    conf->sync();

    accept();
  }
}

