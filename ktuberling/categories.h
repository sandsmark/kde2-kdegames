/* -------------------------------------------------------------
   KDE Tuberling
   Categories of texts and sounds used in the "layout.txt" file
   mailto:e.bischoff@noos.fr
 ------------------------------------------------------------- */

#ifndef _CATEGORIES_H_
#define _CATEGORIES_H_

#define TEXT_EYES	0
#define TEXT_EYEBROWS	1
#define TEXT_NOSES	2
#define TEXT_EARS	3
#define TEXT_MOUTHS	4
#define TEXT_GOODIES	5

#define SOUND_TUBERLING	0
#define SOUND_EYE	1
#define SOUND_EYEBROW	2
#define SOUND_NOSE	3
#define SOUND_EAR	4
#define SOUND_MOUTH	5
#define SOUND_HAT	6
#define SOUND_MOUSTACHE	7
#define SOUND_CIGAR	8
#define SOUND_TIE	9
#define SOUND_SUNGLASSES 10
#define SOUND_SPECTACLES 11
#define SOUND_BOW	12
#define SOUND_EARRING	13
#define SOUND_BADGE	14
#define SOUND_WATCH	15

#endif

