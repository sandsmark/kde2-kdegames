/* Class AbTop: the toplevel widget of KAbalone
 * 
 * Josef Weidendorfer, 9/97
*/

#ifndef _ABTOP_H_
#define _ABTOP_H_

#include <qobject.h>
#include <qstrlist.h>

#include <kmainwindow.h>

#include "Move.h"


class QTimer;
class QPopupMenu;

class Network;
class Board;
class BoardWidget;
class Move;
class EvalScheme;

#ifdef SPION
class Spy;
#endif



class AbTop: public KMainWindow
{
  Q_OBJECT

public:
  AbTop();
  ~AbTop();

  /* timer states */
  enum { noGame, gameOver, notStarted, moveShown,
	 showMove = 100, showSugg=200
  };

  void netPort(int p) { myPort = p; }
  void netHost(char* h) { hosts.append(h); }
	
protected:
  virtual void closeEvent (QCloseEvent *);

  virtual void saveProperties( KConfig * );
  virtual void readProperties( KConfig * );


public slots:  
  void timerDone();
  void newGame();
  void copy();
  void paste();
  void pastePosition(const char *);
  void stopGame();
  void continueGame();
  void back();
  void forward();
  void suggestion();
  void easy();
  void stopSearch();
  void normal();
  void hard();
  void challange();
  void help();
  void quit();
  void searchBreak();
  void moveChoosen(Move&);
  void play_red();
  void play_yellow();
  void play_both();
  void play_none();
  void savePosition();
  void restorePosition();
  void setSpy(int);
  void updateSpy(QString);
  void edited(int);
  void updateBestMove(Move&,int);
  void readConfig();
  void writeConfig();
  void rightButtonPressed(int,const QPoint&);

  void gameNetwork(bool);
  void editModify(bool);
  void optionMoveSlow(bool);
  void optionRenderBalls(bool);
  void optionShowSpy(bool);
  void toggleMenubar();
  void toggleToolbar();
  void toggleStatusbar();
  void configure();
  void configureKeys();
  void configureToolbars();
  void levelEasy(bool);
  void levelNormal(bool);
  void levelHard(bool);
  void levelChallange(bool);
  void iplayRed(bool);
  void iplayYellow(bool);
  void iplayBoth(bool);
  void iplayNone(bool);

private:
  void setupActions();
  void setupRMBMenu();
  void updateStatus();
  void userMove();
  void playGame();
  void loadPixmaps();
  void setupStatusBar();
  void updateActions();
  void setLevel(int);
  void setMoveNo(int, bool updateGUI = false);
  void setIplay(int);
  bool iPlayNow();
  Move haveHint();
  void readOptions(KConfig *);
  void writeOptions(KConfig *);

  Move actMove;
  Board* board;
  int actValue;
  BoardWidget *boardWidget;
  EvalScheme* currentEvalScheme;
  QTimer *timer;
  int timerState;
  int depth, moveNo;
  bool showMoveLong, stop, showSpy;
  bool editMode, renderBalls;
  int spyLevel;
  bool pastePossible, validShown;

  int iplay;
  enum { none, cYellow, cRed, cBoth, cNone };

  QPopupMenu *rmbMenu;

  int stop_id, back_id, hint_id;
  int easy_id, normal_id, hard_id, challange_id, slow_id, level_id;
  int render_id;
  int yellow_id, red_id, both_id, none_id, iplay_id;
  int spy_id, paste_id, edit_id, forward_id, net_id;

  QLabel *validLabel, *ballLabel, *moveLabel, *statusLabel;
  QPixmap warningPix, okPix, redBall, yellowBall, noBall, netPix;
  QPixmap spy0, spy1, spy2, spy3;

  Network *net;
  int myPort;
  QStrList hosts;

#ifdef SPION	
  Spy* spy;
#endif

};

#endif /* _ABTOP_H_ */
