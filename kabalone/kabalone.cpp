/* Start point for KAbablone */

#include <qobject.h>

#include <kapp.h>
#include <klocale.h>
#include <kcmdlineargs.h>

#include <kaboutdata.h>

#include "version.h"
#include "AbTop.h"


static const char *description = 
	I18N_NOOP("KDE Implementation of Abalone");

static KCmdLineOptions options[] =
{
  { "h", 0, 0},
  { "host <host>", I18N_NOOP("Use 'host' for network game"), 0 },
  { "p", 0, 0},
  { "port <port>", I18N_NOOP("Use 'port' for network game"), 0 },
  { 0, 0, 0 }
};

int main(int argc, char *argv[])
{
	KAboutData aboutData( "kabalone", I18N_NOOP("KAbalone"), 
		KABALONE_VERSION, description, KAboutData::License_GPL, 
		"(c) 1997-2000, Josef Weidendorfer");
	aboutData.addAuthor("Josef Weidendorfer",0, "Josef.Weidendorfer@in.tum.de");
	aboutData.addAuthor("Robert Williams");
	KCmdLineArgs::init( argc, argv, &aboutData );
	KCmdLineArgs::addCmdLineOptions( options ); // Add our own options.

	KApplication app;
	AbTop* top = new AbTop;
	int res;

	/* command line handling */
	KCmdLineArgs *args = KCmdLineArgs::parsedArgs();

	if (args->isSet("port"))
	    top->netPort( args->getOption("port").toInt() );
	if (args->isSet("host"))
	    top->netHost( args->getOption("host").data() );
	args->clear();

	/* session management */
	if (app.isRestored())
	  top->restore(1);
	else
	  top->readConfig();

	app.setMainWidget(top);
	top->show();	
	res = app.exec();

	delete top;
	return res;
}

