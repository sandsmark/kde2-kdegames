#include <qlcdnumber.h>

int BRICKSIZE = 16;
int MAPWIDTH = BRICKSIZE * 35;
int MAPHEIGHT = MAPWIDTH;

#include "trys.h"
#include "progress.h"
#include "view.h"
#include "rattler.h"

View::View( QWidget *parent, const char *name )
        : QWidget( parent, name )
{
    lcd  = new QLCDNumber( this);
    lcd->setFrameStyle( QFrame::Panel | QFrame::Sunken );

    trys = new Trys(this);
    pg = new Progress(this);
    rattler = new Rattler( this);
    resize(540, 620); //Adjusted, was cut-off.
}

void View::resizeEvent( QResizeEvent * )
{
    BRICKSIZE= (int)16* ((width() < height() - 56) ? width() : height() - 56) / 560;
    MAPWIDTH=BRICKSIZE * BoardWidth;
    MAPHEIGHT=MAPWIDTH;
    lcd->setGeometry(MAPWIDTH-85, 5, 85, 32);
    trys->setGeometry(0, 0, MAPWIDTH-85, 40);
    pg->setGeometry(5, 42, MAPWIDTH-5, 12);
    rattler->setGeometry(0, 56, this->width(), this->height()-56);

}

#include "view.moc"
