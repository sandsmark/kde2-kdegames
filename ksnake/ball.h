#ifndef BALL_H
#define BALL_H

#include <qpixmap.h>
#include <qwidget.h>

#include "board.h"
#include "pixServer.h"

class Ball {
public:
    Ball(Board *b, PixServer *p);
	virtual ~Ball() {};
	virtual void nextMove();
    void repaint();
    void zero();
protected:
    Board   *board;
    PixServer *pixServer;
    int  index;
    int  hold;
    int  next;
};

class KillerBall : public Ball {
public:
	KillerBall(Board *b, PixServer *p) : Ball(b, p) {};
	virtual ~KillerBall() {};
	virtual void nextMove();
};

class DumbKillerBall : public Ball {
public:
	DumbKillerBall(Board *b, PixServer *p) : Ball(b, p) {};
	virtual ~DumbKillerBall() {};
	virtual void nextMove();
};

#endif // BALL_H
