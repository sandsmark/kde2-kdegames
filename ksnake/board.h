#ifndef BOARD_H
#define BOARD_H

#include <qarray.h>
#include <qrect.h>

enum Square {empty, brick, Apple, Balle, snake, head};

#define N  0
#define S  1
#define E  2
#define W  3
#define NE 4
#define SE 5
#define NW 6
#define SW 7

#define BoardWidth 35
extern int BRICKSIZE;
extern int MAPWIDTH;
extern int MAPHEIGHT;
#define OUT -1

typedef int Gate;
const int NORTH_GATE = BoardWidth/2;
const int SOUTH_GATE =(BoardWidth*BoardWidth)-(NORTH_GATE+1);

class Pos {
public:
	Pos(Pos *p, int i, int r);
	~Pos();
	int index() const;
	void setPrice(int p);
	int price() const;
	void setParent(Pos *p);
	Pos *parent() const;
	Pos *listNext();
	void addBTree(Pos *np);
	Pos *searchBTree(int i);
	void addFList(Pos *);
	void addList(Pos *np);
private:
	int _index;
	int _price; // Price to reach this position.
	Pos *_parent; // Way to reach destination.
	Pos *next; // Single linked list.
	Pos *fnext; // Link all Pos instances, so we can delete them all.
	Pos *left, *right; // BTree.
	bool inList;
};

class Board : public QArray<int>
{
public:
  Board  (int s);
  ~Board() {};
  QRect  rect(int i);

  void   set(int i, Square sq);

  bool   isBrick(int i);
  bool   isEmpty(int i);
  bool   isApple(int i);
  bool   isHead(int i);
  bool   isSnake(int i);

  int    getNext(int, int);
	int    getNextCloseToDumb(int, int);
	int    getNextCloseTo(int, int, bool, int = -1);
	int    direction(int, int);
	
	int    samyHeadIndex() const {
		return(samyIndex);
	}

private:
  void   index(int i);
  bool   inBounds(int i);
  int    row;
  int    col;
  int    sz;

	int    samyIndex;
};

#endif // BOARD_H
