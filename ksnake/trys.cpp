#include <qpixmap.h>
#include <qstring.h>
#include <kglobal.h>
#include <kiconloader.h>

#include "trys.h"


Trys::Trys( QWidget *parent, const char *name )
        : QWidget( parent, name )
{
  pixmap = BarIcon("samy");
  snakes = 2;
  repaint();
}

void Trys::paintEvent( QPaintEvent *)
{
  for ( int x = 0; x < snakes; x++)
      bitBlt(this, 10+ ( (pixmap.width()+16)*x), 2,
	     &pixmap, 0, 0, pixmap.width(), pixmap.height());
}

void Trys::set(int i)
{
  snakes = i;
  repaint();
}

#include "trys.moc"
