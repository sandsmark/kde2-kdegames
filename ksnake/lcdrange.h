/****************************************************************
**
** Definition of LCDRange class, Qt tutorial 14
**
****************************************************************/

#ifndef LCDRANGE_H
#define LCDRANGE_H

#include <qwidget.h>

class QScrollBar;
class QLCDNumber;
class QLabel;


class LCDRange : public QWidget
{
    Q_OBJECT
public:
    LCDRange( QWidget *parent=0, const char *name=0 );
    LCDRange( const QString &s, QWidget *parent=0, const char *name=0 );

    int         value() const;
    QString text()  const;
public slots:
    void setValue( int );
    void setRange( int minVal, int maxVal );
    void setText( const QString & );
signals:
    void valueChanged( int );
protected:
    void resizeEvent( QResizeEvent * );
private:
    void init();

    QScrollBar  *sBar;
    QLCDNumber  *lcd;
    QLabel      *label;
};

#endif // LCDRANGE_H
