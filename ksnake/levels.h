#ifndef LEVELS_H
#define LEVELS_H

#include <qfile.h>
#include <qstring.h>
#include <qstringlist.h>

#include <qpixmap.h>
#include <qimage.h>



class Levels
{
public:
    Levels ();
    QImage  getImage(int);
    QPixmap getPixmap(int);
    int max();
private:
    QStringList list;
};


extern Levels *leV;

#endif // LEVELS_H
