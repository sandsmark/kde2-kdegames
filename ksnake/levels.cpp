#include <qstring.h>
#include <qstringlist.h>
#include <qpixmap.h>
#include <qimage.h>
#include <qbitmap.h>

#include <kapp.h>
#include <kglobal.h>
#include <kstddirs.h>

#include "levels.h"

Levels *leV = 0;

Levels::Levels()
{
    leV = this;

    list = KGlobal::dirs()->findAllResources("appdata", "levels/*");    

    list.prepend( "dummy" );
}

int Levels::max()
{
    return ( list.count() -1 );
}

QImage Levels::getImage(int at)
{
    QBitmap bitmap(*list.at(at));
    QImage image = bitmap.convertToImage();
    return image;
}

QPixmap Levels::getPixmap(int at)
{
    QPixmap pixmap(*list.at(at));
    return pixmap;
}
