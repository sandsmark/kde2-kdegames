#ifndef GAME_H
#define GAME_H

#include <kmenubar.h>
#include <qpopupmenu.h>
#include <qlcdnumber.h>

#include <qlist.h>
#include <qfileinfo.h>

#include <kmainwindow.h>

#include "rattler.h"
#include "trys.h"
#include "score.h"
#include "progress.h"
#include "levels.h"

class KAction;
class KSelectAction;
class KRadioAction;

class Game : public KMainWindow
  {
    Q_OBJECT
public:
    Game();
    ~Game() {}
private slots:
    void ballsChecked();
	void ballsAIChecked();
    void skillChecked();
    void snakesChecked();
	void snakesAIChecked();
    void pixChecked();

    void newGame();
    void pauseGame();
    void togglePaused();
    void quitGame();

    void backgroundColor();
    void confKeys();
	void confKeys2();

    void showHighScores();
    void startingRoom();

private:

    Rattler *rattler;
    QLCDNumber *lcd;

    Trys *trys;
    Score   *score;
    Progress *pg;
    Levels *levels;

    KConfig *conf;

    void menu();
    void checkMenuItems();
    void lookupBackgroundPixmaps();

    KAction *pause;
    KSelectAction *balls;
	KSelectAction *ballsAI;
    KSelectAction *snakes;
	KSelectAction *snakesAI;
    KRadioAction *skills[4];
    KSelectAction *pix;

    QStringList backgroundPixmaps;
};

#endif // GAME_H
