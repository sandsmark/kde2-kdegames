#include <qpushbutton.h>
#include <qlabel.h>
#include <qframe.h>
#include <qkeycode.h>
#include <qpixmap.h>
#include <qstring.h>

#include <kaccel.h>
#include <kapp.h>
#include <kglobal.h>
#include <kiconloader.h>

#include "keys.h"
#include <klocale.h>
#include <kconfig.h>

Keys::Keys( QWidget *parent, const char *name, int player)
    : QDialog( parent, name, TRUE )
{
	playerNum = player;

    QPushButton *okButton  = new QPushButton(this);
    okButton->setText(i18n("OK"));
    okButton->setFixedSize(okButton->size());
    connect( okButton, SIGNAL(clicked()),this, SLOT(ok()) );
    okButton->move(20,210);

    QPushButton *defaultButton  = new QPushButton(this);
    defaultButton->setText(i18n("Defaults"));
    defaultButton->setFixedSize(defaultButton->size());
    connect( defaultButton, SIGNAL(clicked()),this, SLOT(defaults()) );
    defaultButton->move(140,210);

    QPushButton *cancelButton  = new QPushButton(this);
    cancelButton->setText(i18n("Cancel"));
    cancelButton->setFixedSize(cancelButton->size());
    connect( cancelButton, SIGNAL(clicked()),this, SLOT(reject()) );
    cancelButton->move(260,210);

    QFrame *separator = new QFrame(this);
    separator->setFrameStyle( QFrame::HLine | QFrame::Sunken );
    separator->setGeometry( 20, 190, 340, 4 );

    for ( int x = 0; x < 4; x++) {
	labels[x] = new QLabel(this);
	labels[x]->setAlignment(AlignCenter);
    }

    labels[0]->setGeometry(120, 20, 140, 20 );
    labels[1]->setGeometry(120,160, 140, 20 );
    labels[2]->setGeometry( 20, 92, 100, 20 );
    labels[3]->setGeometry(265, 92, 100, 20 );


    QPushButton *up = new QPushButton(this);
    up->setPixmap( BarIcon("up"));
    up->adjustSize();
    up->setFixedSize(up->size() );
    connect( up, SIGNAL(clicked()),this, SLOT(butUp()) );
    up->move(180, 50);

    QPushButton *down = new QPushButton(this);
    down->setPixmap( BarIcon("down"));
    down->adjustSize();
    down->setFixedSize(down->size() );
    connect( down, SIGNAL(clicked()),this, SLOT(butDown()) );
    down->move(180, 130);

    QPushButton *left = new QPushButton(this);
    left->setPixmap( BarIcon("back"));
    left->adjustSize();
    left->setFixedSize(left->size() );
    connect( left, SIGNAL(clicked()),this, SLOT(butLeft()) );
    left->move(140, 90);

    QPushButton *right = new QPushButton(this);
    right->setPixmap( BarIcon("forward"));
    right->adjustSize();
    right->setFixedSize(right->size() );
    connect( right, SIGNAL(clicked()),this, SLOT(butRight()) );
    right->move(220, 90);


    setCaption(i18n("Change Direction Keys"));
    setFixedSize(380, 260);
    lab = 0;
    init();
}

void Keys::keyPressEvent( QKeyEvent *e )
{
	uint kCode = e->key() & ~(SHIFT | CTRL | ALT);
	QString string = KAccel::keyToString(kCode, true);

	if (lab != 0) {
	    if ( string.isNull() )
		lab->setText(i18n("Undefined key"));
	    else lab->setText(string);
	}
	else if ( lab == 0 && e->key() == Key_Escape)
	    reject();
}

void Keys::butUp()
{
    getKey(0);
}

void Keys::butDown()
{
    getKey(1);
}

void Keys::butLeft()
{
    getKey(2);
}

void Keys::butRight()
{
    getKey(3);
}

void Keys::getKey(int i)
{
    if ( lab != 0)
	focusOut(lab);

    focusIn(labels[i]);
}

void Keys::focusOut(QLabel *l)
{
    l->setFrameStyle( QFrame::NoFrame );
    l->setBackgroundColor(backgroundColor());
    l->repaint();
}

void Keys::focusIn(QLabel *l)
{
    lab = l;
    lab->setFrameStyle( QFrame::Panel | QFrame::Sunken );
    lab->setBackgroundColor(white);
    lab->repaint();
}

void Keys::defaults()
{
    if ( lab != 0)
	focusOut(lab);

    lab = 0;

    labels[0]->setText(i18n("Up"));
    labels[1]->setText(i18n("Down"));
    labels[2]->setText(i18n("Left"));
    labels[3]->setText(i18n("Right"));
}

void Keys::init()
{
    KConfig *conf = kapp->config();
    if(conf != NULL) {
		QString up = conf->readEntry(QString("upKey") + QString::number(playerNum), i18n("Up"));
	labels[0]->setText(up);

		QString down = conf->readEntry(QString("downKey") + QString::number(playerNum), i18n("Down"));
	labels[1]->setText(down);

		QString left = conf->readEntry(QString("leftKey") + QString::number(playerNum), i18n("Left"));
	labels[2]->setText(left);

		QString right = conf->readEntry(QString("rightKey") + QString::number(playerNum), i18n("Right"));
	labels[3]->setText(right);
    }
}

void Keys::ok()
{
    KConfig *conf = kapp->config();
    if(conf != NULL) {
		conf->writeEntry(QString("upKey") + QString::number(playerNum),   labels[0]->text() );
		conf->writeEntry(QString("downKey") + QString::number(playerNum), labels[1]->text() );
		conf->writeEntry(QString("leftKey") + QString::number(playerNum), labels[2]->text() );
		conf->writeEntry(QString("rightKey") + QString::number(playerNum),labels[3]->text() );
    }

    accept();
}

#include "keys.moc"
