.TH KMAHJONGG 6
.SH NAME
kmahjongg \- Mahjongg game for KDE
.SH SYNOPSIS
.B kmahjongg
.I [Qt-options] [KDE-options]
.SH DESCRIPTION
Your mission in this game is to remove all tiles from the game
board. A matching pair of tiles can be removed, if they are `free',
which means that no other tiles block them on the left or right side.
.so kdeopt.man

