/* Yo Emacs, this -*- C++ -*-

  Copyright (C) 1999,2000 Jens Hoefkens
  jens@hoefkens.com

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
  
*/

#ifndef __KBGINVITE_H
#define __KBGINVITE_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif 

#include <qdialog.h>
#include <qspinbox.h>
#include <qpushbutton.h>
#include <qstring.h>
#include <klineedit.h>


/** 
 * Simple dialog that allows to invite somebody.
 */
class KBgInvite : public QDialog
{
	Q_OBJECT
	
public:
	/**
	 * Constructor and destructor
	 */
	KBgInvite(const char *name = 0);
	virtual ~KBgInvite();
	
protected:
	/**
	 * Entry field, spin bix and three buttons are childs
	 */
	KLineEdit    *le;
	QSpinBox     *sb;

	QPushButton  *invite, *resume, *unlimited, *cancel, *close;
	
public slots:
        
        /**
	 * After hiding, we tell our creator that we are ready to die.
	 */
        virtual void hide();

	void setPlayer(const QString &name);

protected slots:

	/**
	 * Emits the FIBS command if the Ok button was clicked.
	 */
	void inviteClicked();
	void resumeClicked();
	void unlimitedClicked();
	void cancelClicked();
	
signals:
	/**
	 * Emits the text of an invitation
	 */
	void inviteCommand(const QString &cmd);

	/**
	 * Delete the dialog after it is closed.
	 */
	void dialogDone();
};

#endif // __KBGINVITE_H
