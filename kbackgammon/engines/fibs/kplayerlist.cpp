/* Yo Emacs, this -*- C++ -*-

  Copyright (C) 1999-2001 Jens Hoefkens
  jens@hoefkens.com

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  $Id: kplayerlist.cpp 192400 2002-12-06 12:26:32Z lunakl $
  
*/

#include "kplayerlist.moc"
#include "kplayerlist.h"

#include <qlayout.h>
#include <qiconset.h>
#include <qgroupbox.h>
#include <qpopupmenu.h>
#include <qcheckbox.h>
#include <qlistview.h>
#include <qwhatsthis.h>
#include <qdatetime.h>

#include <kapp.h>
#include <kconfig.h>
#include <kaction.h>
#include <kstdaction.h>
#include <ktabctl.h>
#include <kiconloader.h>
#include <kdebug.h>

#include <string.h>
#include <stdio.h>

#include "kbgfibs.h"
#include "version.h"


/*
 * Simple container for information on columns of the list view.
 *
 * index : the internal index in the list
 * width : width of the column in pixel
 * show  : whether the column is visible
 * cb    : check box for the setup dialog 
 */
struct columnInfo 
{
	int index;
	int width;
	bool show;
	QCheckBox *cb;
};

/*
 * Extension of the QListViewItem class that has a custom key function
 * that can deal with the different items of the player list.
 */
class KFibsPlayerListLVI : public QListViewItem
{

public:	
	/*
	 * Constructor
	 */
	KFibsPlayerListLVI(KFibsPlayerList *parent) : QListViewItem(parent) { _plist = parent; }

	/*
	 * Destructor
	 */
	virtual ~KFibsPlayerListLVI() {}

private:

	KFibsPlayerList *_plist;

public:	
	
	/*
	 * Overloaded key function for sorting
	 */
	virtual QString key(int col, bool) const
	{
		int real_col = _plist->cIndex(col);

		QString s = text(real_col);

		switch (real_col) {
		case KFibsPlayerList::Player:
		case KFibsPlayerList::Opponent:
		case KFibsPlayerList::Watches:
		case KFibsPlayerList::Client:
		case KFibsPlayerList::Email:
		case KFibsPlayerList::Status:
		case KFibsPlayerList::Host:
			s = s.lower();
			break;
		case KFibsPlayerList::Idle:
		case KFibsPlayerList::Experience:
			s.sprintf("%08d", s.toInt());
			break;
		case KFibsPlayerList::Rating:
			s.sprintf("%08d", (int)(1000*s.toDouble()));
			break;
		case KFibsPlayerList::Time:
			s = s.lower();
			break;
		default:
			kdDebug(10500) << "KFibsPlayerListLVI::key(): illegal column" << endl;
			break;
		}
		return s;
	}
};


// ----------------------------------------------------------------------------------------------------
// from here down to the constructor the code has to be fixed - everything else is kind of done
// ----------------------------------------------------------------------------------------------------


/*
 * Request information about the user selected user
 */
void KFibsPlayerList::getPlayerInfo(QListViewItem *i, const QPoint &, int col)
{
	// __JENS__ what if 1 and 2 are not players...
	if (col < 0 || col > 2 || i->text(col).isEmpty())
		col = 0;
	emit fibsCommand("whois " + i->text(col));
}

/*
 * Setup dialog page of the player list - allow the user to select the 
 * columns to show
 */
void KFibsPlayerList::getSetupPages(KTabCtl *nb, int space)
{
	/*
	 * Main Widget
	 */
	QWidget *w = new QWidget(nb);
	QGridLayout *gl = new QGridLayout(w, 1, 1, space);
	
#if 0

	/*
	 * Label
	 */
	QGroupBox *gbl = new QGroupBox(w);
	gbl->setTitle(i18n("Column Selection:"));

	gl->addWidget(gbl, 0, 0);

	QGridLayout *gll = new QGridLayout(gbl, LVEnd, 1, 20);

	/*
	 * Note that the first column (Player == 0) is always there
	 */
	for (int i = 1; i < LVEnd; i++) {
		column[i]->cb = new QCheckBox(columnTitle[i], gbl);
		column[i]->cb->setChecked(column[i]->show);
		column[i]->cb->adjustSize();
		column[i]->cb->setMinimumSize(column[i]->cb->size());
		gll->addRowSpacing(0, column[i]->cb->height());
		gll->addWidget(column[i]->cb, i, 0, AlignLeft);
	}

#endif

	/*
	 * put in the page and connect
	 * ===========================
	 */
	gl->activate();
	w->adjustSize();
	w->setMinimumSize(w->size());
	nb->addTab(w, i18n("&Playerlist"));

	//connect(nb, SIGNAL(applyButtonPressed()), this, SLOT(setupOk()));
}

void KFibsPlayerList::setupCancel() {}
void KFibsPlayerList::setupDefault() {}

/*
 * Called when the setup dialog is positively closed
 */
void KFibsPlayerList::setupOk()
{
#if 0
	int i;
	bool change = false;
	
	for (i = 1; i < LVEnd; i++) 
		change |= (column[i]->cb->isChecked() != column[i]->show);

	/*
	 * Only juggle with the columns if something changed
	 */
	if (change) {

		/*
		 * It's important to remove the columns in reverse order
		 */
		for (i = LVEnd-1; i > 0; i--)
			if (column[i]->show) 
				removeColumn(column[i]->index);
		
		/*
		 * Now add all columns that are selected
		 */
		for (i = 1; i < LVEnd; i++) {
			column[i]->show = column[i]->cb->isChecked();
			if (column[i]->show) {
				column[i]->index = addColumn(columnTitle[i], column[i]->width);
				if (i == Experience || i == Rating || i == Time || i == Idle)
					setColumnAlignment(column[i]->index, AlignRight);
			}
		}
		
		/*
		 * Reload the list
		 */
		slotReload();
	}
		
	/*
	 * store the new settings
	 */
	saveConfig();
#endif
}

// ----------------------------------------------------------------------------------------------------


// == constructor, destructor and setup ========================================

/*
 * The titles of the column headers
 */
QString KFibsPlayerList::columnTitle[LVEnd] = {
	
	i18n("Player"),
	i18n("Opponent"),
	i18n("Watches"),
	i18n("Status"),
	i18n("Rating"),
	i18n("Exp."),
	i18n("Idle"),
	i18n("Time"),
	i18n("Hostname"),
	i18n("Client"),
	i18n("Email")
};

/*
 * These strings shouldn't be translated!!
 */
QString KFibsPlayerList::columnKey[LVEnd] = {
	
	"player",
	"opponent",
	"watching",
	"status",
	"rating",
	"experience",
	"idle",
	"time",
	"hostname",
	"client",
	"email"
};

/*
 * Construct the playerlist and do some initial setup
 */
KFibsPlayerList::KFibsPlayerList(QWidget *parent, const char *name)
	: KListView(parent, name)
{
	/*
	 * Initialize variables
	 */
	cCount[0] = cCount[1] = 0;

	_abbrev[Blind] = i18n("abreviate blind", "B");
	_abbrev[Away ] = i18n("abreviate away",  "A");
	_abbrev[Ready] = i18n("abreviate ready", "R");

	_name = QString::null;

	watching = false;

	/*
	 * Allocate the column information
	 */
	for (int i = 0; i < LVEnd; i++)
		column[i] = new columnInfo;

	/*
	 * Get a sane caption, initialize some eye candy and read the
	 * configuration - needed for the column information.
	 */
	updateCaption();
	readConfig();
	setIcon(kapp->miniIcon());
	QWhatsThis::add(this, i18n("This window contains the player list. It shows "
				   "all players that are currently logged into FIBS."
				   "Use the right mouse button to get a context "
				   "menu with helpful information and commands."));

	/*
	 * Put the columns into the list view
	 */
	for (int i = 0; i < LVEnd; i++) {
		if (column[i]->show) {
		 	column[i]->index = addColumn(columnTitle[i], column[i]->width);
			if (i == Experience || i == Rating || i == Time || i == Idle)
				setColumnAlignment(column[i]->index, AlignRight);
		} else {
			column[i]->index = -1;
		}
	}
	setAllColumnsShowFocus(true);

	/* 
	 * Create context menus
	 */
	cm = new QPopupMenu();
	im = new QPopupMenu();
	 
	/*
	 * Create the whole set of actions
	 */
	act[Info]    = new KAction(i18n("Info   "), QIconSet(kapp->iconLoader()->loadIcon("help.xpm", KIcon::Small)),
				   0, this, SLOT(slotInfo()),   this);
	act[Talk]    = new KAction(i18n("Talk   "), QIconSet(kapp->iconLoader()->loadIcon(PROG_NAME "-chat.png", KIcon::Small)),
				   0, this, SLOT(slotTalk()),   this);

	act[Look]     = new KAction(i18n("Look   "), 0, this, SLOT(slotLook()),   this);
	act[Watch]    = new KAction(i18n("Watch  "), 0, this, SLOT(slotWatch()),  this);
	act[Unwatch]  = new KAction(i18n("Unwatch"), 0, this, SLOT(slotUnwatch()),this);
	act[BlindAct] = new KAction(i18n("Blind  "), 0, this, SLOT(slotBlind()),  this);
	act[Update]   = new KAction(i18n("Update "), 0, this, SLOT(slotUpdate()), this);

	act[Reload] = KStdAction::redisplay(this, SLOT(slotReload()), this);
	act[Mail]   = KStdAction::mail(this, SLOT(slotMail()),   this);
	act[Close]  = KStdAction::close(this, SLOT(hide()), this);

	act[InviteD]  = new KAction(i18n("Use Dialog"),    0, this, SLOT(slotInviteD()), this);
	act[Invite1]  = new KAction(i18n("1 Point match"), 0, this, SLOT(slotInvite1()), this);
	act[Invite2]  = new KAction(i18n("2 Point Match"), 0, this, SLOT(slotInvite2()), this);
	act[Invite3]  = new KAction(i18n("3 Point Match"), 0, this, SLOT(slotInvite3()), this);
	act[Invite4]  = new KAction(i18n("4 Point Match"), 0, this, SLOT(slotInvite4()), this);
	act[Invite5]  = new KAction(i18n("5 Point Match"), 0, this, SLOT(slotInvite5()), this);
	act[Invite6]  = new KAction(i18n("6 Point Match"), 0, this, SLOT(slotInvite6()), this);
	act[Invite7]  = new KAction(i18n("7 Point Match"), 0, this, SLOT(slotInvite7()), this);
	act[InviteU]  = new KAction(i18n("Unlimited"),     0, this, SLOT(slotInviteU()), this);
	act[InviteR]  = new KAction(i18n("Resume"),        0, this, SLOT(slotInviteR()), this);

	/*
	 * Fill normal context menu
	 */
	act[Info]->plug(cm); act[Talk]->plug(cm); act[Mail]->plug(cm);    
	cm->insertSeparator();
	imID = cm->insertItem(i18n("Invite"), im); // save ID for later
	act[Look    ]->plug(cm); act[Watch   ]->plug(cm); 
	act[Unwatch ]->plug(cm); act[BlindAct]->plug(cm);
	cm->insertSeparator();
	act[Update]->plug(cm); act[Reload]->plug(cm);
	cm->insertSeparator();
	act[Close]->plug(cm);

	/*
	 * Fill the invitation menu
	 */
	act[InviteD]->plug(im); 
	im->insertSeparator();
	act[Invite1]->plug(im); act[Invite2]->plug(im); 
	act[Invite3]->plug(im); act[Invite4]->plug(im); 
	act[Invite5]->plug(im); act[Invite6]->plug(im); 
	act[Invite7]->plug(im); 
	im->insertSeparator();
	act[InviteU]->plug(im);	act[InviteR]->plug(im); 

	/* 
	 * Right mouse button gets context menu, double click gets player info 
	 */
	connect(this, SIGNAL(contextMenu(KListView *, QListViewItem *, const QPoint &)), 
		this, SLOT(showContextMenu(KListView *, QListViewItem *, const QPoint &)));
	connect(this, SIGNAL(doubleClicked(QListViewItem *, const QPoint &, int)),
		this, SLOT(getPlayerInfo(QListViewItem *, const QPoint &, int)));
}

/*
 * Destructor deletes members
 */
KFibsPlayerList::~KFibsPlayerList()
{
	for (int i = 0; i < LVEnd; i++)
		delete column[i];
	delete cm;
	delete im;
}


// == settings and config ======================================================

/*
 * Restore the saved settings
 */
void KFibsPlayerList::readConfig()
{
	KConfig* config = kapp->config();
	config->setGroup(name());
	
	QPoint pos, defpos(10, 10);
	pos = config->readPointEntry("ori", &defpos);
	setGeometry(pos.x(), pos.y(), config->readNumEntry("wdt",460), config->readNumEntry("hgt",190));
	
	(config->readBoolEntry("vis", false)) ? show() : hide();

	int c = 0;
	for (int i = 0; i < LVEnd; i++) {
		column[i]->show  = config->readBoolEntry("col-" + columnKey[i], true);
		column[i]->width = config->readNumEntry("col-w-" + columnKey[i], -1);
	}
}

/*
 * Save current settings
 */
void KFibsPlayerList::saveConfig()
{
	KConfig* config = kapp->config();
	config->setGroup(name());

	config->writeEntry("ori", pos());
	config->writeEntry("hgt", height());
	config->writeEntry("wdt", width());

	config->writeEntry("vis", isVisible());

	for (int i = 0; i < LVEnd; i++) {
		config->writeEntry("col-" + columnKey[i], column[i]->show);
		config->writeEntry("col-w-" + columnKey[i], (column[i]->show) ? columnWidth(column[i]->index) : -1);
	}
}


// == popup menu slots and functions ===========================================

/*
 * Save selected player, update the menu entries and show the popup menu
 */
void KFibsPlayerList::showContextMenu(KListView *, QListViewItem *i, const QPoint &p) 
{
	/*
	 * Get the name of the selected player
	 */
	user = i ? i->text(Player) : QString::null;

	act[Info  ]->setText(i18n("Info on %1" ).arg(user));
	act[Talk  ]->setText(i18n("Talk to %1" ).arg(user));
	act[Mail  ]->setText(i18n("Email to %1").arg(user));
	act[Look  ]->setText(i18n("Look at %1" ).arg(user));
	act[Watch ]->setText(i18n("Watch %1"   ).arg(user));
	act[Update]->setText(i18n("Update %1"  ).arg(user));
	
	act[Info    ]->setEnabled(i);
	act[Talk    ]->setEnabled(i);
	act[Mail    ]->setEnabled(i);
	act[Look    ]->setEnabled(i);
	act[Watch   ]->setEnabled(i);
	act[Update  ]->setEnabled(i);
	act[BlindAct]->setEnabled(i);

	act[Unwatch]->setEnabled(watching);

	cm->setItemEnabled(imID, i && _name != user);
	cm->changeItem(imID, i18n("Invite %1").arg(user));

	email = i && column[Email]->show ? i->text(column[Email]->index) : QString::null;
	act[Mail]->setEnabled(!email.isEmpty()); 

	if (i && column[Status]->show) 
		act[BlindAct]->setText((i->text(column[Status]->index).contains(_abbrev[Blind])) ?
				       i18n("Unblind %1").arg(user) : i18n("Blind %1").arg(user));
	else 
		act[BlindAct]->setText(i18n("Blind"));
	
	// show the menu
	cm->popup(p);
}

/*
 * Reload the entire list
 */
void KFibsPlayerList::slotReload()
{
	emit fibsCommand("rawwho");
	clear();
}

/*
 * Stop watching 
 */
void KFibsPlayerList::slotUnwatch()
{
	emit fibsCommand("unwatch");
}

/*
 * Blind/Unblind user
 */
void KFibsPlayerList::slotBlind()
{
	emit fibsCommand("blind " + user);
}

/*
 * Start talking to user
 */
void KFibsPlayerList::slotTalk()
{
	emit fibsTalk(user);
}

/*
 * Request information on user
 */
void KFibsPlayerList::slotInfo()
{
	emit fibsCommand("whois " + user);
}

/*
 * Look at user
 */
void KFibsPlayerList::slotLook()
{
	emit fibsCommand("look " + user);
}

/*
 * Send an email to player user at address email
 */
void KFibsPlayerList::slotMail()
{
	kapp->invokeMailer(email, QString::null);
}

/*
 * Request a new entry for user
 */
void KFibsPlayerList::slotUpdate()
{
	emit fibsCommand("rawwho " + user);
}

/*
 * Watch user and get an updated board
 */
void KFibsPlayerList::slotWatch()
{
	emit fibsCommand("watch " + user);
	emit fibsCommand("board");
}

/*
 * Invite the selected user.
 */
void KFibsPlayerList::slotInviteD()
{
	emit fibsInvite(user);
}
void KFibsPlayerList::slotInvite1() { emit fibsCommand("invite " + user + " 1"); }
void KFibsPlayerList::slotInvite2() { emit fibsCommand("invite " + user + " 2"); }
void KFibsPlayerList::slotInvite3() { emit fibsCommand("invite " + user + " 3"); }
void KFibsPlayerList::slotInvite4() { emit fibsCommand("invite " + user + " 4"); }
void KFibsPlayerList::slotInvite5() { emit fibsCommand("invite " + user + " 5"); }
void KFibsPlayerList::slotInvite6() { emit fibsCommand("invite " + user + " 6"); }
void KFibsPlayerList::slotInvite7() { emit fibsCommand("invite " + user + " 7"); }

void KFibsPlayerList::slotInviteU() { emit fibsCommand("invite " + user + " unlimited"); }
void KFibsPlayerList::slotInviteR() { emit fibsCommand("invite " + user); }


// == inserting and updating the list ==========================================

/*
 * Add or change the entry of player with the corresponding string
 * from the server - rawwho
 */
void KFibsPlayerList::changePlayer(const QString &line)
{
	char entry[LVEnd][100];
	char ready[2], away[2];
	QListViewItem *i;
	QDateTime fromEpoch;
	QString str_entry[LVEnd], tmp;

	entry[Status][0] = '\0';

	// the line comes from FIBS and is 7 bit ASCII
	sscanf(line.latin1(), "%99s %99s %99s %1s %1s %99s %99s %99s %99s %99s %99s %99s", entry[Player], entry[Opponent], 
	       entry[Watches], ready, away, entry[Rating], entry[Experience], entry[Idle], entry[Time], 
	       entry[Host], entry[Client], entry[Email]);
	
	// convert time
	tmp = entry[Time];
	fromEpoch.setTime_t(tmp.toUInt());
	strcpy(entry[Time], fromEpoch.toString().latin1());

	// clear empty strings and copy
	for (int j = 0; j < LVEnd; j++) {
		if ((str_entry[j] = entry[j]) == "-") 
			str_entry[j] = "";
	}
	str_entry[Status].replace(Ready, 1, ready[0] == '0' ? "-" : _abbrev[Ready]);
	str_entry[Status].replace(Away,  1, away [0] == '0' ? "-" : _abbrev[Away ]);
	str_entry[Status].replace(Blind, 1, "-");

	// disable drawing until the end of update
	setUpdatesEnabled(false); 

	// try to find the item in the list
	QListViewItemIterator it(this);
	for ( ; it.current(); ++it) {
		if (it.current()->text(0) == str_entry[Player]) {
			i = it.current();
			goto found;
		}
	}

	// getting here means we have to create a new entry
	i = new KFibsPlayerListLVI(this);
	
	// count the KFibs and KBackgammon clients
	if (str_entry[Client].contains("KFibs"))
		cCount[0]++;
	else if (str_entry[Client].contains(PROG_NAME)) 
		cCount[1]++;

	// new entry requires an update to the player count
	updateCaption();
	
	goto update;

 found:
	
	// getting here means the player is in the list - update private status
	str_entry[Status].replace(Blind,1,i->text(Status).contains(_abbrev[Blind]) ? _abbrev[Blind] : "-");
	
 update:

	// update entries
        for (int j = 0; j < LVEnd; j++) {
		if (column[j]->show) 
			i->setText(column[j]->index, str_entry[j]);
	}

	// find out if we are watching somebody
	if (_name == str_entry[Player]) 
		watching = !str_entry[Watches].isEmpty();
}

/*
 * Remove player from the list
 */
void KFibsPlayerList::deletePlayer(const QString &player)
{
	QListViewItemIterator it(this);
	for ( ; it.current(); ++it) {
		if (it.current()->text(0) == player) {
			if (it.current()->text(Client).contains(PROG_NAME))
				--cCount[1];
			else if (it.current()->text(Client).contains("KFibs"))
				--cCount[0];
			delete it.current();
			updateCaption();
			return;
		}
	}
}

/*
 * Set/Unset the status stat in the corresponding column of the list
 */
void KFibsPlayerList::changePlayerStatus(const QString &player, int stat, bool flag)
{
	QListViewItem *i = 0;

	/*
	 * Find the correct line
	 */
	QListViewItemIterator it(this);
	for ( ; it.current(); ++it) {
		if (it.current()->text(Player) == player) {
			i = it.current();
			break;
		}
	}
	if (!i)	return;
	
	/*
	 * Update the status flag
	 */
	i->setText(Status, i->text(Status).replace(stat, 1, (flag) ? _abbrev[stat] : "-"));
}


// == various slots and functions ==============================================

/*
 * Reverse column to index mapping. Return negative on error.
 */
int KFibsPlayerList::cIndex(int col)
{
	for (int i = 0; i < LVEnd; i++)
		if (column[i]->index == col)
			return i;
	return -1;
}

/*
 * Catch hide events, so the engine's menu can be update. 
 */
void KFibsPlayerList::showEvent(QShowEvent *e)
{
	KListView::showEvent(e);
	emit windowVisible(true);
}

/*
 * Catch hide events, so the engine's menu can be update. 
 */
void KFibsPlayerList::hideEvent(QHideEvent *e)
{
	emit windowVisible(false);
	KListView::hideEvent(e);
}

/*
 * Called at the end of updates to re-enable the UI
 */
void KFibsPlayerList::stopUpdate()
{
	setUpdatesEnabled(true);
	triggerUpdate();
}

/*
 * Knowing our own name allows us to disable certain menu entries for
 * ourselves.
 */
void KFibsPlayerList::setName(const QString &name)
{
	_name = name;
}

/*
 * Update the caption of the list by including the current client
 * count
 */
void KFibsPlayerList::updateCaption()
{
	setCaption(i18n("Player List - %1 - %2/%3").arg(childCount()).arg(cCount[0]).arg(cCount[1]));
}

/*
 * Clear the list and reset the client counters
 */
void KFibsPlayerList::clear()
{
	cCount[0] = 0;
	cCount[1] = 0;
	QListView::clear();
}

// EOF
