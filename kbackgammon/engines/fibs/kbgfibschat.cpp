
/* Yo Emacs, this -*- C++ -*-

  Copyright (C) 1999-2001 Jens Hoefkens
  jens@hoefkens.com
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  $Id: kbgfibschat.cpp 102972 2001-06-19 06:22:43Z hoefkens $
  
*/


#include "kbgfibschat.h"
#include "kbgfibschat.moc"

#include <qlayout.h>
#include <qlabel.h>
#include <qpopupmenu.h>
#include <qregexp.h>
#include <qfont.h>
#include <qwhatsthis.h>
#include <qdatetime.h>
#include <qclipboard.h>
#include <qsimplerichtext.h>
#include <qregion.h>
#include <qpalette.h>
#include <qpainter.h>
#include <qpoint.h>
#include <qlistbox.h>
#include <qiconset.h>

#include <klocale.h>
#include <kconfig.h>
#include <kapp.h>
#include <kdebug.h>
#include <kstdaction.h>
#include <ktabctl.h>
#include <kaction.h>
#include <kiconloader.h>

#include "clip.h"
#include "version.h"


/*
 * Private utility class that might become more generally usefull in
 * the future. Basically, it implements rich text QListBox items.
 */
class KLBT : public QListBoxText
{
	
public:
	
	/**
	 * Constructor
	 */
	KLBT(QWidget *parent, const QString &text = QString::null, const QString &player = QString::null);
	
	/**
	 * Destructor
	 */
	virtual ~KLBT();
	
	/**
	 * Overloaded required members returning width and height of the 
	 */
	virtual int height(const QListBox *) const;
	virtual int width(const QListBox *) const;
	
	/**
	 * The context menu needs the name of the player. It's easier
	 * than extracting it from the text.
	 */
	QString player() const;
	
protected:
	
	/**
	 * Required overloaded member to paint the text on the painter p.
	 */
	virtual void paint(QPainter *p);
		
private:
	
	QSimpleRichText *t;
	QWidget *w;
	QString *n;
};


// == constructor, destructor ==================================================

/*
 * Constructor of the chat window.
 */
KBgChat::KBgChat(QWidget *parent, const char *name)
	: KChat(parent, false)
{ 
	user = QString::null;
	cm = 0;
	im = new QPopupMenu();
	
	setAutoAddMessages(false); // we get an echo from FIBS
	setFromNickname("hoefkens");
	
	if (!addSendingEntry(i18n("Kibitz to watchers and players"), CLIP_YOU_KIBITZ))
		kdDebug(10500) << "adding kibitz" << endl;
	if (!addSendingEntry(i18n("Whisper to watchers only"), CLIP_YOU_WHISPER))
		kdDebug(10500) << "adding whisper" << endl;
	
	connect(this, SIGNAL(rightButtonClicked(QListBoxItem *, const QPoint &)),
		this, SLOT(contextMenu(QListBoxItem *, const QPoint &)));

	connect(this, SIGNAL(signalSendMessage(int, const QString &)),
		this, SLOT(handleCommand(int, const QString &)));
	
	nameToID = new QDict<int>(17, true);
	nameToID->setAutoDelete(true);

	/*
	 * some eye candy :)
	 */
	setIcon(kapp->miniIcon());
	setCaption(i18n("Chat Window"));

	QWhatsThis::add(this, i18n("This is the chat window.\n\n"
				   "The text in this window is colored depending on whether "
				   "it is directed at you personally, shouted to the general "
				   "FIBS population, has been said by you, or is of general "
				   "interest. If you select the name of a player, the context "
				   "contains entries specifically geared towards that player."));
	/*
	 * Define set of available actions
	 */	
	act[Inquire]  = new KAction(i18n("Info on"), QIconSet(kapp->iconLoader()->loadIcon("help.xpm", KIcon::Small)),
				    0, this, SLOT(slotInquire()), this);
	act[Talk]     = new KAction(i18n("Talk to"), QIconSet(kapp->iconLoader()->loadIcon(PROG_NAME "-chat.png", KIcon::Small)),
				    0, this, SLOT(slotTalk()),    this);

	act[InviteD]  = new KAction(i18n("Use Dialog"),    0, this, SLOT(slotInviteD()),  this);
	act[Invite1]  = new KAction(i18n("1 Point match"), 0, this, SLOT(slotInvite1()),  this);
	act[Invite2]  = new KAction(i18n("2 Point Match"), 0, this, SLOT(slotInvite2()),  this);
	act[Invite3]  = new KAction(i18n("3 Point Match"), 0, this, SLOT(slotInvite3()),  this);
	act[Invite4]  = new KAction(i18n("4 Point Match"), 0, this, SLOT(slotInvite4()),  this);
	act[Invite5]  = new KAction(i18n("5 Point Match"), 0, this, SLOT(slotInvite5()),  this);
	act[Invite6]  = new KAction(i18n("6 Point Match"), 0, this, SLOT(slotInvite6()),  this);
	act[Invite7]  = new KAction(i18n("7 Point Match"), 0, this, SLOT(slotInvite7()),  this);
	act[InviteU]  = new KAction(i18n("Unlimited"),     0, this, SLOT(slotInviteU()),  this);
	act[InviteR]  = new KAction(i18n("Resume"),        0, this, SLOT(slotInviteR()),  this);

	act[InviteD]->plug(im);

	im->insertSeparator();

	act[Invite1]->plug(im);
	act[Invite2]->plug(im);
	act[Invite3]->plug(im);
	act[Invite4]->plug(im);
	act[Invite5]->plug(im);
	act[Invite6]->plug(im);
	act[Invite7]->plug(im);

	im->insertSeparator();

	act[InviteU]->plug(im);
	act[InviteR]->plug(im);

	act[Gag]      = new KAction(i18n("Gag"),   0, this, SLOT(slotGag()),     this);
	act[Ungag]    = new KAction(i18n("Ungag"), 0, this, SLOT(slotUngag()),   this);
	act[Cleargag] = new KAction(i18n("Clear Gag List"), 0, this, SLOT(slotCleargag()), this);
	act[Copy]     = KStdAction::copy(this, SLOT(slotCopy()), this);
	act[Clear]    = new KAction(i18n("Clear"),        0, this, SLOT(slotClear()), this);
	act[Close]    = KStdAction::close(this, SLOT(hide()), this);
	act[Silent]   = new KToggleAction(i18n("Silent"), 0, this, SLOT(slotSilent()), this);
}


/*
 * Destructor
 */
KBgChat::~KBgChat()
{
	delete nameToID;
	delete cm; // save to delete NULL pointers
	delete im;
}


// == configuration handling ===================================================

/*
 * Restore the previously stored settings
 */
void KBgChat::readConfig()
{
	KConfig* config = kapp->config();
	config->setGroup(name());

	QFont kappFont = kapp->font();
	QPoint pos(10, 10);

	pos = config->readPointEntry("ori", &pos);
	setGeometry(pos.x(), pos.y(), config->readNumEntry("wdt",460), config->readNumEntry("hgt",200));
	
	config->readBoolEntry("vis", false) ? show() : hide();
	
	setFont(config->readFontEntry("fon", &kappFont));

	((KToggleAction *)act[Silent])->setChecked(config->readBoolEntry("sil", false));

	gagList = config->readListEntry("gag");
}

/*
 * Save the current settings to disk
 */
void KBgChat::saveConfig()
{
	KConfig* config = kapp->config();
	config->setGroup(name());

	config->writeEntry("ori", pos());
	config->writeEntry("hgt", height());
	config->writeEntry("wdt", width());
	config->writeEntry("vis", isVisible());
	config->writeEntry("fon", font());

	config->writeEntry("sil", ((KToggleAction *)act[Silent])->isChecked());
	
	config->writeEntry("gag", gagList);
}


/*
 * Setup dialog page of the player list - allow the user to select the 
 * columns to show
 */
void KBgChat::getSetupPages(KTabCtl *nb, int space)
{
	/*
	 * Main Widget
	 * ===========
	 */
	QWidget *w = new QWidget(nb);
	QGridLayout *gl = new QGridLayout(w, 2, 1, space);

	lb = new QListBox(w);
	lb->setMultiSelection(true);
	
	lb->insertStringList(gagList);
	
	QLabel *info = new QLabel(w);
	info->setText(i18n("Select users to be removed from the gag list."));

	QWhatsThis::add(w, i18n("Select all the users you want "
				"to remove from the gag list "
				"and then click Ok. Afterwards "
				"you will again hear what they shout."));
	
	gl->addWidget(lb, 0, 0);
	gl->addWidget(info, 1, 0);
	
	/*
	 * put in the page
	 * ===============
	 */
	gl->activate();
	w->adjustSize();
	w->setMinimumSize(w->size());
	nb->addTab(w, i18n("&Gag List"));
}

/*
 * Remove all the selected entries from the gag list
 */
void KBgChat::setupOk()
{
	for (uint i = 0; i < lb->count(); ++i) { 
		if (lb->isSelected(i))
			gagList.remove(lb->text(i));
	}
	lb->clear();
	lb->insertStringList(gagList);
}

/*
 * Don't do anything
 */
void KBgChat::setupCancel()
{
	// empty
}

/*
 * By default, all players stay in the gag list
 */
void KBgChat::setupDefault()
{
	lb->clearSelection();
}


// == various slots and functions ==============================================

/*
 * Overloaded member to create a QListBoxItem for the chat window.
 */
QListBoxItem* KBgChat::layoutMessage(const QString& fromName, const QString& text)
{
	QListBoxText* message = new KLBT(this, text, fromName);
	return message;
}

/*
 * Catch hide events, so the engine's menu can be update. 
 */
void KBgChat::showEvent(QShowEvent *e)
{
	QFrame::showEvent(e);
	emit windowVisible(true);
}

/*
 * Catch hide events, so the engine's menu can be update. 
 */
void KBgChat::hideEvent(QHideEvent *e)
{
	emit windowVisible(false);
	QFrame::hideEvent(e);
}

/*
 * At the beginning of a game, add the name to the list and switch to
 * kibitz mode.
 */
void KBgChat::startGame(const QString &name)
{
	int *id = nameToID->find(opponent = name);
	if (!id) {
		id = new int(nextId());
		nameToID->insert(name, id);
		addSendingEntry(i18n("Talk to %1").arg(name), *id);
	}
	setSendingEntry(CLIP_YOU_KIBITZ);
}

/*
 * At the end of a game, we switch to talk mode.
 */
void KBgChat::endGame()
{
	int *id = nameToID->find(opponent);
	if (id)
		setSendingEntry(*id);
	else
		setSendingEntry(SendToAll);
}

/*
 * Set the chat window ready to talk to name
 */
void KBgChat::fibsTalk(const QString &name) 
{
	int *id = nameToID->find(name);
	if (!id) {
		id = new int(nextId());
		nameToID->insert(name, id);
		addSendingEntry(i18n("Talk to %1").arg(name), *id);
	}
	setSendingEntry(*id);
};

/*
 * Remove the player from the combo box when he/she logs out.
 */
void KBgChat::deletePlayer(const QString &name)
{
	int *id = nameToID->find(name);
	if (id) {
		removeSendingEntry(*id);
		nameToID->remove(name);
	}
}

/*
 * Take action when the user presses return in the line edit control.
 */
void KBgChat::handleCommand(int id, const QString& msg)
{
	int realID = sendingEntry();

	switch (realID) {
	
	case SendToAll:
		emit fibsCommand("shout " + msg);
		break;

	case CLIP_YOU_KIBITZ:
		emit fibsCommand("kibitz " + msg);
		break;		

	case CLIP_YOU_WHISPER:
		emit fibsCommand("whisper " + msg);
		break;

	default:

		QDictIterator<int> it(*nameToID);
		while (it.current()) {
			if (*it.current() == realID) {				
				emit fibsCommand("tell " + it.currentKey() + " " + msg);
				return;
			}
			++it;
		}
		kdDebug(10500) << "unrecognized ID in KBgChat::handleCommand" << endl;
	}
}


// == handle strings from the server ===========================================

/*
 * A message from the server that should be handled by us. If necessary, 
 * we replace the CLIP number by a string and put the line into the window.
 *
 * This function emits the string in rich text format with the signal 
 * personalMessage - again: the string contains rich text!
 */
void KBgChat::handleData(const QString &msg)
{    
	QString clip = msg.left(msg.find(' ')), user, cMsg = msg;

	bool flag = false;
	int cmd = clip.toInt(&flag);

	QDateTime date;
 
	if (flag) {
		cMsg.replace(0, cMsg.find(' ')+1, "");

		user = cMsg.left(cMsg.find(' '));

		switch (cmd) {			
		case CLIP_SAYS:
			if (!gagList.contains(user)) {
				cMsg = i18n("<u>%1 tells you:</u> %2").arg(user).arg(cMsg.replace(QRegExp("^" + user), ""));
				cMsg = "<font color=\"red\">" + cMsg + "</font>";
				emit personalMessage(cMsg);
			} else 
				cMsg = "";
			break;

		case CLIP_SHOUTS:
			if ((!((KToggleAction *)act[Silent])->isChecked()) && (!gagList.contains(user))) {
				cMsg = i18n("<u>%1 shouts:</u> %2").arg(user).arg(cMsg.replace(QRegExp("^" + user), ""));
				cMsg = "<font color=\"black\">" + cMsg + "</font>";
			} else
				cMsg = "";
			break;
			
		case CLIP_WHISPERS:
			if (!gagList.contains(user)) {
				cMsg = i18n("<u>%1 whispers:</u> %2").arg(user).arg(cMsg.replace(QRegExp("^" + user), ""));
				cMsg = "<font color=\"red\">" + cMsg + "</font>";
				emit personalMessage(cMsg);
			} else 
				cMsg = "";
			break;

		case CLIP_KIBITZES:
			if (!gagList.contains(user)) {
				cMsg = i18n("<u>%1 kibitzes:</u> %2").arg(user).arg(cMsg.replace(QRegExp("^" + user), ""));
				cMsg = "<font color=\"red\">" + cMsg + "</font>";
				emit personalMessage(cMsg);
			} else 
				cMsg = "";
			break;

		case CLIP_YOU_SAY:		
			cMsg = i18n("<u>You tell %1:</u> %2").arg(user).arg(cMsg.replace(QRegExp("^" + user), ""));
			cMsg = "<font color=\"darkgreen\">" + cMsg + "</font>";
			emit personalMessage(cMsg);
			user = QString::null;
			break;

		case CLIP_YOU_SHOUT:
			cMsg = i18n("<u>You shout:</u> %1").arg(cMsg);
			cMsg = "<font color=\"darkgreen\">" + cMsg + "</font>";
			emit personalMessage(cMsg);
			user = QString::null;
			break;

		case CLIP_YOU_WHISPER:
			cMsg = i18n("<u>You whisper:</u> %1").arg(cMsg);
			cMsg = "<font color=\"darkgreen\">" + cMsg + "</font>";
			emit personalMessage(cMsg);
			user = QString::null;
			break;

		case CLIP_YOU_KIBITZ:
			cMsg = i18n("<u>You kibitz:</u> %1").arg(cMsg);
			cMsg = "<font color=\"darkgreen\">" + cMsg + "</font>";
			emit personalMessage(cMsg);
			user = QString::null;
			break;

		case CLIP_MESSAGE:
			user = cMsg.left(cMsg.find(' ')+1);
			cMsg.remove(0, cMsg.find(' ')+1);
			date.setTime_t(cMsg.left(cMsg.find(' ')+1).toUInt());
			cMsg.remove(0, cMsg.find(' '));
			cMsg = i18n("<u>User %1 left a message at %2</u>: %3").arg(user).arg(date.toString()).arg(cMsg);
			cMsg = "<font color=\"red\">" + cMsg + "</font>";
			emit personalMessage(cMsg);
			user = QString::null;
			break;

		case CLIP_MESSAGE_DELIVERED:
			cMsg = i18n("Your message for %1 has been delivered.").arg(user);
			cMsg = QString("<font color=\"darkgreen\">") + cMsg + "</font>";
			emit personalMessage(cMsg);
			user = QString::null;
			break;

		case CLIP_MESSAGE_SAVED:
			cMsg = i18n("Your message for %1 has been saved.").arg(user);
			cMsg = QString("<font color=\"darkgreen\">") + cMsg + "</font>";
			emit personalMessage(cMsg);
			user = QString::null;
			break;
			
		default: // ignore the message
			return;
		}

	} else {

		/*
		 * Special treatment for non-CLIP messages
		 */
		if (cMsg.contains(QRegExp("^You say to yourself: "))) {
			cMsg.replace(QRegExp("^You say to yourself: "), 
				     i18n("<u>You say to yourself:</u> ")); 
		} else {
			kdDebug(user == QString::null, 10500) << "KBgChat::handleData unhandled message: " 
							      << cMsg.latin1() << endl;
			return;
		}
	}

	if (cMsg != "")
		addMessage(user, cMsg);
}


// == context menu and related slots ===========================================

/*
 * RMB opens a context menu.
 */
void KBgChat::contextMenu(QListBoxItem *i, const QPoint &p)
{
	/*
	 * Even if i is non-null, user might still be QString::null
	 */
	user = (i == 0) ? QString::null : ((KLBT *)i)->player();
	text = (i == 0) ? QString::null : ((KLBT *)i)->text();

	/*
	 * Get a new context menu every time. Safe to delete the 0
	 * pointer.
	 */
	delete cm; cm = new QPopupMenu();

	/*
	 * Fill the context menu with actions
	 */
	if (user != QString::null) {

		act[Talk]->setText(i18n("Talk to %1").arg(user));
		act[Talk]->plug(cm);
		
		act[Inquire]->setText(i18n("Info on %1").arg(user));
		act[Inquire]->plug(cm);

		// invite menu is always the same
		cm->insertItem(i18n("Invite %1").arg(user), im);

		cm->insertSeparator();      

		if (gagList.contains(user) <= 0) {
			act[Gag]->setText(i18n("Gag %1").arg(user));
			act[Gag]->plug(cm);
		} else {
			act[Ungag]->setText(i18n("Ungag %1").arg(user));
			act[Ungag]->plug(cm);
		}
	}
	if (gagList.count() > 0)
		act[Cleargag]->plug(cm);
		
	if ((gagList.count() > 0) || (user != QString::null))
		cm->insertSeparator();

	act[Silent]->plug(cm);

	cm->insertSeparator();      

	act[Copy ]->plug(cm);
	act[Clear]->plug(cm);
	act[Close]->plug(cm);

	/*
	 * Show the context menu
	 */
	cm->popup(p);

}

/*
 * Clear the gag list
 */
void KBgChat::slotCleargag()
{
	gagList.clear();

	QString msg("<font color=\"blue\">");
	msg += i18n("The gag list is now empty.");
	msg += "</font>";
	
	addMessage(QString::null, msg);
}

/*
 * Gag the selected user
 */
void KBgChat::slotGag()
{
	gagList.append(user);

	QString msg("<font color=\"blue\">");
	msg += i18n("You won't hear what %1 says and shouts.").arg(user);
	msg += "</font>";
	
	addMessage(QString::null, msg);
}

/*
 * Simple interface to the actual talk slot
 */
void KBgChat::slotTalk()
{
	fibsTalk(user);
}

/*
 * Remove selected user from gag list
 */
void KBgChat::slotUngag()
{
	gagList.remove(user);
	
	QString msg("<font color=\"blue\">");
	msg += i18n("You will again hear what %1 says and shouts.").arg(user);
	msg += "</font>";

	addMessage(QString::null, msg);
}

/*
 * Get information on selected user
 */
void KBgChat::slotInquire()
{
	kdDebug(user == QString::null, 10500) << "KBgChat::slotInquire: user == null" << endl;
	emit fibsCommand("whois " + user);
}

/*
 * Block all shouts from the chat window
 */
void KBgChat::slotSilent()
{
	QString msg;
	if (((KToggleAction *)act[Silent])->isChecked())
		msg = "<font color=\"blue\">" + i18n("You won't hear what people shout.") + "</font>";
	else
		msg = "<font color=\"blue\">" + i18n("You will hear what people shout.") + "</font>";
	addMessage(QString::null, msg);
}

/*
 * Copy the selected line to the clipboard. Strip the additional HTML
 * from the text before copying.
 */
void KBgChat::slotCopy()
{
	text.replace(QRegExp("<u>"), ""); 
	text.replace(QRegExp("</u>"), ""); 
	text.replace(QRegExp("</font>"), ""); 
	text.replace(QRegExp("^.*\">"), ""); 

	kapp->clipboard()->setText(text);
}

/*
 * Invite the selected player.
 */
void KBgChat::slotInviteD()
{
	kdDebug(user == QString::null, 10500) << "KBgChat::slotInvite: user == null" << endl;
	emit fibsRequestInvitation(user);
}
void KBgChat::slotInvite1() { emit fibsCommand("invite " + user + " 1"); }
void KBgChat::slotInvite2() { emit fibsCommand("invite " + user + " 2"); }
void KBgChat::slotInvite3() { emit fibsCommand("invite " + user + " 3"); }
void KBgChat::slotInvite4() { emit fibsCommand("invite " + user + " 4"); }
void KBgChat::slotInvite5() { emit fibsCommand("invite " + user + " 5"); }
void KBgChat::slotInvite6() { emit fibsCommand("invite " + user + " 6"); }
void KBgChat::slotInvite7() { emit fibsCommand("invite " + user + " 7"); }

void KBgChat::slotInviteU() { emit fibsCommand("invite " + user + " unlimited"); }
void KBgChat::slotInviteR() { emit fibsCommand("invite " + user); }


// == implementation of the private QListBoxItem class =========================

KLBT::KLBT(QWidget *parent, const QString &text, const QString &player)
	: QListBoxText(text)	
{
	w = parent;
	n = new QString(player);
	t = new QSimpleRichText(text, w->font());
	
	t->setWidth(w->width());
	t->setWidth(2+t->widthUsed());
}

KLBT::~KLBT()
{
	delete t;
	delete n;
}

QString KLBT::player() const
{
	return *n;
}

int KLBT::height(const QListBox *lb) const
{
	return (1+t->height());
}

int KLBT::width(const QListBox *lb) const
{
	return t->width();
}

void KLBT::paint(QPainter *p)
{
	t->draw(p, 1, 1, QRegion(p->viewport()), w->palette());
}

// EOF
