/* Yo Emacs, this -*- C++ -*-

  Copyright (C) 1999,2000 Jens Hoefkens
  jens@hoefkens.com

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
  
*/

#include "kbginvite.h"
#include "kbginvite.moc"

#include <qlabel.h>
#include <qlayout.h>
#include <klocale.h>


/**
 *
 * This class implements a dialog for inviting players for games. 
 * 
 * This dialog is quite simple and follows the default style 
 * guide (almost :-)) The dialog offers specific numbers, unlimited
 * and resume.
 *
 */

/*
 * Constructor is quite simple - most positioning is left to 
 * the toolkit.
 */
KBgInvite::KBgInvite(const char *name)
	: QDialog(0, name, false)
{ 

	// FIXME: use standard layout

	setCaption(i18n("Invite Players"));

	/*
	 * All child widgets of the dialog
	 */
	QLabel *info = new QLabel(this);
	
	le     = new KLineEdit(this, "invitation dialog");
	sb     = new QSpinBox(1, 999, 1, this, "spin box");

	invite    = new QPushButton(i18n("&Invite"),    this);
	resume    = new QPushButton(i18n("&Resume"),    this);
	unlimited = new QPushButton(i18n("&Unlimited"), this);

	close     = new QPushButton(i18n("Close"),     this);
	cancel    = new QPushButton(i18n("Clear"),     this);
  
	info->setText(i18n("Type the name of the player you want to invite in the first entry\n"
			   "field and select the desired match length in the spin box."));

	/*
	 * Get the layouts
	 */
	QBoxLayout *vbox   = new QVBoxLayout(this, 7);

	QBoxLayout *hbox_1 = new QHBoxLayout(vbox);
	QBoxLayout *hbox_2 = new QHBoxLayout(vbox);
	QBoxLayout *hbox_3 = new QHBoxLayout(vbox);
	QBoxLayout *hbox_4 = new QHBoxLayout(vbox);
	
	/*
	 * Add all client widgets to the layouts
	 */
	hbox_1->addWidget(info);

	hbox_2->addWidget(le);
	hbox_2->addWidget(sb);

	hbox_3->addWidget(invite);
	hbox_3->addWidget(resume);
	hbox_3->addWidget(unlimited);

	hbox_4->addWidget(close);
	hbox_4->addWidget(cancel);

	/*
	 * Adjust widget sizes
	 */
	le->setMinimumSize(le->sizeHint());
	sb->setMinimumSize(sb->sizeHint());

	info->setMinimumSize(info->sizeHint());
	
	resume->setMinimumSize(resume->sizeHint());
	unlimited->setMinimumSize(unlimited->sizeHint());

	invite->setMinimumSize(invite->sizeHint());
	close->setMinimumSize(close->sizeHint());
	cancel->setMinimumSize(cancel->sizeHint());
	
	/*
	 * Resize the dialog
	 */
	setMinimumSize(childrenRect().size());
	vbox->activate();
	resize(minimumSize());
	
	/*
	 * Set focus and default buttons
	 */
	invite->setDefault(true);
	invite->setAutoDefault(true);
	le->setFocus();
	
	/*
	 * Connect the individual buttons
	 */
	connect(unlimited, SIGNAL(clicked()), SLOT(unlimitedClicked()));
	connect(resume,    SIGNAL(clicked()), SLOT(resumeClicked()));
	connect(invite,    SIGNAL(clicked()), SLOT(inviteClicked()));
	connect(close,     SIGNAL(clicked()), SLOT(hide()));
	connect(cancel,    SIGNAL(clicked()), SLOT(cancelClicked()));
}

/*
 * Destructor is empty
 */
KBgInvite::~KBgInvite()
{
	// nothing
}

/*
 * After hiding, we tell our creator that we are ready to die.
 */
void KBgInvite::hide()
{
	emit dialogDone();
}

/*
 * Set player name
 */
void KBgInvite::setPlayer(const QString &player)
{
	le->setText(player);
}

/*
 * Invitation with number
 */
void KBgInvite::inviteClicked()
{
	QString tmp;
	emit inviteCommand(QString("invite ") + le->text() + " " + tmp.setNum(sb->value()));
}

/*
 * Invitation for unlimited match
 */
void KBgInvite::unlimitedClicked()
{
	emit inviteCommand(QString("invite ") + le->text() + " unlimited");
}

/*
 * Resume a game
 */
void KBgInvite::resumeClicked()
{
	emit inviteCommand(QString("invite ") + le->text());
}

/*
 * Slot for Cancel. clear everything to default.
 */
void KBgInvite::cancelClicked()
{
	sb->setValue(1);
	le->clear();
}

// EOF
