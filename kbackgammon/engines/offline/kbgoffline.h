/* Yo Emacs, this -*- C++ -*-

  Copyright (C) 1999-2001 Jens Hoefkens
  jens@hoefkens.com

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

/*
  $Log$
  Revision 1.7  2001/04/18 05:16:40  hoefkens
  Lots of changes to the GNU Backgammon engine.
  Removed getMenu() fom all engines.
  Changed the Edit menu to Move.

  Revision 1.6  2001/03/19 05:34:04  hoefkens
  Engine redesign and settings changes - not yet finished

  Revision 1.5  2001/03/16 07:33:50  hoefkens
  Rewrite of the setup dialog. First (not fully working) eventsrc file.

  Revision 1.4  2001/03/11 02:01:55  hoefkens
  Many small changes and a major overhaul of the KBg class. More changes in
  the engines to come.


  Revision 1.3  2001/03/05 00:07:03  hoefkens
  Expanded and renamed the old KbgBoardStatus class. KBgStatus is now the
  basic representation of games.

  Many small changes (including several fixes).

  Revision 1.2  2001/01/14 23:15:55  hoefkens
  Several bugs fixed. Most notable is that the edit mode works now.
  
*/

#ifndef __KBGOFFLINE_H 
#define __KBGOFFLINE_H 

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif 


#include <qtimer.h>
#include <qspinbox.h>
#include <kaction.h>
#include <krandomsequence.h>

#include "kbgengine.h"
#include "kbgboard.h"
#include "kbgstatus.h"

/**
 *
 * The interface of an offline backgammon engine. The engine is inherently 
 * stupid and doesn't play - it just manages the games betweeen two humans.
 *
 */
class KBgEngineOffline : public KBgEngine
{
	Q_OBJECT

public:

	/*
	 * Constructor and destructor
	 */
	KBgEngineOffline( QWidget *parent = 0, QString *name = 0, QPopupMenu *pmenu = 0);
	virtual ~KBgEngineOffline();

	/**
	 * Fills the engine-specific page into the notebook
	 */
	virtual void getSetupPages(KDialogBase *nb);

	virtual void setupOk();
	virtual void setupDefault();
	virtual void setupCancel();

	/*
	 * Check with the engine if we can quit. This may require user
	 * interaction.
	 */
	virtual bool queryClose();

	/**
	 * About to be closed. Let the engine exit properly.
	 */
	virtual bool queryExit();

public slots:

	/**
	 * Read user settings from the config file
	 */
	virtual void readConfig();

	/**
	 * Save user settings to the config file
	 */
	virtual void saveConfig();
	
        /**
	 * Roll dice for the player w
	 */
        virtual void rollDice(const int w);
	
	/**
	 * Double the cube of player w
	 */
	virtual void doubleCube(const int w);

	/**
	 * A move has been made on the board - see the board class
	 * for the format of the string s
	 */
	virtual void handleMove(QString *s);

	/**
	 * Undo the last move
	 */
        virtual void undo();

	/**
	 * Redo the last move
	 */
        virtual void redo();

	/**
	 * Roll dice for whoevers turn it is
	 */
        virtual void roll();

	/**
	 * Double the cube for whoevers can double right now
	 */
        virtual void cube();

	/**
	 * Reload the board to the last known sane state
	 */
	virtual void load();

	/**
	 * Commit a move
	 */
	virtual void done();

	/**
	 * Process the string cmd
	 */
	virtual void handleCommand(const QString& cmd);

	/**
	 * Start a new game.
	 */
	virtual void newGame();
	virtual bool haveNewGame() {return true;}


protected slots:

        /**
	 * Initialize the state descriptors game[0] and game[1]
	 */
        void initGame();

	/**
	 * Switch back and forth between edit and play mode
	 */
	void toggleEditMode();
	
	/**
	 * Store if cmd is allowed or not
	 */
	void setAllowed(int cmd, bool f);

	/**
	 * Swaps the used colors on the board
	 */
	void swapColors();

protected:

	/**
	 * Returns a random integer between 1 and 6
	 */
	int getRandom();

	/**
	 * Set the dice for player w to a and b. Reload the board and determine the
	 * maximum number of moves
	 */
	void rollDiceBackend(const int w, const int a, const int b);

	/**
	 * Open a dialog to query for the name of player w. Return true unless
	 * the dialog was canceled.
	 */
	bool queryPlayerName(int w);
	
private:

	/**
	 * Use the standard method of obtaining random numbers
	 */
	KRandomSequence random;

	/**
	 * Store two copies of the game: one backup and a working copy
	 */
	KBgStatus game[2];

	/**
	 * Player's names
	 */
	QString nameUS, nameTHEM;

	/**
	 * Who did the last roll
	 */
	int lastRoll;
	
	/**
	 * How many checkers to move
	 */
	int toMove;

	/**
	 * Timer from making last move to commit
	 */
	QTimer* commitTimeout;
	
	/**
	 * Lenghth of the interval for committing
	 */
	int commitTimeoutLength;

	/**
	 * Various flags, representing the current status of the game
	 */
	bool rollingAllowed, undoPossible, donePossible; 
	bool gameRunning,    redoPossible, doublePossible; 

	/**
	 * Toggle the board into edit mode
	 */
	KToggleAction *editAction;

	/**
	 * Start a new game
	 */
	KAction *newAction;
	
	/**
	 * Swap the player's colors
	 */
	KAction *swapAction;

	/**
	 * Count the number of available undos
	 */
	int dummy, undoCounter;
};

#endif // __KBGOFFLINE_H 
