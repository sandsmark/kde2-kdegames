/* Yo Emacs, this -*- C++ -*-

  Copyright (C) 1999-2001 Jens Hoefkens
  jens@hoefkens.com

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

/*
  $Log$
  Revision 1.1  2001/05/02 19:40:10  hoefkens
  Added the engine directory

  Revision 1.9  2001/04/18 05:16:40  hoefkens
  Lots of changes to the GNU Backgammon engine.
  Removed getMenu() fom all engines.
  Changed the Edit menu to Move.

  Revision 1.8  2001/03/19 05:34:04  hoefkens
  Engine redesign and settings changes - not yet finished

  Revision 1.7  2001/03/16 07:33:50  hoefkens
  Rewrite of the setup dialog. First (not fully working) eventsrc file.

  Revision 1.6  2001/03/11 02:01:55  hoefkens
  Many small changes and a major overhaul of the KBg class. More changes in
  the engines to come.


  Revision 1.5  2001/03/05 00:07:03  hoefkens
  Expanded and renamed the old KbgBoardStatus class. KBgStatus is now the
  basic representation of games.

  Many small changes (including several fixes).

  Revision 1.4  2001/02/10 15:29:34  hoefkens
  Proper use of i18n() allows for better translations.

  Revision 1.3  2001/01/14 23:15:55  hoefkens
  Several bugs fixed. Most notable is that the edit mode works now.
  
*/

#include "kbgoffline.moc"
#include "kbgoffline.h"

#include <kapp.h>
#include <kmessagebox.h> 
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <qlayout.h>
#include <kiconloader.h>
#include <kstdaction.h>
#include <qbuttongroup.h> 
#include <qcheckbox.h>
#include <kconfig.h>
#include <iostream.h>
#include <klocale.h>
#include <kmainwindow.h>
#include <klineeditdlg.h>


// == constructor, destructor and other ========================================

/*
 * Constructor
 */
KBgEngineOffline::KBgEngineOffline(QWidget *parent, QString *name, QPopupMenu *pmenu)
	: KBgEngine(parent, name, pmenu)
{
	/*
	 * get some entropy for the dice
	 */
	random.setSeed(getpid()*time(NULL));

	/*
	 * Create engine specific actions
	 */
	newAction  = new KAction(i18n("&New Game"),    0, this, SLOT(newGame()),  this);
	swapAction = new KAction(i18n("&Swap Colors"), 0, this, SLOT(swapColors()), this);

	editAction = new KToggleAction(i18n("&Edit Mode"), 0, this, SLOT(toggleEditMode()), this);
	editAction->setChecked(false);

	/*
	 * create & initialize the menu
	 */
	 newAction->plug(menu);
	editAction->plug(menu);
	swapAction->plug(menu);

	/*
	 * get standard board and set it
	 */
	initGame();
	emit newState(game[0]);

	/*
	 * initialize the commit timeout
	 */
	commitTimeout = new QTimer(this);
	connect(commitTimeout, SIGNAL(timeout()), this, SLOT(done()));
	
	/*
	 * internal statue variables 
	 */
	rollingAllowed = undoPossible = gameRunning = donePossible = false;
	connect(this, SIGNAL(allowCommand(int, bool)), this, SLOT(setAllowed(int, bool)));

	/*
	 * Restore last stored settings
	 */
	readConfig();
}

/*
 * Destructor. The only child is the popup menu.
 */
KBgEngineOffline::~KBgEngineOffline()
{
	saveConfig();
}


// == start and init games =====================================================

/*
 * Start a new game.
 */
void KBgEngineOffline::newGame()
{
	int u = 0;
	int t = 0;

	/*
	 * If there is a game running we warn the user first
	 */
	if (gameRunning && (KMessageBox::warningYesNo((QWidget *)parent(),
						      i18n("A game is currently in progress. "
							   "Starting a new one will terminate it."),
						      QString::null, i18n("Start new game"), i18n("Continue old game"))
			    == KMessageBox::No))
		return;

	/*
	 * Separate from the previous game
	 */
	emit infoText("<br/><br/><br/>");

	/*
	 * Get player's names - user can still cancel
	 */
	if (!queryPlayerName(US) || !queryPlayerName(THEM))
		return;
		
	/*
	 * let the games begin
	 */
	gameRunning = true;
	
	/*
	 * Initialize the board
	 */
	initGame();

	/*
	 * Figure out who starts by rolling
	 */
	while (u == t) {
		u = getRandom(); 
		t = getRandom();
		emit infoText(i18n("%1 rolls %2, %3 rolls %4.").arg(nameUS).arg(u).arg(nameTHEM).arg(t));
	}
	
	if (u > t) {
		emit infoText(i18n("%1 makes the first move.").arg(nameUS));
		lastRoll = US;		
	} else {		
		emit infoText(i18n("%1 makes the first move.").arg(nameTHEM));
		lastRoll = THEM;		
		int n = u; u = t; t = n;		
	}

	/*
	 * set the dice and tell the board
	 */
	rollDiceBackend(lastRoll, u, t);

	/*
	 * tell the user
	 */
	emit statText(i18n("%1 vs. %2").arg(nameUS).arg(nameTHEM));
}

/*
 * Initialize the state descriptors game[0] and game[1]
 */
void KBgEngineOffline::initGame()
{
	/*
	 * nobody rolled yet
	 */
	lastRoll = -1;
	
	/*
	 * set up a standard game
	 */
	game[0].setCube(1, true, true);
	game[0].setDirection(+1);
	game[0].setColor(+1);
	for (int i = 1; i < 25; i++) 
		game[0].setBoard(i, US, 0);
	game[0].setBoard( 1, US,   2); game[0].setBoard( 6, THEM, 5);
	game[0].setBoard( 8, THEM, 3); game[0].setBoard(12, US,   5);
	game[0].setBoard(13, THEM, 5); game[0].setBoard(17, US,   3);
	game[0].setBoard(19, US,   5); game[0].setBoard(24, THEM, 2);
	game[0].setHome(US, 0); game[0].setHome(THEM, 0);
	game[0].setBar(US, 0); game[0].setBar(THEM, 0);
	game[0].setDice(US  , 0, 0); game[0].setDice(US  , 1, 0);
	game[0].setDice(THEM, 0, 0); game[0].setDice(THEM, 1, 0);	

	/*
	 * save backup of the game state
	 */
	game[1] = game[0];

	emit allowCommand(Load, true);
}

/*
 * Open a dialog to query for the name of player w. Return true unless
 * the dialog was canceled.
 */
bool KBgEngineOffline::queryPlayerName(int w)
{
	bool ret = false;
	QString *name;
	QString text;
	
	if (w == US) {
		name = &nameUS;
		text = i18n("Please enter the nickname of the player whose home\n"
			    "is in the lower half of the board.");
	} else {
		name = &nameTHEM;
		text = i18n("Please enter the nickname of the player whose home\n"
			    "is in the upper half of the board.");
	}

	do {
		*name = KLineEditDlg::getText(text, *name, &ret, (QWidget *)parent());
		if (!ret) break;

	} while (name->isEmpty());
	
	return ret;
}


// == moving ===================================================================

/*
 * Finish the last move - called by the timer and directly by the used
 */
void KBgEngineOffline::done()
{
	commitTimeout->stop();

	emit allowMoving(false);
	emit allowCommand(Done, false);
	emit allowCommand(Undo, false);
	
	if (abs(game->home(lastRoll)) == 15) {

		emit infoText(i18n("%1 wins the game. Congratulations!").arg((lastRoll == US) ? nameUS : nameTHEM));
		gameRunning = false;
		emit allowCommand(Roll, false);
		emit allowCommand(Cube, false);

	} else {
		
		emit allowCommand(Roll, true);
		if (game->cube((lastRoll == US ? THEM : US)) > 0) {

			game[0].setDice(US  , 0, 0); game[0].setDice(US  , 1, 0);
			game[0].setDice(THEM, 0, 0); game[0].setDice(THEM, 1, 0);	

			emit newState(game[0]);
			emit getState(game);

			game[1] = game[0];

			emit infoText(i18n("%1, please roll or double.").arg((lastRoll == THEM) ? nameUS : nameTHEM));
			emit allowCommand(Cube, true);

		} else {

			roll();
			emit allowCommand(Cube, false);

		}
	}
}

/*
 * Undo the last move
 */
void KBgEngineOffline::undo()
{
	commitTimeout->stop();
	
	redoPossible = true;
	++undoCounter;

	emit allowMoving(true);
	emit allowCommand(Done, false);
	emit allowCommand(Redo, true);
	emit undoMove();
}

/*
 * Redo the last move
 */
void KBgEngineOffline::redo()
{
	--undoCounter;
	emit redoMove();
}

/*
 * Take the move string and make the changes on the working copy
 * of the state. 
 */
void KBgEngineOffline::handleMove(QString *s)
{
	int index = 0;
	QString t = s->mid(index, s->find(' ', index));
	index += 1 + t.length();       
	int moves = t.toInt();

	/*
	 * Allow undo and possibly start the commit timer
	 */
	redoPossible &= ((moves < toMove) && (undoCounter > 0));
	emit allowCommand(Undo, moves > 0);
	emit allowCommand(Redo, redoPossible);
	emit allowCommand(Done, moves == toMove);
	if (moves == toMove && commitTimeoutLength) {
		emit allowMoving(false);
		commitTimeout->start(commitTimeoutLength, true);
	}

	/*
	 * Apply moves to game[1] and store results in game[0]	
	 */
	game[0] = game[1];

	/*
	 * process each individual move
	 */
	for (int i = 0; i < moves; i++) {
		bool kick = false;
		t = s->mid(index, s->find(' ', index) - index);
		index += 1 + t.length();
		char c = '-';
		if (t.contains('+')) {
			c = '+';
			kick = true;
		}
		QString r = t.left(t.find(c));
		if (r.contains("bar")) {
			game[0].setBar(lastRoll, abs(game[0].bar(lastRoll)) - 1);
		} else {
			int from = r.toInt();	
			game[0].setBoard(from, lastRoll, abs(game[0].board(from)) - 1);
		}
		t.remove(0, 1 + r.length());
		if (t.contains("off")) {
			game[0].setHome(lastRoll, abs(game[0].home(lastRoll)) + 1);
		} else {
			int to = t.toInt();
			if (kick) {
				game[0].setBoard(to, lastRoll, 0);
				int el = ((lastRoll == US) ? THEM : US);
				game[0].setBar(el, abs(game[0].bar(el)) + 1);
			}
			game[0].setBoard(to, lastRoll, abs(game[0].board(to)) + 1);
		}
	}       
}


// == dice & rolling ===========================================================

/*
 * Roll random dice for the player whose turn it is
 */
void KBgEngineOffline::roll()
{
	rollDice((lastRoll == US) ? THEM : US);
}

/*
 * If possible, roll random dice for player w
 */
void KBgEngineOffline::rollDice(const int w)
{
	if ((lastRoll != w) && rollingAllowed) {
		rollDiceBackend(w, getRandom(), getRandom());
		return;
	} 
	emit infoText(i18n("It's not your turn to roll!"));
}

/*
 * Return a random integer between 1 and 6. According to the man
 * page of rand(), this is the way to go...
 */
int KBgEngineOffline::getRandom()
{
	return 1+random.getLong(6);
}

/*
 * Set the dice for player w to a and b. Reload the board and determine the
 * maximum number of moves
 */
void KBgEngineOffline::rollDiceBackend(const int w, const int a, const int b)
{
	/*
	 * This is a special case that stems from leaving the edit
	 * mode.
	 */
	if (a == 0) 
		return;

	/*
	 * Set the dice and tel the board about the new state
	 */
	game[0].setDice(w, 0, a);
	game[0].setDice(w, 1, b);
	game[0].setDice((w == US) ? THEM : US, 0, 0);
	game[0].setDice((w == US) ? THEM : US, 1, 0);
	game[0].setTurn(w);

	game[1] = game[0];

	lastRoll = w;
	emit newState(game[0]);
	
	/*
	 * No more roling until Done and no Undo yet
	 */
	emit allowCommand(Undo, false);
	emit allowCommand(Roll, false);
	redoPossible = false;
	undoCounter = 0;

	/*
	 * Tell the players how many checkers to move
	 */
	switch (toMove = game->moves()) {
	case -1:
		emit infoText(i18n("Game over!"));
		gameRunning = false;
		emit allowCommand(Roll, false);
		emit allowCommand(Cube, false);
		emit allowMoving(false);
		break;
	case  0:
		emit infoText(i18n("%1, you can't move.").arg((w == US) ? nameUS : nameTHEM));
		if (commitTimeoutLength)
			commitTimeout->start(commitTimeoutLength, true);
		emit allowMoving(false);
		break;
	case  1:
		emit infoText(i18n("%1, please move 1 piece.").arg((w == US) ? nameUS : nameTHEM));
		emit allowMoving(true);
		break;
	default:
		emit infoText(i18n("%1, please move %2 pieces.").arg((w == US) ? nameUS : nameTHEM).arg(toMove));
		emit allowMoving(true);
		break;
	}
}


// == cube =====================================================================

/*
 * Double the cube for the player that can double  - asks player
 */
void KBgEngineOffline::cube()
{
	int w = ((lastRoll == US) ? THEM : US);

	if (rollingAllowed && game->cube(w) > 0) {
		emit allowCommand(Cube, false);
		if (KMessageBox::questionYesNo((QWidget *)parent(), 
					       i18n("%1 has doubled. %2, do you accept the double?").
					       arg((w == THEM) ? nameTHEM : nameUS).
					       arg((w == US) ? nameTHEM : nameUS), 
					       i18n("Doubling"), i18n("Accept"), i18n("Reject")) != KMessageBox::Yes) {
			gameRunning = false;
			emit allowCommand(Roll, false);
			emit allowCommand(Cube, false);
			emit infoText(i18n("%1 wins the game. Congratulations!").arg((w == US) ? nameUS : nameTHEM));
			return;

		}

		emit infoText(i18n("%1 has accepted the double. The game continues").arg((w == THEM) ? nameUS : nameTHEM));
		
		if (game->cube(US)*game->cube(THEM) > 0)
			game->setCube(2, w == THEM, w == US);
		else
			game->setCube(2*game->cube(w), w == THEM, w == US);
		
		emit newState(game[0]);
		emit getState(game);
		
		game[1] = game[0];
		
		roll();
	}
}

/*
 * Double the cube for player w
 */
void KBgEngineOffline::doubleCube(const int w)
{
	dummy = w; // avoid compiler warning
	cube();
}


// == configuration handling ===================================================

/*
 * Put the engine specific details in the setup dialog
 */
void KBgEngineOffline::getSetupPages(KDialogBase *nb)
{
	// avoid compiler warning
	if (!nb) nb = 0;
}

/*
 * Called when the setup dialog is positively closed
 */
void KBgEngineOffline::setupOk()
{
	// do nothing
}
void KBgEngineOffline::setupDefault()
{
	// do nothing
}
void KBgEngineOffline::setupCancel()
{
	// do nothing
}

/*
 * Restore settings
 */
void KBgEngineOffline::readConfig()
{
	KConfig* config = kapp->config();
	config->setGroup("offline engine");

	nameUS   = config->readEntry("player-one", i18n("South"));
	nameTHEM = config->readEntry("player-two", i18n("North"));
	commitTimeoutLength = config->readNumEntry("timer", 2500);
}

/*
 * Save the engine specific settings
 */
void KBgEngineOffline::saveConfig()
{
	KConfig* config = kapp->config();
	config->setGroup("offline engine");

	config->writeEntry("player-one", nameUS  );
	config->writeEntry("player-two", nameTHEM);
	config->writeEntry("timer",      commitTimeoutLength);
}


// == various slots & functions ================================================

/*
 * Check with the user if we should really quit in the middle of a
 * game.
 */
bool KBgEngineOffline::queryClose()
{
	if (!gameRunning)
		return true;
	
	switch (KMessageBox::warningYesNo((QWidget *)parent(),i18n("In the middle of a game. Really quit?"))) {
	case KMessageBox::Yes :
		return TRUE;
	case KMessageBox::No :
		return FALSE;
	default: // cancel
		return FALSE;
	}


	return true;
}

/*
 * Quitting is fine at any time
 */
bool KBgEngineOffline::queryExit()
{
	return true;
}

/*
 * Handle textual commands. Right now, all commands are ignored
 */
void  KBgEngineOffline::handleCommand(const QString& cmd)
{
	emit infoText(i18n("Text commands are not yet working. The command '%1' has been ignored.").arg(cmd));
}

/*
 * Load the last known sane state of the board
 */
void KBgEngineOffline::load()
{
	if (editAction->isChecked())
		emit newState(game[1]);
	else {
		// undo up to four moves
		undo();
		undo();
		undo();
		undo();
	}
}

/*
 * Store if cmd is allowed or not
 */
void KBgEngineOffline::setAllowed(int cmd, bool f)
{
	switch (cmd) {		
	case Roll:
		rollingAllowed = f;
		return;
	case Undo:
		undoPossible = f;
		return;
	case Cube:
		doublePossible = f;
		return;
	case Done:
		donePossible = f;
		return;
	}
}

/*
 * Swaps the used colors on the board
 */
void KBgEngineOffline::swapColors()
{
	game[1].setDice(US,   0, game[0].dice(US,   0));
	game[1].setDice(US,   1, game[0].dice(US,   1));
	game[1].setDice(THEM, 0, game[0].dice(THEM, 0));
	game[1].setDice(THEM, 1, game[0].dice(THEM, 1));
	game[1].setColor(game[1].color(THEM), US);
	emit newState(game[1]);
	emit getState(game+1);
	game[0] = game[1];
}

/*
 * Switch back and forth between edit and play mode
 */
void KBgEngineOffline::toggleEditMode()
{
	emit setEditMode(editAction->isChecked());
	if (editAction->isChecked()) {
		commitTimeout->stop();
		newAction->setEnabled(false);
		swapAction->setEnabled(false);
		emit allowCommand(Undo, false);
		emit allowCommand(Roll, false);
		emit allowCommand(Done, false);
		emit allowCommand(Cube, false);
		emit statText(i18n("%1 vs. %2 - Edit Mode").arg(nameUS).arg(nameTHEM));
	} else {
		newAction->setEnabled(true);
		swapAction->setEnabled(true);
		emit statText(i18n("%1 vs. %2").arg(nameUS).arg(nameTHEM));
		emit getState(game+1);
		game[0] = game[1];
		emit allowCommand(Done, donePossible);
		emit allowCommand(Cube, doublePossible);
		emit allowCommand(Undo, undoPossible);
		emit allowCommand(Roll, rollingAllowed);
		int w =((game[0].dice(US, 0) && game[0].dice(US, 1)) ? US : THEM);
		rollDiceBackend(w, game[0].dice(w, 0), game[0].dice(w, 1));
	}
}

// EOF
