/*
    This file is part of the KDE games library
    Copyright (C) 2000 Martin Heni (martin@heni-online.de)

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.
*/
#ifndef __KCARDDIALOG_H__
#define __KCARDDIALOG_H__

#include <qstring.h>
#include <kdialogbase.h>
#include <qmap.h> // TODO: remove - it is in kcarddialog.cpp now; left here for source compatibility

class QIconViewItem;

class KCardDialogPrivate;

/**
 * The KCardDialog provides a dialog for interactive carddeck selection.
 * It gives cardgames an easy to use interface to select front and
 * back of the card sets. As card sets the KDE default cardsets are
 * offered as well as used specified ones.
 *
 * In most cases, the simplest
 * use of this class is the static method @ref KCardDialog::getCardDeck(),
 * which pops up the dialog, allows the user to select a carddeck, and
 * returns when the dialog is closed. Only if you really need some specific
 * behaviour or if you overwrite the dialog you need all the other access
 * functions.
 *
 * Example:
 *
 * <pre>
 *      QString deck,card;
 *      int result = KCardDialog::getCardDeck(deck,card );
 *      if ( result == KCardDialog::Accepted )
 *            ...
 * </pre>
 *
 * Here you can see a card dialog in action
 * @image kcarddialog.png KCarddialog
 *
 * @short A carddeck selection dialog for card games.
 * @author Martin Heni <martin@heni-online.de>
 * @version $Id: kcarddialog.h 105441 2001-07-09 17:28:27Z andreas $
 */
class KCardDialog : public KDialogBase
{
  Q_OBJECT

public:

  /**
   *  @li @p Both - both are shown
   *  @li @p NoDeck - The deck (back) selection is not shown
   *  @li @p NoCards -  The cards (front) selection is not shown
   */
   enum CardFlags { Both=0, NoDeck=0x01, NoCards=0x02 };

   /**
   * Constructs a card deck selection dialog.
   *
   * @param parent The parent widget of the dialog, if any.
   * @param name The name of the dialog.
   * @param modal Specifies whether the dialog is modal or not.
   *
   */
   KCardDialog (QWidget* parent = NULL,const char* name = NULL,
                CardFlags mFlags = Both);
   /**
   * Destructs a card deck selection dialog.
   *
   */
   ~KCardDialog();

   /**
   * Creates a modal carddeck dialog, lets the user choose a deck,
   * and returns when the dialog is closed.
   *
   * @param deck a reference to the filename used as backside of the
   *        cards. It is an absolute path and can directly be loaded as
   *        pixmap.
   *
   * @param carddir a reference to the directory name used as front of the
   *        cards. The directory contains the card images as 1.png to 52.png
   *
   * @param parent an optional pointer to the parent window of the dialog
   *
   * @param flags what to show
   *
   * @param mask An optinonal filemask for the icons. Per default *.png
   *        images are assumed.
   *
   * @param randomDeck if this pointer is non-zero, *ok is set to TRUE if
   *        the user wants a random deck otherwise to FALSE. Use this in the
   *        config file of your game to load a random deck on startup.
   *        See @ref getRandomDeck()
   *
   * @param randomCardDir if this pointer is non-zero, *ok is set to TRUE if
   *        the user wants a random card otherwise to FALSE.
   *        Use this in the config file of your game to load a random card
   *        foregrounds on startup.
   *        See @ref getRandomCardDir()
   *
   * @return @ref #QDialog::result().
   */
   static int getCardDeck(QString &deck,QString &carddir, QWidget *parent=0,
                          CardFlags flags=Both, bool* randomDeck=0,
                          bool* randomCardDir=0);

   /**
   * Returns the default path to the card deck backsides. You want
   * to use this usually before the user used the card dialog the first
   * time to get a default deck. You can assume that
   * <pre>
   *   getDefaultDeckPath()
   * </pre>
   * is a valid deck.
   *
   * @return The default path
   *
   */
   static QString getDefaultDeck();

   /**
   * Returns the default path to the card frontsides. You want
   * to use this usually before the user used the card dialog the first
   * time to get an default deck. You can assume that
   * <pre>
   *   getCardPath(getDefaultCardPath(), *)
   * </pre>
   * are valid cards for * from 1 to 52.
   *
   * @return returns the path to the card directory
   */
   static QString getDefaultCardDir();

    /**
    * Returns the path to the card frontside in dir @param carddir
    *
    * @param index the card to open
    *
    * @return returns the path to the card
    */
    static QString getCardPath(const QString &carddir, int index);

   /**
    * Returns a random deck in @ref deckPath()
    * @return A random deck
    **/
    static QString getRandomDeck();

   /**
    * Returns a random directory of cards
    * @return A random card dir
    **/
    static QString getRandomCardDir();

   /**
    * Show or hides the "random backside" checkbox
    * @param s Shows the checkbox if true otherwise hides it
    **/
   void showRandomDeckBox(bool s);

   /**
    * Show or hides the "random foreside" checkbox
    * @param s Shows the checkbox if true otherwise hides it
    **/
   void showRandomCardDirBox(bool s);

   /**
   * Returns the choosen deck, which is a valid path to a imagefile.
   *
   * @return The deck
   */
   const QString& deck() const;

   /**
   * Sets the default deck.
   *
   * @param file The full path to an image file
   */
   void setDeck(const QString& file);

   /**
   * Returns the choosen card directory
   *
   * @return The card directory
   */
   const QString& cardDir() const;

   /**
   * Sets the default card directory.
   *
   * @param dir The full path to an card directory
   *
   */
   void setCardDir(const QString& dir);

   /**
   * Returns the flags set to the dialog
   *
   * @return the flags
   */
   CardFlags flags() const;

   /**
   * Creates the default widgets in the dialog. Must be called after
   * all flags are set. This is only needed if you do NOT use the
   * @ref #getCardDeck static function which provides all calls for you.
   */
   void setupDialog();

   /**
    * @return TRUE if the selected deck is a random deck (i.e. the user checked
    * the random checkbox) otherwise FALSE
    **/
   bool isRandomDeck() const;

   /**
    * @return TRUE if the selected carddir is a random dir (i.e. the user
    * checked the random checkbox) otherwise FALSE
    **/
   bool isRandomCardDir() const;


protected:
    void insertCardIcons();
    void insertDeckIcons();

    static QString getDeckName(const QString& desktop);

protected slots:
   void slotDeckClicked(QIconViewItem *);
   void slotCardClicked(QIconViewItem *);
   void slotRandomCardDirToggled(bool on);
   void slotRandomDeckToggled(bool on);

private:
   static void init();

   KCardDialogPrivate* d;
};

#endif
