/*
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 2 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

// This file contains some functions i did not want to leave in kpoker.cpp
// they are used to get the winner only

#include <qlist.h>

#include "kpoker.h"
#include "player.h"
#include "defines.h"

void kpok::winner()
{ //diplay winner and give money and so on
 showComputerCards();

 QList<Player> testPlayers = player;

 for (int unsigned i = 0; i < testPlayers.count(); i++) {
	bool out = false;
	for (int unsigned j = i + 1; !out && j < testPlayers.count(); j++) {
		int resultI = testPlayers.at(i)->sortedResult();
		int resultJ = testPlayers.at(j)->sortedResult();
		if (resultI < resultJ) {
			testPlayers.remove(testPlayers.at(j));
			j--;
		}
		else if (resultI > resultJ) {
			testPlayers.remove(testPlayers.at(i));
			i--;
			out = true;
		}
	}
 }

//have some players the same points? if so then continue checking
//TODO: merge with above
 for (int testRun = 0; testPlayers.count() > 1 && testRun < CARDS; testRun++) {
	for (int unsigned i = 0; i < testPlayers.count(); i++) {
		bool out = false;
		for (int unsigned j = i+1; !out && j < testPlayers.count(); j++) {
			// should both be the same:
			int resultI = testPlayers.at(i)->sortedResult();
			int resultJ = testPlayers.at(j)->sortedResult();
			for (int k = 0; k <= testRun; k++) {
				resultI += testPlayers.at(i)->findBestButFoundCard();
				resultJ += testPlayers.at(j)->findBestButFoundCard();
			}
			if (resultI < resultJ) {
				testPlayers.remove(testPlayers.at(j));
				j--;
			}
			else if (resultI > resultJ) {
				testPlayers.remove(testPlayers.at(i));
				i--;
				out = true;
			}
		}
	}
 }

// TODO: merge with above
 if (testPlayers.count() > 1) { // still in draw??? players seem to have the same cards, just different suits... - now we test the bestCard again 
	 for (int unsigned i = 0;  i < testPlayers.count(); i++) {
		bool out = false;
		for (int unsigned j = 0; !out && j < testPlayers.count(); j++) {
			int pointsI = getSuitePoints(testPlayers.at(i)->findBestCard() % 4);
			int pointsJ = getSuitePoints(testPlayers.at(j)->findBestCard() % 4);
			if (pointsI < pointsJ) {
				testPlayers.remove(testPlayers.at(j));
				j--;
			}
			else if (pointsI > pointsJ) {
				testPlayers.remove(testPlayers.at(i));
				i--;
				out = true;
			}
		}
	}
 }
 
 displayWinner_Computer(player.at(player.find(testPlayers.at(0))));
 testPlayers.clear();

}

int kpok::getSuitePoints(int suite)
{
 // which suite shall win?
 // i guess:
 // 1. hearts
 // 2. spades
 // 3. diamonds
 // 4. clubs

 int points = 0;
 switch (suite) {
	case 0: // diamonds
		points = 3;
		break;
	case 1: // clubs
		points = 4;
		break;
	case 2: // spades 
		points = 2;
		break;
	case 3: // hearts 
		points = 1;
		break;
	default: // error
		points = 100;
		break;
 }
 return points;
}


