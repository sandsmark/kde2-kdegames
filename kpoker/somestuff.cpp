/*
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 2 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

// QT includes
#include <qtooltip.h>
#include <qlabel.h>
#include <qlayout.h>

// KDE includes
#include <kapp.h>
#include <kglobal.h>
#include <kaccel.h>
#include <klocale.h>
#include <kconfig.h>
#include <kcarddialog.h>
#include <kdebug.h>

// own includes
#include "betbox.h"
#include "kpoker.h"
#include "kpaint.h"
#include "defines.h"

extern CardImages* cardImage;

void kpok::initSomeStuff()
{
//init Some Stuff -> called by kpok::kpok only. And only once.

 done = new bool[highestCard];

 blinking = true;

// KKeyCode initialization
 kacc = new KAccel( this );
 kacc->insertItem(i18n("Draw"), "Draw", Key_Return);
 kacc->connectItem("Draw", this, SLOT(drawClick())); // same as drawButton
 kacc->insertItem(i18n("Exchange Card 1"), "ExchangeCard1", Key_1);
 kacc->connectItem("ExchangeCard1", this, SLOT(exchangeCard1()));
 kacc->insertItem(i18n("Exchange Card 2"), "ExchangeCard2", Key_2);
 kacc->connectItem("ExchangeCard2", this, SLOT(exchangeCard2()));
 kacc->insertItem(i18n("Exchange Card 3"), "ExchangeCard3", Key_3);
 kacc->connectItem("ExchangeCard3", this, SLOT(exchangeCard3()));
 kacc->insertItem(i18n("Exchange Card 4"), "ExchangeCard4", Key_4);
 kacc->connectItem("ExchangeCard4", this, SLOT(exchangeCard4()));
 kacc->insertItem(i18n("Exchange Card 5"), "ExchangeCard5", Key_5);
 kacc->connectItem("ExchangeCard5", this, SLOT(exchangeCard5()));

 QFont myFixedFont;
 myFixedFont.setPointSize(12);
 QFont wonFont;
 wonFont.setPointSize(14);
 wonFont.setBold(true);

 topLayout = new QVBoxLayout(this, BORDER);
 QVBoxLayout* topInputLayout = new QVBoxLayout;
 topLayout->addLayout(topInputLayout);

 QHBoxLayout* betLayout = new QHBoxLayout;
 inputLayout = new QHBoxLayout;
 inputLayout->addLayout(betLayout);
 topInputLayout->addLayout(inputLayout);

 drawButton = new QPushButton(this);
 drawButton->setText(i18n("&Draw!"));
 connect(drawButton, SIGNAL(clicked()), this, SLOT(drawClick()));
 inputLayout->addWidget(drawButton);
 inputLayout->addStretch(1);

 QFont waveFont;
 waveFont.setBold(true);
 waveFont.setPointSize(16);
 QFontMetrics tmp(waveFont);
 mWonWidget = new QWidget(this);
 inputLayout->addWidget(mWonWidget, 2);
 mWonWidget->setMinimumHeight(50); //FIXME hardcoded value for the wave
 mWonWidget->setMinimumWidth(tmp.width(i18n("You won %1").arg(KGlobal::locale()->formatMoney(100))) + 20); // workaround for width problem in wave
 QHBoxLayout* wonLayout = new QHBoxLayout(mWonWidget);
 wonLayout->setAutoAdd(true);

 wonLabel = new QLabel(mWonWidget);
 wonLabel->setFont(wonFont);
 wonLabel->setAlignment(AlignCenter);
 wonLabel->hide();
 inputLayout->addStretch(1);

 potLabel = new QLabel(this);
 potLabel->setFont(myFixedFont);
 potLabel->setFrameStyle(QFrame::WinPanel | QFrame::Sunken);
 inputLayout->addWidget(potLabel, 0, AlignCenter);

 clickToHold = new QLabel(this);
 clickToHold->hide();

//	timers
 blinkTimer = new QTimer(this);
 connect( blinkTimer, SIGNAL(timeout()), SLOT(bTimerEvent()) );
 blinkStat=0;

 waveTimer = new QTimer(this);
 connect( waveTimer, SIGNAL(timeout()), SLOT(waveTimerEvent()) );

 drawTimer = new QTimer(this);
 connect (drawTimer, SIGNAL(timeout()), SLOT(drawCardsEvent()) );

// and now the betUp/Down Buttons
 betBox = new BetBox(this, 0);
 betLayout->addWidget(betBox);
 connect(betBox, SIGNAL(betChanged(int)), this, SLOT(betChange(int)));
 connect(betBox, SIGNAL(betAdjusted()), this, SLOT(adjustBet()));
 connect(betBox, SIGNAL(fold()), this, SLOT(out()));


// some tips 
 QToolTip::add(drawButton, i18n("draw new cards"));
 QToolTip::add(potLabel, i18n("the current pot"));

// load all cards into pixmaps first -> in the constructor
 cardImage = new CardImages(this, 0);
 KConfig* conf = kapp->config();
 conf->setGroup("General");
 //TODO random
 if (!conf->readBoolEntry("RandomDeck", true)) {
	loadDeck(conf->readEntry("DeckPath", KCardDialog::getDefaultDeck()));
 } else {
	loadDeck(KCardDialog::getRandomDeck());
 }

 if (!conf->readBoolEntry("RandomCardDir", true)) {
	loadCards(conf->readEntry("CardPath", KCardDialog::getDefaultCardDir()));
 } else {
	loadCards(KCardDialog::getRandomCardDir());
 }

 random.setSeed(0);

 blinkingBox = 0;
}

