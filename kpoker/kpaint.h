/*
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 2 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __CardWidget__
#define __CardWidget__

#include <qpushbutton.h>
#include <qpixmap.h>

class QLabel;

/**
  * This class extends the QPushButton by some methods / variables to provide a card with held labels and so on
  *
  * @short The cards
 **/
class CardWidget : public QPushButton
{
	Q_OBJECT
	    
signals:	
  /**
    * This signal is emitted by @ref ownClick()
    * @param CardWidget is a this pointer
   **/
	void pClicked(CardWidget *);
	
public:
	CardWidget( QWidget *parent=0, const char *name=0 );
	
	/**
		* Paints the deck if cardType = 0 or the card spezified in cardType
		* @param cardType the card to be painted. 0 is the deck
   **/
	void        paintCard(int cardType);
	
	/**
	  * @return The held status of this card
   **/
	bool        queryHeld();
	
	/**
	  * Sets the new held
	  * @param newHeld specifies the new held
	 **/
	void        setHeld(bool newheld);
	
	/**
	  * Sets held = true if it is false or false if it is true
	 **/
	bool        toggleHeld(); /* returns the new value of held*/

	void repaintDeck();
	
	QLabel      *heldLabel;
	
protected slots:
  /**
    * Emits the signal @ref pClicked() when the player clicks on the card
   **/
	void ownClick();
	

private:
	QPixmap     *pm;                     // the loaded pixmap
	bool        held;
};

/**
  * This class loads all pictures, first. So they don't have to be loaded after the start again
  * @short A help class for loading the pictures
 **/
class CardImages : public QWidget
{
	Q_OBJECT
public:
	CardImages( QWidget *parent = 0, const char *name = 0 ); 
	~CardImages();

//	bool        loadCards();
	
	QPixmap     *cardP;
	QPixmap     deck;
};
#endif
