/*
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 2 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */   

// QT includes
#include <qdir.h>
#include <qlabel.h>
#include <qlayout.h>

// KDE includes
#include <kglobal.h>
#include <kstddirs.h>
#include <kiconloader.h>
#include <kapp.h>
#include <kdebug.h>

// own includes
#include "kpaint.h"
#include "defines.h"

// note: this file and both classes in it will be removed in KDE-2.2 

extern CardImages  *cardImage;

CardWidget::CardWidget( QWidget *parent, const char *name )
  : QPushButton( parent, name )
{
  held = false;
  setBackgroundMode( NoBackground ); // disables flickering
  connect(this, SIGNAL(clicked()), this, SLOT(ownClick()));
  setFixedSize(cardWidth, cardHeight);
}

void CardWidget::paintCard(int cardType)
{
  if (cardType==0) {
    pm=&cardImage->deck;
  } else {
    pm=&cardImage->cardP[cardType-1];
  }
  if ( pm->size() != QSize( 0, 0 ) ) {   // is an image loaded?
  	setPixmap(*pm);
  }
}

void CardWidget::repaintDeck()
{ 
 setPixmap(*pm); 
 setFixedSize(cardImage->cardP[0].width(), cardImage->cardP[0].height());
 ((QWidget*)parent())->layout()->invalidate();
 ((QWidget*)parent())->setFixedSize( ((QWidget*)parent())->sizeHint());
 
}

void CardWidget::ownClick()
{
  emit pClicked(this);
}

bool CardWidget::queryHeld() { return held; }

void CardWidget::setHeld(bool newheld) { held = newheld; }

bool CardWidget::toggleHeld()
{
  held = !held;
  return held;
}
/*
bool CardImages::loadCards()
{
 int w;
 int randomDeck;

 QString fname;
 for (w = 0; w < highestCard; w++) {
 	fname = KGlobal::dirs()->findResource("appdata", 
			QString("pics/%1.png").arg(w+1));

 	if(!cardP[w].load(fname)) {
 		kdWarning() << "Fatal error: bitmap pics/"
                            << w + 1
                            << ".png not found - try reading from current dir"
                            << endl;
// 		QDir dir;
//		dir.cd("pics");

 		if(!cardP[w].load(QString("pics/%1.png").arg(w+1))) {
	 		kdWarning() << "Fatal error: bitmap pics/" << w + 1 
					<< ".png not found" << endl;
		
		} else {
	 		kdWarning() << "Found bitmap in pics/" << w + 1 
					<< ".png" << endl;
		}
	}
 }


 randomDeck = (kapp->random() % 4) +1;
 fname = KGlobal::dirs()->findResource("appdata",
		QString("pics/deck%1.png").arg(randomDeck));
 if(!deck.load(fname)) {
 	kdWarning() << "Fatal error: bitmap pics/deck"
                    << randomDeck
                    << ".png not found - try reading from current dir" << endl;
 	QDir dir;
	dir.cd("pics");
	if (!deck.load(dir.absPath() + QString("/deck%1.png").arg(randomDeck))) {
	 	kdWarning() << "Fatal error: bitmap "
                            << dir.absPath()
                            << "/deck"
                            << randomDeck
                            << ".png not found" << endl;

	} else 
 		kdWarning() << "Found bitmap in "
                            << dir.absPath()
                            << "/deck"
                            << randomDeck
                            << ".png" << endl;
 }
 return true;

}*/

CardImages::CardImages(QWidget* parent, const char* name) : QWidget(parent, name)
{
 cardP = new QPixmap[highestCard];
 hide();
// loadCards();
}

CardImages::~CardImages()
{
delete[] cardP;
}

#include "kpaint.moc"
