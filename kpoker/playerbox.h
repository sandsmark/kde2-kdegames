/*
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 2 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#ifndef PLAYERBOX_H
#define PLAYERBOX_H

#include <qgroupbox.h>

class QLabel;

class Player;
class CardWidget;

class PlayerBox : public QGroupBox
{
	Q_OBJECT

public:
	PlayerBox(bool playerOne, QWidget* parent = 0, const char* name = 0);
	~PlayerBox();

	void cardClicked(int no);
	
	/**
	  * Sets the name of the player who owns this box (via localPlayer)
	 **/
	void setName();
	
	/**
	  * Paints the cash
 	 **/
	void paintCash(); // and some more

	/**
	  * Sets the local player which is used e.g. by @ref paintCash()
	  * @param p The guy who owns this box
	 **/
	void setLocalPlayer(Player* p)		{ localPlayer = p; }
	
	/**
	  * Hides the card nr (0 = first card)
	  *
	  * used to let the cards blink
	  * @param nr The number of the card which will be hidden
   **/
	void hideCard(int nr);

	/**
	  * @return The X position of the cash label
	 **/
	int getCashLabelX() const;
	
	/**
	  * @return The Y position of the cash label
	 **/
	int getCashLabelY() const;

	/**
	  * @paramm nr The number of the card (where 0 = first card)
	  * @rerturn If the card nr shall be held
	 **/
	bool getHeld(int nr) const;
	
	/**
	  * shows all held labels or hides them all
	  * @param e Shows all all labels if true and hides them if false
	 **/
	void showHelds(bool e);
	
	/**
	  * Enables the held labels if e is true or disables them (e.g. after exchange phase) if false
	  * @param e Enables held labels if true, disables if false
	 **/
	void setHeldEnabled(bool on);
	
	/**
	  * Paints the card nr
	  * @param nr The number of the card (where 0 = first card)
	 **/
	void paintCard(int nr);
	
	/**
	  * Pains the deck
	  * @param nr The number of the card (where 0 = first card)
	 **/
	void paintDeck(int nr);
	
  /**
    * Starts a one player game
    * @param newCashPerRound The cash that the player has to pay every round
   **/
	void onePlayerGame(int newCashPerRound);
	
	/**
	  * Activates the held labels for this player (human player)
	 **/
	void activateToggleHeld();

	/**
	  * Begins blinking of the winning cards
	 **/
	void blinkOn();	
	
	/**
	  * Stops blinking of the winning cards
	 **/
	void blinkOff();
	
	void repaintCard();

protected:
	virtual void resizeEvent( QResizeEvent* e );

protected slots:
	void cardClicked(CardWidget* );

signals:
	void toggleHeld();

private:
	CardWidget** cardW;
	QLabel** heldLabels;
	bool showLabels;

	QLabel* currentBetLabel;
	QLabel* cashLabel;

	Player* localPlayer;

	bool onePlayer;
	int cashPerRound; // one player game only
};
#endif

