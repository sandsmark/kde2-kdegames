/*
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 2 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __PokerWindow__
#define __PokerWindow__

#include <kmainwindow.h>

class KMenuBar;
class KToolBar;
class KStatusBar;
class kpok;

class PokerWindow : public KMainWindow
{
	Q_OBJECT
private:
	kpok            *_kpok;

	// statusbar elements:
	QLabel		*LHLabel;
	
protected slots:
        void saveProperties(KConfig*);
        void readProperties(KConfig*);
	void setHand(QString newHand, bool lastHand = true);
	void showClickToHold(bool show);
	void statusBarMessage(QString);
	void clearStatusBar();
	void saveOptions();
	void toggleMenubar();
	void toggleStatusbar();
	
public:
	PokerWindow();
	~PokerWindow();

protected:
	virtual bool queryClose();
	bool eventFilter(QObject*, QEvent*);
	void initKAction();
	void readOptions();

private:
	bool clickToHoldIsShown;
};

#endif
