/*
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 2 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include <qtooltip.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qhbox.h>

#include <kglobal.h>
#include <klocale.h>
#include <kdebug.h>

#include "player.h"
#include "playerbox.h"
#include "defines.h"
#include "kpaint.h"

PlayerBox::PlayerBox(bool playerOne, QWidget* parent, const char* name) : QGroupBox(parent, name)
{
 QHBoxLayout* l = new QHBoxLayout(this, PLAYERBOX_BORDERS, PLAYERBOX_HDISTANCEOFWIDGETS);

 cardW = new CardWidget *[CARDS];
 heldLabels = new QLabel *[CARDS];
 QFont myFixedFont;
 myFixedFont.setPointSize(12);

 for (int i = 0; i < CARDS; i++) {
	QVBoxLayout* vl = new QVBoxLayout(0);
	l->addLayout(vl, 0);

	QHBox* cardBox = new QHBox(this);
	vl->addWidget(cardBox, 0);
	cardBox->setFrameStyle(Box | Sunken);
	cardW[i] = new CardWidget(cardBox);
	cardBox->setFixedSize(cardBox->sizeHint());

	if (playerOne) {
		QHBox* b = new QHBox(this);
		heldLabels[i] = new QLabel(b);
		heldLabels[i]->setText(i18n("Held"));
		b->setFrameStyle(Box | Sunken);
		b->setFixedSize(b->sizeHint());
		cardW[i]->heldLabel = heldLabels[i];

		QHBoxLayout* heldLayout = new QHBoxLayout(0);
		heldLayout->addWidget(b, 0, AlignCenter);
		vl->insertLayout(0, heldLayout, 0);
		vl->insertStretch(0, 1);
		vl->addStretch(1);
	}
 }

 QVBoxLayout* vl = new QVBoxLayout;
 l->addLayout(vl);
 vl->addStretch();

 cashLabel = new QLabel(this);
 cashLabel->setFrameStyle(QFrame::WinPanel | QFrame::Sunken);
 cashLabel->setFont(myFixedFont);
 vl->addWidget(cashLabel, 0, AlignHCenter);
 vl->addStretch();

 currentBetLabel = new QLabel(this);
 currentBetLabel->setFrameStyle(QFrame::WinPanel | QFrame::Sunken);
 currentBetLabel->setFont(myFixedFont);
 vl->addWidget(currentBetLabel, 0, AlignHCenter);
 vl->addStretch();

 QToolTip::add(cashLabel, i18n("Money of %1").arg("Player"));//change via setName()

 onePlayer = false;
}

PlayerBox::~PlayerBox()
{

}

void PlayerBox::resizeEvent(QResizeEvent* e)
{
 QGroupBox::resizeEvent(e);

 paintCash();
 setName();
}

void PlayerBox::paintCash()
{
 cashLabel->setText(i18n("Cash: %1").arg(KGlobal::locale()->formatMoney
		(localPlayer->getCash())));

 if (!localPlayer->out()) {
	if (!onePlayer)
		currentBetLabel->setText(i18n("Bet: %1").arg(KGlobal::locale()->
				formatMoney(localPlayer->getCurrentBet())));
	else
		currentBetLabel->setText(i18n("Cash per round: %1").arg
				(KGlobal::locale()->formatMoney(cashPerRound)));
	}
 else
	currentBetLabel->setText(i18n("Out"));
}

void PlayerBox::setName()
{
 setTitle(localPlayer->getName());
 QToolTip::remove(cashLabel);
 QToolTip::add(cashLabel, i18n("Money of %1").arg(localPlayer->getName()));
}

void PlayerBox::showHelds(bool e) 
{
 for (int i = 0; i < CARDS; i++) {
	if (!e) {
 		cardW[i]->heldLabel->hide();
 		cardW[i]->setHeld(e);
	}
	else
		cardW[i]->heldLabel->show();
 }
}

void PlayerBox::paintCard(int nr)
{
 cardW[nr]->paintCard(localPlayer->getCard(nr));
 cardW[nr]->show();
}

void PlayerBox::activateToggleHeld()
{
 for (int i = 0; i < CARDS; i++) {
	 connect(cardW[i], SIGNAL(pClicked(CardWidget*)), this, SLOT(cardClicked(CardWidget*)));
 }
}

void PlayerBox::cardClicked(CardWidget* MyCW)
{
 emit toggleHeld();
 if (showLabels && MyCW->toggleHeld()) 
 	MyCW->heldLabel->show();
 else
 	MyCW->heldLabel->hide();
}


void PlayerBox::paintDeck(int nr)
{
 localPlayer->giveCardBack(nr);
 paintCard(nr);
}

void PlayerBox::blinkOn()
{
 for (int i = 0; i < CARDS; i++) {
	if (localPlayer->getFoundCard(i).cardType != 0) 
		hideCard(localPlayer->getFoundCard(i).cardNum);
 }
}

void PlayerBox::blinkOff()
{
 for (int i = 0; i < CARDS; i++) {
	if (!cardW[i]->isVisible())
		paintCard(i);
 }
// repaintCards(); 
}

void PlayerBox::setHeldEnabled(bool on)
{
 showLabels = on;
 if (!on) {
	for (int i = 0; i < CARDS; i++)
		heldLabels[i]->hide();
 }
}

void PlayerBox::onePlayerGame(int newCashPerRound)
{
 onePlayer = true;
 cashPerRound = newCashPerRound;
}

void PlayerBox::hideCard(int nr)
{
 cardW[nr]->hide();
}

int PlayerBox::getCashLabelX() const { return cashLabel->x(); }
int PlayerBox::getCashLabelY() const { return cashLabel->y(); }

bool PlayerBox::getHeld(int nr) const { return cardW[nr]->queryHeld(); }

void PlayerBox::cardClicked(int no)
{
 cardClicked(cardW[no-1]);
}

void PlayerBox::repaintCard()
{
 for (int i = 0; i < CARDS; i++)
	cardW[i]->repaintDeck();
}

#include "playerbox.moc"
