/*
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 2 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */  

// QT includes
#include<qlayout.h>
#include<qpixmap.h>
#include<qkeycode.h>

// KDE includes
#include <kapp.h>
#include <kmenubar.h>
#include <ktoolbar.h>
#include <kstatusbar.h>
#include <kmessagebox.h>
#include <kconfig.h>
#include <klocale.h>
#include <kstdaccel.h>
#include <kstdaction.h>
#include <kstdgameaction.h>
#include <kaction.h>

// own includes
#include "top.h"
#include "kpoker.h"
#include "defines.h"

#include "version.h"

PokerWindow::PokerWindow() : KMainWindow(0)
{
        setCaption("");
	_kpok = new kpok(this, 0);
	setCentralWidget( _kpok );
	_kpok->show();
	
	clickToHoldIsShown = false;

	LHLabel = new QLabel(statusBar());
	LHLabel->adjustSize();
	connect(_kpok, SIGNAL(changeLastHand(QString, bool)), this, 
			SLOT(setHand(QString, bool)));
	connect(_kpok, SIGNAL(showClickToHold(bool)), this, 
			SLOT(showClickToHold(bool)));
	connect(_kpok, SIGNAL(statusBarMessage(QString)), this, 
			SLOT(statusBarMessage(QString)));
	statusBar()->addWidget(LHLabel, 0, true);
	_kpok->updateLHLabel();
//FIXME: LHLabel is shown twize until the bar is repainted!

	initKAction();
	readOptions();
}

PokerWindow::~PokerWindow()
{
}

void PokerWindow::initKAction()
{
//Game
 KStdGameAction::gameNew(_kpok, SLOT(newGame()), actionCollection());
 KStdGameAction::save(_kpok, SLOT(saveGame()), actionCollection());
 KStdGameAction::quit(this, SLOT(close()), actionCollection());

//Settings
 KStdAction::showMenubar(this, SLOT(toggleMenubar()), actionCollection());
 KStdAction::showStatusbar(this, SLOT(toggleStatusbar()), actionCollection());
 KToggleAction* sound = new KToggleAction(i18n("Soun&d"), 0, _kpok, 
		SLOT(toggleSound()), actionCollection(), "options_sound");
 if (sound->isChecked() != _kpok->getSound())
	_kpok->toggleSound();
 KToggleAction* blinking = new KToggleAction(i18n("&Blinking Cards"), 0, _kpok, 
		SLOT(toggleBlinking()), actionCollection(), "options_blinking");
 if (blinking->isChecked() != _kpok->getBlinking())
	_kpok->toggleBlinking();
 KToggleAction* adjust = new KToggleAction(i18n("&Adjust bet is default"), 0,
		_kpok, SLOT(toggleAdjust()), actionCollection(), 
		"options_adjust");
 if (adjust->isChecked() != _kpok->getAdjust())
	_kpok->toggleAdjust();

 KStdAction::saveOptions(this, SLOT(saveOptions()), actionCollection());
 KStdGameAction::carddecks(_kpok, SLOT(slotCardDeck()), actionCollection());
 KStdAction::preferences(_kpok, SLOT(slotPreferences()), actionCollection());

 createGUI("kpokerui.rc");
}

void PokerWindow::readOptions()
{
 KConfig* conf = kapp->config();
 conf->setGroup("General");

 if (_kpok->getSound() != conf->readBoolEntry("Sound", true))
	(actionCollection()->action("options_sound"))->activate();
 if (_kpok->getBlinking() != conf->readBoolEntry("Blinking", true))
	(actionCollection()->action("options_blinking"))->activate();
 if (_kpok->getAdjust() != conf->readBoolEntry("Adjust", true))
	(actionCollection()->action("options_adjust"))->activate();

 if ( ((KToggleAction*)actionCollection()->action(
		KStdAction::stdName(KStdAction::ShowMenubar)))->isChecked() !=
		conf->readBoolEntry("ShowMenuBar", true))
	(actionCollection()->action(KStdAction::stdName(
			KStdAction::ShowMenubar)))->activate();
 if ( ((KToggleAction*)actionCollection()->action(
		KStdAction::stdName(KStdAction::ShowStatusbar)))->isChecked() !=
		conf->readBoolEntry("ShowStatusbar", true))
	(actionCollection()->action(KStdAction::stdName(
			KStdAction::ShowStatusbar)))->activate();
}

void PokerWindow::toggleMenubar()
{
 if (!menuBar()->isHidden())
	menuBar()->hide();
 else 
	menuBar()->show();
}

void PokerWindow::toggleStatusbar()
{
 if (!statusBar()->isHidden())
	statusBar()->hide();
 else
	statusBar()->show();
}

void PokerWindow::saveProperties(KConfig* conf)
{
// conf->writeEntry("Sound", optionsPopup->isItemChecked(ID_SOUND));
// conf->writeEntry("Blinking", optionsPopup->isItemChecked(ID_BLINKING));
// conf->writeEntry("Adjust", optionsPopup->isItemChecked(ID_ADJUST));
 conf->writeEntry("Sound", ((KToggleAction*)actionCollection()->action(
		"options_sound"))->isChecked());
 conf->writeEntry("Blinking", ((KToggleAction*)actionCollection()->action(
		"options_blinking"))->isChecked());
 conf->writeEntry("Adjust", ((KToggleAction*)actionCollection()->action(
		"options_adjust"))->isChecked());
// and further? - save the game?
 _kpok->saveGame(conf);
}

void PokerWindow::readProperties(KConfig* conf)
{
 bool i;
 i = conf->readBoolEntry("Sound", SOUND_DEFAULT);
 _kpok->setSound(i);
 ((KToggleAction*)actionCollection()->action("options_sound"))->setChecked(i);

 i = conf->readBoolEntry("Blinking", BLINKING_DEFAULT);
 _kpok->setBlinking(i);
 ((KToggleAction*)actionCollection()->action("options_blinking"))->
		setChecked(i);
		
 i = conf->readBoolEntry("Adjust", ADJUST_DEFAULT);
 _kpok->setAdjust(i);
 ((KToggleAction*)actionCollection()->action("options_adjust"))->setChecked(i);

 _kpok->loadGame(conf);
}

bool PokerWindow::queryClose()
{
 switch(KMessageBox::warningYesNoCancel(this, i18n("Do you want to save this game?"))) {
	case KMessageBox::Yes :
		_kpok->saveGame();
		return true;
	case KMessageBox::No :
		return true;
	default :
		return false;
 }
}

void PokerWindow::setHand(QString newHand, bool lastHand)
{
 if (lastHand) 
	LHLabel->setText(i18n("Last hand: ") + newHand);
 else
	LHLabel->setText(i18n("Last winner: ") + newHand);
 LHLabel->adjustSize();
}

void PokerWindow::showClickToHold(bool show)
{
 if (show) {
	statusBar()->clear();
	statusBar()->message(i18n("Click a card to hold it"));
	clickToHoldIsShown = true;
 } else if (clickToHoldIsShown) {
	statusBar()->clear();
	clickToHoldIsShown = false;
 }
}

void PokerWindow::statusBarMessage(QString s)
{
 clearStatusBar();
 statusBar()->message(s);
 clickToHoldIsShown = false;
}

void PokerWindow::clearStatusBar()
{
 if (!clickToHoldIsShown)
	statusBar()->clear();
}

void PokerWindow::saveOptions()
{
 KConfig* conf = kapp->config();
 conf->setGroup("General");

 conf->writeEntry("Sound", ((KToggleAction*)actionCollection()->action(
		"options_sound"))->isChecked());
 conf->writeEntry("Blinking", ((KToggleAction*)actionCollection()->action(
		"options_blinking"))->isChecked());
 conf->writeEntry("Adjust", ((KToggleAction*)actionCollection()->action(
		"options_adjust"))->isChecked());
 conf->writeEntry("ShowMenubar", ((KToggleAction*)actionCollection()->action(
		KStdAction::stdName(KStdAction::ShowMenubar)))->isChecked());
 conf->writeEntry("ShowStatusbar", ((KToggleAction*)actionCollection()->action(
		KStdAction::stdName(KStdAction::ShowStatusbar)))->isChecked());
}

bool PokerWindow::eventFilter(QObject*, QEvent* e)
{
 switch (e->type()) {
	case QEvent::MouseButtonPress:
		if(((QMouseEvent*)e)->button() == RightButton) {
			QPopupMenu* popup = (QPopupMenu*)factory()->container
					("popup", this);
			if (popup)
				popup->popup(QCursor::pos());
			return true;
		} else
			return false;
 	default:
		return false;
 }
}
#include "top.moc"
