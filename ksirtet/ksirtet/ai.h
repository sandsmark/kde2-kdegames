#ifndef TL_AI_H
#define TL_AI_H

#include "common/ai.h"

class TLAI : public AI
{
 Q_OBJECT

 public:
    TLAI(uint thinkTime, uint orderTime);

 private:
	Board *createAIBoard();
};

#endif
