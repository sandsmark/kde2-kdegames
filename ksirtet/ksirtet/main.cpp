#include <klocale.h>

#include "common/main.h"
#include "board.h"
#include "piece.h"
#include "ai.h"
#include "lib/mp_interface.h"


const MPGameInfo GAME_INFO = {
        "005", // multiplayer id (increase when incompatible changes are made
        4,     // max nb local games
        500,   // interval
        TRUE,  // IA allowed
        0, 0   // no setting slots
};

const MPGameInfo &gameInfo()
{
	return GAME_INFO;
}

Board *createBoard(QWidget *parent)
{
	return new TLBoard(TRUE, parent);
}

GPieceInfo *createPieceInfo()
{
	return new TLPieceInfo;
}

AI *createAI()
{
	return  new TLAI(0, 10); // times need tuning ?
}

const MainData MAIN_DATA = {
    "ksirtet",
    I18N_NOOP("KSirtet"),
    I18N_NOOP("KSirtet is a clone of the well-known Tetris game."),
    "http://ksirtet.sourceforge.net"
};

QString removedLabel()
{
    return i18n("Removed lines");
}

const char *highscoresHost()
{
    return "localhost"; // #### fixme
}

int main(int argc, char **argv)
{
    return generic_main(argc, argv, MAIN_DATA);
}
