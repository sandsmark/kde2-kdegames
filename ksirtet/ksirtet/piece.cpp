#include "piece.h"

const TLForm TL_FORMS[TL_NB_FORMS] = {
	{ {  0,  0, -1, -1 },
	  { -1,  0,  0,  1 }, 2 }, // red broken line
	{ {  0,  0,  1,  1 },
	  { -1,  0,  0,  1 }, 2 }, // green broken line
	{ {  0,  0,  0,  0 },
	  { -1,  0,  1,  2 }, 2 }, // blue line
	{ { -1,  0,  1,  0 },
	  {  0,  0,  0, -1 }, 4 }, // orange T
	{ { -1,  0, -1,  0 },
	  {  0,  0,  1,  1 }, 1 }, // violet square
	{ { -1,  0,  0,  0 },
	  { -1, -1,  0,  1 }, 4 }, // backward L
	{ {  1,  0,  0,  0 },
	  { -1, -1,  0,  1 }, 4 }  // real L
};

const QColor TL_COLORS[TL_NB_BLOCK_TYPES] = {
	QColor(200, 100, 100),
	QColor(100, 200, 100),
	QColor(100, 100, 200),
	QColor(200, 200, 100),
	QColor(200, 100, 200),
	QColor(100, 200, 200),
	QColor(218, 170,   0),
	QColor(100, 100, 100)  // gray blocks
};

QPixmap *TLPieceInfo::drawPixmap(uint blockWidth, uint blockHeight,
								 uint blockType, uint,
								 bool lighted) const
{
	QPixmap *pix = new QPixmap(blockWidth, blockHeight);

	QColor col = TL_COLORS[blockType];
	if (lighted) col = col.light();

	QRect r(0, 0, blockWidth, blockHeight);
	pix->fill(col);
	QPainter p(pix);

	p.setPen( col.light() );
	p.moveTo(r.bottomLeft());
	p.lineTo(r.topLeft());
	p.lineTo(r.topRight());

	p.setPen( col.dark() );
	p.moveTo(r.topRight() + QPoint(0,1));
	p.lineTo(r.bottomRight());
	p.lineTo(r.bottomLeft() + QPoint(1,0));

	return pix;
}
