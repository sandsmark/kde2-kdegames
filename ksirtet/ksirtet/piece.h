#ifndef TL_PIECE_H
#define TL_PIECE_H

#include "common/gpiece.h"

const uint TL_NB_BLOCKS      = 4;
const uint TL_NB_FORMS       = 7;
const uint TL_NB_TYPES       = TL_NB_FORMS;
const uint TL_NB_BLOCK_TYPES = TL_NB_FORMS + 1;
const uint TL_NB_BLOCK_MODES = 1;

typedef struct {
	int i[TL_NB_BLOCKS];
	int j[TL_NB_BLOCKS];
	uint nbConfigs;
} TLForm;

extern const TLForm TL_FORMS[TL_NB_FORMS];
extern const QColor TL_COLORS[TL_NB_BLOCK_TYPES];

class TLPieceInfo : public GPieceInfo
{
 public:
	TLPieceInfo() {}

	uint nbBlocks() const { return TL_NB_BLOCKS; }
	uint nbForms()  const { return TL_NB_FORMS;  }
	uint nbTypes()  const { return TL_NB_TYPES;  }

	const int *i(uint form) const          { return TL_FORMS[form].i; }
	const int *j(uint form) const          { return TL_FORMS[form].j; }
	uint value(uint type, uint) const      { return type; }
	uint form(uint type) const             { return type; }
	uint nbConfigurations(uint type) const { return TL_FORMS[type].nbConfigs; }

	QPixmap *drawPixmap(uint blockWidth, uint blockHeight,
						uint blockType, uint blockMode, bool lighted) const;

	uint nbBlockTypes() const { return TL_NB_BLOCK_TYPES; }
	uint nbBlockModes() const { return TL_NB_BLOCK_MODES; }
	uint garbageType() const  { return TL_NB_BLOCK_TYPES-1; }
};

#endif



