#ifndef TL_BOARD_H
#define TL_BOARD_H

#include "common/board.h"

/** Class that implements the management for the "Tetris Like" game */
class TLBoard : public Board
{
 Q_OBJECT
 public:
	TLBoard(bool graphic, QWidget *parent, const char *name=0);
	void copy(const GenericTetris &);

	void init(const InitData &);
	void remove();

 private:
	// standard methods
	void gluePiece();
	bool beforeRemove(bool first);
	AfterRemoveResult afterRemove(bool doAll, bool first);
	uint gift();
	bool putGift(uint nb);
	bool _putGift(uint nb);
	bool needRemoving();
	void removeLine(uint line);
	void moveLine(uint srcLine, uint destLine);
	void partialMoveLine(uint srcLine, uint destLine, int dec);

	QArray<uint> filled;
	uint         addRemoved;
};

#endif
