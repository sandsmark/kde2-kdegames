#include "ai.h"
#include "ai.moc"

#include <klocale.h>

#include "board.h"


const AIElementInfo FULL_LINES
    = { "full lines",  50, 0, 100, true, 1, 1, 4, false };

const AIElementInfo OCC_LINES
    = { "occupied lines", 2, 0, 100, false, 0, 0, 0, true };
const AIElementInfo PEAK_TO_PEAK
    = { "peak-to-peak", 0, 0, 100, false, 0, 0, 0, true };
const AIElementInfo HOLES
    = { "holes", 2, 0, 100, false, 0, 0, 0, true };
const AIElementInfo MEAN
    = { "mean height", 0, 0, 100, false, 0, 0, 0, true };
const AIElementInfo SPACES
    = { "spaces", 0, 0, 100, false, 0, 0, 0, true };

TLAI::TLAI(uint thinkTime, uint orderTime)
: AI(thinkTime, orderTime)
{
	add(i18n("Number of full lines"), FULL_LINES, nbRemoved);
}

Board *TLAI::createAIBoard()
{
	return new TLBoard(FALSE, 0, 0);
}
