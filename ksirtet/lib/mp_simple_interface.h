#ifndef MP_SIMPLE_INTERFACE_H
#define MP_SIMPLE_INTERFACE_H

#include "mp_interface.h"
#include "mp_simple_types.h"


class MPSimpleInterface : public MPInterface
{
 Q_OBJECT

 public:
	MPSimpleInterface(const MPGameInfo &gi, KAccel *kacc,
					  QWidget *parent = 0, const char *name = 0);

    bool isPaused() const { return state==SS_Pause; }

 public slots:
	void start();
	void pause();

 protected:
	virtual void _init(bool server) = 0;
	virtual void _readGameOverData(QDataStream &s) = 0;
	virtual void _sendGameOverData(QDataStream &s) = 0;
	virtual void _showGameOverData(bool server) = 0;
	virtual void _firstInit() = 0;
	virtual void _treatInit() = 0;
	virtual bool _readPlayData() = 0;
	virtual void _sendPlayData() = 0;

 private:
	ServerState state;
	bool        first_init;

	void treatData();
	void treatInit();
	void treatPlay();
	void treatPause(bool pause);
	void treatStop();

	void init(bool server);
	void stop(bool server);
	void dataFromServer(QDataStream &);
};

#endif // MP_SIMPLE_INTERFACE_H
