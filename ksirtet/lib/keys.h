#ifndef KEYS_H
#define KEYS_H

#include <kaccel.h>

#include "mp_interface.h"
#include "types.h"

/** Internal class : store a key action. */
class KeyData
{
 public:
	KeyData(const QString &description, const QString &action,
			const char *member)
	: d(description), a(action), m(member) {}

	QString d, a;
	const char *m;
};

/** Internal class : store an array of key action. */
class KeyConnection
{
 public:
	KeyConnection(KAccel *_kacc) : kacc(_kacc) {}
	~KeyConnection();
	
	void append(const QString &description, const QString &action,
				const char *member);
	
	KAccel *kacc;
	uint size() const                         { return keys.size(); }
	QString action(uint h, uint i) const      { return prefix(h) + keys[i]->a; }
	QString description(uint h, uint i) const { return prefix(h) + keys[i]->d; }
	const char *member(uint i) const          { return keys[i]->m; }
	
	int findAction(const QString &action) const;
	
 private:
	QArray<KeyData *> keys;
	
	static QString prefix(uint h) { return QString("%1_").arg(h+1); }
};

/** Internal class : store a key configuration for a given number of human players. */
class KeyConfiguration
{
 public:
	KeyConfiguration(uint nbHumans, KeyConnection &kn);

	void init(const QArray<const char **> &defaultKeys);
	void init(const KeyConfiguration *kf);

	void insert();
	void configure();
	void connect(uint h, QObject *receiver);
	void activateBoard(uint h, bool activate);
	void activateAction(const QString &action, bool activate);
	void remove();
	
 private:
	KeyConnection &kn;
	uint           nbHumans;
	QArray<uint>   codes;
	
	uint &code(uint h, uint i) const { return codes[h*kn.size()+i]; }
};

#endif // KEYS_H
