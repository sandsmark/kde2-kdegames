#ifndef FIELD_H
#define FIELD_H

#include <qwidget.h>
#include <qlcdnumber.h>
#include <qlabel.h>
#include <qpushbutton.h>
#include <qlayout.h>
#include <kprogress.h>

#include "lib/mp_simple_board.h"
#include "board.h"

class ShowNextPiece;
class GiftShower;
class Shadow;

Board *createBoard(QWidget *);

class Field : public MPSimpleBoard
{
 Q_OBJECT

 public:
    Field(QWidget *parent = 0, const char *name = 0);

	QSizePolicy sizePolicy() const;

	void hideButton() { pb->hide(); }

	enum PreferenceType { ShowNext, ShowShadow, Animations };

 public slots:
	void pMoveLeft() { board->pMoveLeft(); };
	void pMoveRight() { board->pMoveRight(); };
	void pDropDown() { board->pDropDown(); };
	void pOneLineDown() { board->pOneLineDown(); };
	void pRotateLeft() { board->pRotateLeft(); };
	void pRotateRight() { board->pRotateRight(); };

	void enablePreference(Field::PreferenceType type, bool enabled);
	void setBlockSize(uint size) { setBlockSizeSignal(size); }
	void AIConfigChanged();

 signals:
	void setBlockSizeSignal(uint);

 private slots:
    void updateScore(int);

 private:
    Board         *board;
    ShowNextPiece *showNext;
    QLCDNumber    *showScore, *showLines, *showLevel;
    QLabel        *prevName, *nextName, *labShowNext;
    KProgress     *prevHeight, *nextHeight;
	GiftShower    *giftReceived, *giftSent;
	Shadow        *shadow;

	bool           button, multiplayers;
    uint           firstScore, lastScore;

	QPushButton   *pb;
	QLabel        *msg;

	void midButton(const QString &pt = QString::null,
				   const QString &mt = QString::null,
				   bool start = TRUE);
	void showOpponents(bool show);

	void _init(bool AI, bool multiplayer, bool server, bool first,
			   const QString &name);
	void _initFlag(QDataStream &s);
	void _playFlag(QDataStream &s);
	void _pauseFlag(bool pause);
	void _stopFlag(bool gameover);
	void _dataOut(QDataStream &s);
	void _gameOverDataOut(QDataStream &s);
	void _initDataOut(QDataStream &s);
};

#endif // FIELD_H
