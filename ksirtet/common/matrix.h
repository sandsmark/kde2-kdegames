#ifndef MATRIX_H
#define MATRIX_H

#include <qarray.h>

template <class Type>
class Matrix
{
 public:
	Matrix(uint width, uint height)	: w(width), h(height), a(w*h) {}

	uint width() const { return w; }
	uint height() const { return h; }
	const Type &operator ()(uint i, uint j) const { return a[i + j*w]; }
	Type &operator ()(uint i, uint j)             { return a[i + j*w]; }
	Matrix<Type> &operator =(const Matrix<Type> &m) {
			w = m.w;
			h = m.h;
			a = m.a.copy(); // deep copy
			return *this;
	}
	void fill(const Type &t) { a.fill(t); }
	
 private:
	uint         w, h;
	QArray<Type> a;
	
	Matrix(const Matrix<Type> &); // disabled
};

#endif








