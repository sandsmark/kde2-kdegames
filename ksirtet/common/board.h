#ifndef BOARD_H
#define BOARD_H

#include "gboard.h"


class GiftPool;
class AI;

AI *createAI();

class Board : public GenericBoard
{
 Q_OBJECT
 public:
    Board(uint width, uint height, bool graphical, GiftPool *,
		  QWidget *parent, const char *name = 0);
	virtual ~Board();

	void setType(bool computer);
	virtual void init(const InitData &);
	void unpause();
	void stop();

	virtual uint gift() = 0;
	virtual bool needRemoving() = 0;
	virtual void remove() = 0;

	GiftPool *giftPool() const  { return _giftPool; }
	BlockInfo *next() const { return _next; }

	void AIConfigChanged();

 public slots:
    void pMoveLeft();
    void pMoveRight();
    void pDropDown();
    void pOneLineDown();
    void pRotateLeft();
    void pRotateRight();

    void setBlockSize(uint);

 private slots:
    bool timeout();

 signals:
    void updateLevelSignal(int level);

 protected:
	// standard methods
	void newPiece();
	void pieceDropped(uint dropHeight);
	virtual bool beforeGlue(bool bump, bool first);
	virtual void gluePiece();
	virtual bool afterGlue(bool /*doAll*/)                { return false; }
	virtual bool afterGift()                              { return false; }
	virtual bool putGift(uint) = 0;

    void updateLevel(uint newLlevel);

	uint            loop;
	KRandomSequence randomGarbage;

 private:
    void checkGift();
	void _afterGift();
	void _beforeGlue(bool first);
	void _afterGlue();
    void _afterAfterRemove() { checkGift(); }
	bool startTimer();
	void showBoard(bool show);

	uint              _dropHeight;
	GiftPool         *_giftPool;
	AI               *aiEngine;
	BlockInfo        *_next;
};

#endif
