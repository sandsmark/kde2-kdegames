#include "main.h"
#include "main.moc"

#include <kglobal.h>
#include <klocale.h>
#include <kcmdlineargs.h>
#include <kstdaction.h>
#include <kaction.h>
#include <kaboutdata.h>
#include <kmenubar.h>
#include <kstdgameaction.h>

#include "inter.h"
#include "version.h"
#include "gpiece.h"
#include "dialogs.h"


MainWidget::MainWidget()
    : KMainWindow(0)
{
	installEventFilter(this);
	KAccel *kacc = new KAccel(this);

	pieceInfo = createPieceInfo();
	inter = new Interface(kacc, this);
	inter->installEventFilter(this);
	inter->singleHuman();
    connect(inter, SIGNAL(settingsChanged()), SLOT(settingsChanged()));

	// File & Popup
	KStdGameAction::gameNew(inter, SLOT(start()), actionCollection());
    KToggleAction *a
        = KStdGameAction::pause( inter, SLOT(pause()), actionCollection());
    a->setEnabled(false);
    KStdGameAction::highscores(inter, SLOT(showHighScores()),
                               actionCollection());
	KAction *action
        = KStdGameAction::print(this, SLOT(print()), actionCollection());
	action->setEnabled(FALSE);
	KStdGameAction::quit(qApp, SLOT(quit()), actionCollection());

	// Multiplayers
	(void)new KAction(i18n("Single Human"), 0, inter, SLOT(singleHuman()),
					  actionCollection(), "mp_single_human");
	(void)new KAction(i18n("Human vs Human"), 0, inter, SLOT(humanVsHuman()),
					  actionCollection(), "mp_human_vs_human");
	(void)new KAction(i18n("Human vs &Computer"), 0,
					  inter, SLOT(humanVsComputer()),
					  actionCollection(), "mp_human_vs_computer");
	(void)new KAction(i18n("More..."), 0, inter, SLOT(dialog()),
					  actionCollection(), "mp_more");

	// Settings
	KStdAction::showMenubar(this, SLOT(toggleMenubar()), actionCollection());
	KStdAction::preferences(inter, SLOT(configureSettings()),
                            actionCollection());
	KStdAction::keyBindings(inter, SLOT(configureKeys()), actionCollection());
	(void)new KAction(i18n("Configure AI..."), 0, inter, SLOT(configureAI()),
					  actionCollection(), "options_configure_ai");

	createGUI();
	readSettings();
	setCentralWidget(inter);
}

MainWidget::~MainWidget()
{
    delete inter;
	delete pieceInfo;
}

bool MainWidget::eventFilter(QObject *, QEvent *e)
{
	QPopupMenu *popup;
	switch (e->type()) {
	 case QEvent::MouseButtonPress:
		if ( ((QMouseEvent *)e)->button()!=RightButton ) return FALSE;
		popup = (QPopupMenu*)factory()->container("popup", this);
		if (popup) popup->popup(QCursor::pos());
		return true;
	case QEvent::LayoutHint:
		setFixedSize(minimumSize()); // because QMainWindow and KMainWindow
		                             // do not manage fixed central widget and
		                             // hidden menubar ...
		return false;
	 default : return false;
	}
}

#define MENUBAR_ACTION \
    ((KToggleAction *)action(KStdAction::stdName(KStdAction::ShowMenubar)))

#define PAUSE_ACTION ((KToggleAction *)action("game_pause"))

void MainWidget::readSettings()
{
	bool visible = OptionDialog::readMenuVisible();
	MENUBAR_ACTION->setChecked(visible);
	toggleMenubar();
}

void MainWidget::settingsChanged()
{
    //pauseFocus = OptionDialog::readPauseFocus(); #### TODO
}

void MainWidget::toggleMenubar()
{
	bool b = MENUBAR_ACTION->isChecked();
	if (b) menuBar()->show();
	else menuBar()->hide();
	OptionDialog::writeMenuVisible(b);
}

void MainWidget::print()
{
	// #### TODO
}

//-----------------------------------------------------------------------------
int generic_main(int argc, char **argv, const MainData &data)
{
	KLocale::setMainCatalogue("ksirtet");
	KAboutData aboutData(data.name, data.trName, LONG_VERSION,
                         data.description, KAboutData::License_GPL,
                         COPYLEFT, 0, data.homepage);
    aboutData.addAuthor("Nicolas Hadacek", 0, "hadacek@kde.org");
	aboutData.addAuthor("Eirik Eng", I18N_NOOP("Core engine"));
	KCmdLineArgs::init(argc, argv, &aboutData);

	KApplication a;
	KGlobal::locale()->insertCatalogue("multiplayers");
    if ( a.isRestored() ) RESTORE(MainWidget)
    else {
        MainWidget *mw = new MainWidget;
        a.setMainWidget(mw);
        mw->show();
    }
	return a.exec();
}
