#ifndef HIGHSCORES_H
#define HIGHSCORES_H

#include "ghighscores.h"


QString removedLabel();

//-----------------------------------------------------------------------------
class ExtScore : public Score
{
 public:
    ExtScore(uint score = 0, uint level = 1, uint nbRemovedLines = 0);

    uint level() const { return data("level").toUInt(); }
};

//-----------------------------------------------------------------------------
class ExtPlayerInfos : public PlayerInfos
{
 public:
    ExtPlayerInfos();
};

//-----------------------------------------------------------------------------
class ShowMultiplayersScores : public KDialogBase, public ShowScores
{
 public:
    ShowMultiplayersScores(const QList<ExtScore> &, QWidget *parent);

 private:
    const QList<ExtScore> &_scores;

    bool showColumn(const ItemBase *) const;
    QString itemText(const ItemBase *, uint row) const;
};

#endif
