#include "misc_ui.h"
#include "misc_ui.moc"

#include <qtimer.h>

#include "gpiece.h"
#include "board.h"
#include "defines.h"


/*****************************************************************************/
ShowNextPiece::ShowNextPiece(BlockInfo *_bi, QWidget *parent,
							 const char *name)
: QFrame(parent, name), bi(_bi)
{
	setFrameStyle( QFrame::Panel | QFrame::Sunken );
	view = new QCanvasView(_bi, this);
	view->move(frameWidth(), frameWidth());
}

QSize ShowNextPiece::sizeHint() const
{
	return QSize(2*frameWidth() + bi->width()+5,
				 2*frameWidth() + bi->height()+5);
}

QSizePolicy ShowNextPiece::sizePolicy() const
{
	return QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
}

void ShowNextPiece::resizeEvent(QResizeEvent *)
{
	view->resize(width() - 2*frameWidth(), height() - 2*frameWidth());
}

/*****************************************************************************/
Shadow::Shadow(const Board *board, int xOffset, QWidget *parent,
               const char *name)
    : QWidget(parent, name), _xOffset(xOffset), _board(board), _show(false)
{}

QSize Shadow::sizeHint() const
{
	return QSize(_xOffset + _board->matrix().width() * _board->blockWidth(),
				 SHADOW_HEIGHT);
}

QSizePolicy Shadow::sizePolicy() const
{
	return QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
}

void Shadow::setDisplay(bool show)
{
    _show = show;
    update();
}

void Shadow::paintEvent(QPaintEvent *)
{
	if ( !_show ) return;

	const Piece *piece = _board->currentPiece();
	uint pf = piece->minX() + _board->currentCol();
	uint pl = pf + piece->width() - 1;

    QPainter p(this);
    p.setBrush(black);
    p.setPen(black);
	for (uint i=pf; i<=pl; i++) {
		QRect r(_xOffset + i * _board->blockWidth() + 1 , 0,
				_board->blockWidth() - 2, SHADOW_HEIGHT);
		p.drawRect(r);
	}
}

/*****************************************************************************/
GiftShower::GiftShower(uint timeout, QWidget *parent, const char *name)
: QWidget(parent, name), _timeout(timeout), _state(FALSE)
{}

QSize GiftShower::sizeHint() const
{
	return QSize(GI_WIDTH, GI_HEIGHT);
}

QSizePolicy GiftShower::sizePolicy() const
{
	return QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
}

void GiftShower::set()
{
	if (_state) return;
	_state = TRUE;
	repaint();
	QTimer::singleShot(_timeout, this, SLOT(timeout()));
}

void GiftShower::timeout()
{
	_state = FALSE;
	repaint();
}

void GiftShower::paintEvent(QPaintEvent *)
{
	QPainter p(this);
	p.setBrush((_state ? yellow : black));
	p.setPen(black);
	p.drawRect(0, (height()-ARROW_HEIGHT)/2, ARROW_WIDTH, ARROW_HEIGHT);
	QPointArray a(3);
	a.setPoint(0, ARROW_WIDTH, 0);
	a.setPoint(1, width()-1, height()/2);
	a.setPoint(2, ARROW_WIDTH, height()-1);
	p.drawPolygon(a);
}

/*****************************************************************************/
// #### FIXME after KLed fixes commit
class Led : public QWidget // KLed
{
 public:
	Led(const QColor &c, QWidget *parent, const char *name=0)
		: QWidget(parent, name), col(c), _on(FALSE) {}
//		: KLed(c, Off, flat, Rectangular, parent, name) {}

	QSizePolicy sizePolicy() const
		{ return QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed); }
	QSize sizeHint() const { return QSize(LED_WIDTH, LED_HEIGHT); }

	void on()  { if (!_on) { _on = TRUE; repaint(); } }
	void off() { if (_on)  {_on = FALSE; repaint(); } }
	void setColor(const QColor &c) { if (c!=col) { col = c; repaint(); } }

 protected:
	void paintEvent(QPaintEvent *) {
		QPainter p(this);
		p.setBrush((_on ? col.light() : col.dark()));
		p.setPen(black);
		p.drawEllipse(0, 0, width(), height());
	}

 private:
	QColor col;
	bool   _on;
};

GiftPool::GiftPool(uint nbLeds, uint m,
				   QWidget *parent, const char *name)
: QHBox(parent, name), _timeout(GIFT_POOL_TIMEOUT), nb(0), max(m), ready(false)
{
	setSpacing(LED_SPACING);
	leds.resize(nbLeds);
	for (uint i=0; i<nbLeds; i++) leds[i] = new Led(yellow, this);
}

QSize GiftPool::sizeHint() const
{
	QSize s = (leds.size() ? leds[0]->sizeHint() : QSize());
	return QSize((s.width()+LED_SPACING)*leds.size()-LED_SPACING, s.height());
}

QSizePolicy GiftPool::sizePolicy() const
{
	return QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
}

void GiftPool::put(uint n)
{
	if ( n==0 ) return;
	if ( nb==0 && !ready ) QTimer::singleShot(_timeout, this, SLOT(timeout()));
	uint e = QMIN(nb+n, leds.size());
	for (uint i=nb; i<e; i++) leds[i]->on();
	uint f = QMIN(nb+n-e, leds.size());
	for (uint i=0; i<f; i++) leds[i]->setColor(red);
	nb += n;
}

uint GiftPool::take()
{
	ASSERT(ready);
	for (uint i=0; i<leds.size(); i++) {
		leds[i]->setColor(yellow);
		leds[i]->off();
	}
	if ( nb>max ) {
		uint p = nb - max;
		nb = 0;
		put(p);
		return max;
	} else {
		ready = FALSE;
		uint t = nb;
		nb = 0;
		return t;
	}
}
