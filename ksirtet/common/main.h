#ifndef MAIN_H
#define MAIN_H

#include <kmainwindow.h>


//-----------------------------------------------------------------------------
class Interface;
class GPieceInfo;

class MainWidget : public KMainWindow
{
 Q_OBJECT

 public:
    MainWidget();
	~MainWidget();

 private slots:
	void toggleMenubar();
    void settingsChanged();
	void print();

 protected:
	bool eventFilter(QObject *, QEvent *);
//    void focus0utEvent(QFocusEvent *);

 private:
	Interface  *inter;
	GPieceInfo *pieceInfo;
//    bool        pauseFocus; #### TODO

	void readSettings();
};

//-----------------------------------------------------------------------------
struct MainData {
    const char *name, *trName, *description, *homepage;
};

int generic_main(int argc, char **argv, const MainData &);
GPieceInfo *createPieceInfo();

#endif
