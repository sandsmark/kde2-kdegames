#ifndef DEFINES_H
#define DEFINES_H

#include <qglobal.h>

#define BORDER         10
#define SEPARATOR      10
#define PROGRESS_WIDTH 10

#define GIFT_SHOWER_TIMEOUT  800
#define GIFT_POOL_TIMEOUT   2000

#define SHADOW_HEIGHT 10

#define GI_WIDTH     15
#define GI_HEIGHT    11
#define ARROW_HEIGHT  3
#define ARROW_WIDTH   7

#define LED_WIDTH  15
#define LED_HEIGHT 15

#define LED_SPACING 5

extern const uint MIN_BLOCK_SIZE;
extern const uint MAX_BLOCK_SIZE;
extern const bool BLOCK_SIZE_MUST_BE_ODD;

#define NB_HS_ENTRIES 10

#endif
