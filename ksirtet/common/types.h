#ifndef TYPES_H
#define TYPES_H

#include <qdatastream.h>

typedef struct { Q_UINT8 height, gift, end; } ClientPlayData;
QDataStream &operator <<(QDataStream &s, const ClientPlayData &d);
QDataStream &operator >>(QDataStream &s, ClientPlayData &d);

typedef struct { Q_UINT8 prevHeight, nextHeight, gift; } ServerPlayData;
QDataStream &operator <<(QDataStream &s, const ServerPlayData &d);
QDataStream &operator >>(QDataStream &s, ServerPlayData &d);

class ServerInitData
{
 public:
	QString  prevName, nextName, name;
	Q_UINT32 initLevel, seed;
};
QDataStream &operator <<(QDataStream &s, const ServerInitData &d);
QDataStream &operator >>(QDataStream &s, ServerInitData &d);

#endif
