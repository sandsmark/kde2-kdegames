#ifndef MISC_UI_H
#define MISC_UI_H

#include <qframe.h>
#include <qcanvas.h>
#include <qpainter.h>
#include <qhbox.h>
#include <kled.h>

class Board;
class BlockInfo;

/*****************************************************************************/
class ShowNextPiece : public QFrame
{
 Q_OBJECT

 public:
    ShowNextPiece(BlockInfo *, QWidget *parent=0, const char *name=0);

	QSize sizeHint() const;
	QSizePolicy sizePolicy() const;

 public slots:
    void blockSizeChanged() { updateGeometry(); }

 protected:
	void resizeEvent(QResizeEvent *);

 private:
	const BlockInfo *bi;
	QCanvasView     *view;
};

/*****************************************************************************/
class Shadow : public QWidget
{
 Q_OBJECT

 public:
    Shadow(const Board *, int xOffset, QWidget *parent, const char *name=0);

	QSize sizeHint() const;
	QSizePolicy sizePolicy() const;

    void setDisplay(bool show);

 public slots:
	void blockSizeChanged() { updateGeometry(); }

 protected:
	void paintEvent(QPaintEvent *);

 private:
	int          _xOffset;
	const Board *_board;
    bool         _show;
};

/****************************************************************************/
class GiftShower : public QWidget
{
 Q_OBJECT

 public:
    GiftShower(uint timeout, QWidget *parent, const char *name=0);

	QSize sizeHint() const;
	QSizePolicy sizePolicy() const;

 public slots:
    void set();

 protected:
	void paintEvent(QPaintEvent *);

 private slots:
    void timeout();

 private:
	uint _timeout;
	bool _state;
};

/*****************************************************************************/
class Led;

class GiftPool : public QHBox
{
 Q_OBJECT

 public:
    GiftPool(uint nbLeds, uint maxToSend,
			 QWidget *parent, const char *name=0);

	QSize sizeHint() const;
	QSizePolicy sizePolicy() const;

	void put(uint);
	uint take();
	bool pending() const { return ready; }

 private slots:
    void timeout() { ready = TRUE; }

 private:
	QArray<Led *> leds;
	uint          _timeout, nb, max;
	bool          ready;
};

#endif
