#include "inter.h"
#include "inter.moc"

#include <qtimer.h>

#include <klocale.h>
#include <kapp.h>

#include "dialogs.h"
#include "version.h"
#include "defines.h"
#include "ai.h"


Interface::Interface(KAccel *kacc, QWidget *parent, const char *name)
: MPSimpleInterface(::gameInfo(), kacc, parent, name)
{
	setAction(i18n("Move left"),     "Move left",     SLOT(pMoveLeft()));
	setAction(i18n("Move right"),    "Move right",    SLOT(pMoveRight()));
	setAction(i18n("Drop down"),     "Drop down",     SLOT(pDropDown()));
	setAction(i18n("One line down"), "One line down", SLOT(pOneLineDown()));
	setAction(i18n("Rotate left"),   "Rotate left",   SLOT(pRotateLeft()));
	setAction(i18n("Rotate right"),  "Rotate right",  SLOT(pRotateRight()));

	QArray<const char **> defaultKeys(1);
	const char *oneHumanKeys[6]
		= { "Left", "Right", "Down", "Shift", "Up", "Return" };
	defaultKeys[0] = oneHumanKeys;
	setKeysConfiguration(defaultKeys);

	defaultKeys.resize(2);
	const char *twoHumansKeys[6] = { "F", "G", "D", "Space", "E", "Alt" };
	defaultKeys[0] = twoHumansKeys;
	defaultKeys[1] = oneHumanKeys;
	setKeysConfiguration(defaultKeys);

	kacc->readSettings(); // keys settings

    gameOverData.setAutoDelete(true);
}

MPBoard *Interface::newBoard(uint)
{
	Field *f = new Field(this);
	connect(this, SIGNAL(enablePreference(Field::PreferenceType, bool)),
			f, SLOT(enablePreference(Field::PreferenceType, bool)));
	connect(this, SIGNAL(AIConfigChangedSignal()), f, SLOT(AIConfigChanged()));
	connect(this, SIGNAL(setBlockSize(uint)), f, SLOT(setBlockSize(uint)));
	setPreferences();
	return f;
}

void Interface::configureSettings()
{
	OptionDialog od(this);
	if ( !od.exec() ) return;
	setPreferences();
}

void Interface::setPreferences()
{
	emit enablePreference(Field::ShowShadow, OptionDialog::readShadow());
	emit enablePreference(Field::ShowNext, OptionDialog::readShowNext());
	emit enablePreference(Field::Animations, OptionDialog::readAnimations());
	emit setBlockSize(OptionDialog::readBlockSize());
    emit settingsChanged();
}

void Interface::configureAI()
{
	AI *ai = createAI();
	if ( ai->configDialog(this) ) emit AIConfigChangedSignal();
	delete ai;
}

void Interface::showHighScores()
{
    if ( !isPaused() ) pause();
    ExtScore scoreDummy;
    ExtPlayerInfos playerDummy;
	ShowHighscores hs(-1, this, scoreDummy, playerDummy);
	hs.exec();
}

void Interface::_init(bool server)
{
	if (server) data.resize(nbPlayers());
}

bool Interface::_readPlayData()
{
	bool end = FALSE;
	for (uint i=0; i<nbPlayers(); i++) {
		readingStream(i) >> data[i];
		if (data[i].end) end = TRUE;
	}
	return end;
}

void Interface::_sendPlayData()
{
	ServerPlayData sd;
	for(uint i=0; i<nbPlayers(); i++) {
		sd.prevHeight = data[prev(i)].height;
		sd.nextHeight = data[next(i)].height;
		sd.gift       = data[prev(i)].gift;
		writingStream(i) << sd;
	}
}

void Interface::_showGameOverData(bool server)
{
	if (server && nbPlayers()==1 ) {
        ExtPlayerInfos infos;
        int localRank = infos.submitScore(*gameOverData.first(), this);
		if ( localRank==-1 ) return;
		ShowHighscores hs(localRank, this, *gameOverData.first(), infos);
		hs.exec();
	} else {
		ShowMultiplayersScores ms(gameOverData, this);
		ms.exec();
	}
}

uint Interface::prev(uint i) const
{
	if ( i==0 ) return nbPlayers()-1;
	else return i-1;
}

uint Interface::next(uint i) const
{
	if ( i==(nbPlayers()-1) ) return 0;
	else return i+1;
}

// server only
void Interface::_firstInit()
{
	// #### TODO (?)
}

void Interface::_treatInit()
{
	ServerInitData sid;
	sid.seed = kapp->random();
	sid.initLevel = OptionDialog::readInitLevel();
	for (uint i=0; i<nbPlayers(); i++) {
		sid.prevName = playerName(prev(i));
		sid.nextName = playerName(next(i));
		sid.name     = playerName(i);
		writingStream(i) << sid;
	}
}

void Interface::_sendGameOverData(QDataStream &s)
{
    s << (Q_UINT32)nbPlayers();
	gameOverData.clear();
	for (uint i=0; i<nbPlayers(); i++) {
        ExtScore *score = new ExtScore;
		readingStream(i) >> *score;
        score->setName( playerName(i) );
		gameOverData.append(score);
        s << *score;
	}
}

// client only
void Interface::_readGameOverData(QDataStream &s)
{
    gameOverData.clear();
    Q_UINT32 nb;
    s >> nb;
    for (uint i=0; i<nb; i++) {
        ExtScore *score = new ExtScore;
        s >> *score;
        gameOverData.append(score);
    }
}
