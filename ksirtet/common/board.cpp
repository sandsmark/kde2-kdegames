#include "board.h"
#include "board.moc"

#include "gpiece.h"
#include "misc_ui.h"
#include "ai.h"

Board::Board(uint width, uint height, bool graphic, GiftPool *gp,
			 QWidget *parent, const char *name)
: GenericBoard(width, height, graphic, parent, name),
  _giftPool(gp), aiEngine(0), _next(0)
{
	if ( graphic ) {
		_next = new BlockInfo(*sequences);
		setBlockInfo(main, _next);
	}
}

void Board::setBlockSize(uint size)
{
	const GPieceInfo *info = Piece::info();
	_next->resize((info->maxWidth() + 2)*size, (info->maxHeight() + 2)*size);

    GenericBoard::setBlockSize(size);
}

Board::~Board()
{
	if ( graphic() ) setBlockInfo(0, 0);
	delete aiEngine;
	delete _next;
}

void Board::setType(bool _ai)
{
	ASSERT( graphic() );
	if (_ai) {
		if ( aiEngine==0 ) {
			aiEngine = createAI();
			connect(aiEngine, SIGNAL(rotateLeft()),  SLOT(pRotateLeft()));
			connect(aiEngine, SIGNAL(rotateRight()), SLOT(pRotateRight()));
			connect(aiEngine, SIGNAL(moveLeft()),    SLOT(pMoveLeft()));
			connect(aiEngine, SIGNAL(moveRight()),   SLOT(pMoveRight()));
			connect(aiEngine, SIGNAL(dropDown()),    SLOT(pDropDown()));
		}
	} else {
		delete aiEngine;
		aiEngine = 0;
	}
}

void Board::init(const InitData &data)
{
	GenericBoard::init(data);
	randomGarbage.setSeed(data.seed);
}

void Board::stop()
{
    GenericBoard::stop();
	if (aiEngine) aiEngine->stop();
}

void Board::showBoard(bool show)
{
    GenericBoard::showBoard(show);
	showCanvas(_next, show);
}

void Board::unpause()
{
    GenericBoard::unpause();
	if (aiEngine) aiEngine->start(); // eventually restart thinking
}

void Board::updateLevel(uint newLevel)
{
	GenericBoard::updateLevel(newLevel);
	emit updateLevelSignal(newLevel);
	if ( graphic() ) startTimer();
}

void Board::AIConfigChanged()
{
	if (aiEngine) aiEngine->configChanged();
}

/*****************************************************************************/
void Board::pMoveLeft()
{
	if ( state==Normal ) {
		moveLeft();
		main->update();
	}
}

void Board::pMoveRight()
{
	if ( state==Normal ) {
		moveRight();
		main->update();
	}
}

void Board::pOneLineDown()
{
	if ( state==Normal ) {
		oneLineDown();
		main->update();
	}
}

void Board::pRotateLeft()
{
	if ( state==Normal ) {
		rotateLeft();
		main->update();
	}
}

void Board::pRotateRight()
{
	if ( state==Normal ) {
		rotateRight();
		main->update();
	}
}

void Board::pDropDown()
{
	if ( state!=Normal ) return;
	if ( !graphic() ) dropDown();
	else {
		_dropHeight = 0;
		oneLineDown();
		if ( state!=Normal ) return;
		state = DropDown;
		startTimer();
	}
}

void Board::pieceDropped(uint dropHeight)
{
	if ( state==DropDown ) state = Normal;
	else _dropHeight = dropHeight;
	_beforeGlue(TRUE);
}

void Board::_beforeGlue(bool first)
{
	if ( graphic() ) {
		state = (beforeGlue(_dropHeight>=1, first) ? BeforeGlue : Normal);
		if ( state==BeforeGlue ) {
			startTimer();
			return;
		}
	}
	gluePiece();
}

void Board::gluePiece()
{
	GenericTetris::gluePiece();
	_afterGlue();
}

void Board::_afterGlue()
{
	bool b = afterGlue( !graphic() );
	if ( graphic() ) {
		state = (b ? AfterGlue : Normal);
		if ( state==AfterGlue ) {
			computeClearLines();
			startTimer();
			return;
		}
	}
	computeClearLines();
	updateScore(score() + _dropHeight);
	if ( needRemoving() ) {
		_beforeRemove(TRUE);
		return;
	}
	checkGift();
}

void Board::checkGift()
{
	if ( graphic() ) {
		if ( _giftPool->pending() ) {
			if ( putGift(_giftPool->take()) ) _afterGift();
			else gameOver();
			return;
		}
	}
	newPiece();
}

void Board::_afterGift()
{
	ASSERT( graphic() );
	if ( afterGift() ) {
		state = AfterGift;
		startTimer();
	} else {
		state = Normal;
		checkGift();
	}
}

void Board::newPiece()
{
	ASSERT( state==Normal );
	GenericTetris::newPiece();
	if ( graphic() ) {
		main->update();
		_next->update();
		startTimer();
		if (aiEngine) aiEngine->launch(this);
		// else : a human player can think by himself ...
	}
}

bool Board::timeout()
{
    if ( GenericBoard::timeout() ) return true;

	switch (state) {
        case DropDown:   _dropHeight++;
        case Normal:     oneLineDown();        break;
        case BeforeGlue: _beforeGlue(FALSE);   break;
        case AfterGlue:  _afterGlue();         break;
        case AfterGift:  _afterGift();         break;
        default:         ASSERT(false);
	}
	main->update();
    return true;
}

bool Board::startTimer()
{
    if ( GenericBoard::startTimer() ) return true;

	switch (state) {
        case Normal:     timer.start(info.baseTime / (1 + level())); break;
        case DropDown:   timer.start(info.dropDownTime);             break;
        case BeforeGlue: timer.start(info.beforeGlueTime, TRUE);     break;
        case AfterGlue:  timer.start(info.afterGlueTime, TRUE);      break;
        case AfterGift:  timer.start(info.afterGiftTime, TRUE);      break;
        default:         ASSERT(false);
	}
    return true;
}

//-----------------------------------------------------------------------------
bool Board::beforeGlue(bool bump, bool first)
{
	if ( !bump ) return FALSE;
	if (first) loop = 0;
	else loop++;

	float dec = blockHeight();
	switch (loop) {
	case 0:             return TRUE;
	case 1: dec *= -0.2; break;
	case 2: dec *= -0.3; break;
	}
	if (_animations) bumpCurrentPiece((int)dec);

	return ( loop!=3 );
}
