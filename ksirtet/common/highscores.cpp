#include "highscores.h"

#include <qlabel.h>
#include <qlayout.h>

#include <klocale.h>
#include <kapp.h>


//-----------------------------------------------------------------------------
ExtScore::ExtScore(uint score, uint level, uint removed)
    : Score(score)
{
    ASSERT(level);

    addData("level", new ItemBase("1", i18n("Level"), Qt::AlignRight), true,
            level);
    addData("removed lines", new ItemBase("0", removedLabel(), Qt::AlignRight),
            true, removed);
}

//-----------------------------------------------------------------------------
ExtPlayerInfos::ExtPlayerInfos()
{
    if ( !_newPlayer ) return;

    // convert legacy highscores ...
    KConfig *config = kapp->config();
    config->setGroup("High Scores");
    for (uint i=0; i<10; i++) {
        QString name
            = config->readEntry(QString("name%1").arg(i), QString::null);
        if ( name.isNull() ) break;
        if ( name.isEmpty() ) name = i18n("anonymous");
        uint score
            = config->readUnsignedNumEntry(QString("score%1").arg(i), 0);
        uint level
            = config->readUnsignedNumEntry(QString("level%1").arg(i), 1);
        ExtScore es(score, level);
        es.setName(name);
        es.submit(0, false);
    }
}

//-----------------------------------------------------------------------------
ShowMultiplayersScores::ShowMultiplayersScores(const QList<ExtScore> &scores,
                                               QWidget *parent)
: KDialogBase(Plain, i18n("Multiplayers scores"), Close, Close,
			  parent, "show_multiplayers_score", true, true), _scores(scores)
{
    QVBoxLayout *vbox = new QVBoxLayout(plainPage());

    QListView *_list = createList(plainPage());
    vbox->addWidget(_list);

    QListIterator<ExtScore> it(scores);
    addLine(_list, *it.current(), -1, false);
    uint i = 0;
    while ( it.current() ) {
        addLine(_list, *it.current(), i, false);
        i++;
        ++it;
    }

    enableButtonSeparator(false);
}

QString ShowMultiplayersScores::itemText(const ItemBase *item, uint row) const
{
    QListIterator<ExtScore> it(_scores);
    it += row;
    return it.current()->prettyData(item->name());
}

bool ShowMultiplayersScores::showColumn(const ItemBase *item) const
{
    return item->name()!="rank";
}
