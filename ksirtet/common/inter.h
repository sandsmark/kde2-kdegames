#ifndef INTER_H
#define INTER_H

#include "lib/mp_simple_interface.h"
#include "types.h"
#include "field.h"
#include "highscores.h"

const MPGameInfo &gameInfo();

class Interface : public MPSimpleInterface
{
 Q_OBJECT

 public:
	Interface(KAccel *kacc, QWidget *parent = 0, const char *name = 0);

 public slots:
	void configureSettings();
	void configureAI();
    void showHighScores();

 signals:
	void enablePreference(Field::PreferenceType, bool enabled);
	void setBlockSize(uint);
    void settingsChanged();
	void AIConfigChangedSignal();

 private:
	QArray<ClientPlayData> data;
    QList<ExtScore>        gameOverData;

	MPBoard *newBoard(uint);
	uint prev(uint i) const;
	uint next(uint i) const;

	void _init(bool server);
	void _readGameOverData(QDataStream &s);
	void _sendGameOverData(QDataStream &s);
	void _showGameOverData(bool server);
	void _firstInit();
	void _treatInit();
	bool _readPlayData();
	void _sendPlayData();

	void setPreferences();
};

#endif // INTER_H
