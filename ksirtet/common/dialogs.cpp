#include "dialogs.h"
#include "dialogs.moc"

#include <qlabel.h>
#include <qgrid.h>
#include <qlayout.h>

#include <kapp.h>
#include <kseparator.h>
#include <klocale.h>
#include <kiconloader.h>
#include <kmessagebox.h>

#include "defines.h"


const char *OP_GRP         = "Options";
const char *OP_MENUBAR     = "menubar visible";
const char *OP_SHOW_NEXT   = "show next piece";
const char *OP_SHADOW      = "show piece shadow";
const char *OP_ANIMATIONS  = "enable animations";
const char *OP_BLOCK_SIZE  = "block size";
const char *OP_INIT_LEVEL  = "init level";

OptionDialog::OptionDialog(QWidget *parent)
: KDialogBase(IconList, i18n("Configure"), Ok|Cancel, Cancel,
			  parent, "option_dialog", true, true)
{
    // game
    QFrame *page = addPage(i18n("Game"), QString::null,
                           BarIcon("misc", KIcon::SizeLarge));
	QVBoxLayout *top = new QVBoxLayout(page, spacingHint());

    il = new KIntNumInput(readInitLevel(), page);
	il->setRange(1, 20);
	il->setLabel(i18n("Initial level"));
	top->addWidget(il);

	shadow = new QCheckBox(i18n("Show tile's shadow"), page);
	shadow->setChecked(readShadow());
	top->addWidget(shadow);

	next = new QCheckBox(i18n("Show next tile"), page);
	next->setChecked(readShowNext());
	top->addWidget(next);

    top->addStretch(1);
    top->addSpacing(100); // #### hack to have a reasonable height ...

    // appearance
    page = addPage(i18n("Appearance"), QString::null,
                   BarIcon("appearance", KIcon::SizeLarge));
    top = new QVBoxLayout(page, spacingHint());

    bs = new KIntNumInput(readBlockSize(), page);
	bs->setRange(MIN_BLOCK_SIZE, MAX_BLOCK_SIZE);
	bs->setLabel(i18n("Block size"));
	top->addWidget(bs);

	anim = new QCheckBox(i18n("Enable animations"), page);
	anim->setChecked(readAnimations());
	top->addWidget(anim);

    top->addStretch(1);

    // highscores
    highscores = new HighscoresOption(this);
}

KConfig *OptionDialog::config()
{
	KConfig *conf = kapp->config();
	conf->setGroup(OP_GRP);
	return conf;
}

void OptionDialog::accept()
{
    if ( !highscores->accept() ) return;

	KConfig *conf = config();
	conf->writeEntry(OP_SHADOW, shadow->isChecked());
	conf->writeEntry(OP_SHOW_NEXT, next->isChecked());
	conf->writeEntry(OP_ANIMATIONS, anim->isChecked());
	conf->writeEntry(OP_BLOCK_SIZE, bs->value());
	conf->writeEntry(OP_INIT_LEVEL, il->value());

	KDialogBase::accept();
}

bool OptionDialog::readShadow()
{
	return config()->readBoolEntry(OP_SHADOW, TRUE);
}

bool OptionDialog::readShowNext()
{
	return config()->readBoolEntry(OP_SHOW_NEXT, TRUE);
}

bool OptionDialog::readAnimations()
{
	return config()->readBoolEntry(OP_ANIMATIONS, TRUE);
}

bool OptionDialog::readMenuVisible()
{
	return config()->readBoolEntry(OP_MENUBAR, TRUE);
}

void OptionDialog::writeMenuVisible(bool visible)
{
	config()->writeEntry(OP_MENUBAR, visible);
}

uint OptionDialog::readBlockSize()
{
	uint size = config()->readUnsignedNumEntry(OP_BLOCK_SIZE, MIN_BLOCK_SIZE);
	if ( BLOCK_SIZE_MUST_BE_ODD && (size%2)==0 ) size = size - 1;
	return QMIN(QMAX(size, MIN_BLOCK_SIZE), MAX_BLOCK_SIZE);
}

uint OptionDialog::readInitLevel()
{
    uint il = config()->readUnsignedNumEntry(OP_INIT_LEVEL, 1);
	return QMAX(1, il);
}
