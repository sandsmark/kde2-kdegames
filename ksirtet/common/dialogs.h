#ifndef DIALOGS_H
#define DIALOGS_H

#include <qcheckbox.h>

#include <kdialogbase.h>
#include <kconfig.h>
#include <knuminput.h>

#include "highscores.h"


class OptionDialog : public KDialogBase
{
 Q_OBJECT

 public:
	OptionDialog(QWidget *parent);

	static bool readMenuVisible();
	static void writeMenuVisible(bool visible);
	static bool readShadow();
	static bool readShowNext();
	static bool readAnimations();
	static uint readBlockSize();
	static uint readInitLevel();

 private slots:
	void accept();

 private:
	QCheckBox        *shadow, *next, *anim;
	KIntNumInput     *bs, *il;
    HighscoresOption *highscores;

	static KConfig *config();
};

#endif
