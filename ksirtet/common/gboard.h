#ifndef GBOARD_H
#define GBOARD_H

#include <qcanvas.h>
#include <qframe.h>
#include <qtimer.h>

#include "gtetris.h"


class SequenceArray;
class BlockInfo;

class GenericBoard : public QFrame, public GenericTetris
{
 Q_OBJECT

 public:
    struct Info {
        uint baseTime, dropDownTime, beforeGlueTime, afterGlueTime;
        uint beforeRemoveTime, afterRemoveTime, afterGiftTime;
    };

    GenericBoard(uint width, uint height, bool graphical,
                 QWidget *parent, const char *name = 0);
	virtual ~GenericBoard();

	virtual void start();
	void pause();
	virtual void unpause();
	virtual void stop();

	QSize sizeHint() const;
	QSizePolicy sizePolicy() const;

	bool isGameOver() const { return _end; }
	virtual bool needRemoving() = 0;
	virtual void remove() = 0;

	uint blockWidth() const;
	uint blockHeight() const;

	int firstColumnBlock(uint column) const;

	void enableAnimations(bool enabled) { _animations = enabled; }

 public slots:
	virtual void setBlockSize(uint);

 protected slots:
    virtual bool timeout(); // return true if treated

 signals:
	void updatePieceConfigSignal();
    void updateRemovedSignal(int nbRemoved);
    void updateScoreSignal(int score);
	void blockSizeChanged();

 protected:
	// standard methods
	void gameOver()                                       { _end = true; }
	virtual bool beforeRemove(bool /*first*/)             { return false; }
    void _beforeRemove(bool first);
	enum AfterRemoveResult { Done, NeedAfterRemove, NeedRemoving };
	virtual AfterRemoveResult afterRemove(bool /*doAll*/, bool /*first*/)
		                                                  { return Done;  }
    virtual void _afterAfterRemove() { startTimer(); }
	virtual bool startTimer(); // return true if treated

    void updateRemoved(uint addRemoved);
    void updateScore(uint newScore);

    virtual void showBoard(bool show);
    void showCanvas(QCanvas *c, bool show);

    enum BoardState { Normal, DropDown, BeforeGlue, AfterGlue, BeforeRemove,
					  AfterRemove, AfterGift };
    BoardState     state;
	Info           info;
	bool           _animations;
    QTimer         timer;
    SequenceArray *sequences;
	BlockInfo     *main;

 private:
	void _afterRemove(bool first);
	void updatePieceConfig() { emit updatePieceConfigSignal(); }

	bool              _end;
	QCanvasView      *view;
};

#endif
