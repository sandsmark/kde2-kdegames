#include "gboard.h"
#include "gboard.moc"

#include "gpiece.h"


GenericBoard::GenericBoard(uint width, uint height, bool graphic,
                           QWidget *parent, const char *name)
: QFrame(parent, name), GenericTetris(width, height, graphic),
  state(Normal), timer(this), sequences(0), main(0), view(0)
{
	if ( graphic ) {
		sequences = new SequenceArray;
		main = new BlockInfo(*sequences);
		connect(&timer, SIGNAL(timeout()), SLOT(timeout()));
		setFrameStyle( QFrame::Panel | QFrame::Sunken );
		view = new QCanvasView(main, this);
		view->move(frameWidth(), frameWidth());
	}
}

void GenericBoard::setBlockSize(uint size)
{
	ASSERT( graphic() );

	sequences->setBlockSize(size, size);
	main->resize(matrix().width()*size, matrix().height()*size);
	view->resize(main->width()+5, main->height()+5);

	for (uint i=0; i<matrix().width(); i++)
		for (uint j=0; j<firstClearLine(); j++)
			partialMoveBlock(i, j, 0);
	main->update();

	emit blockSizeChanged();
	updateGeometry();
}

GenericBoard::~GenericBoard()
{
	delete main;
	delete sequences;
}

QSize GenericBoard::sizeHint() const
{
	if ( !graphic() ) return QSize();
	int fw = 2* frameWidth();
	return QSize(fw + view->width(), fw + view->height());
}

QSizePolicy GenericBoard::sizePolicy() const
{
	return QSizePolicy( QSizePolicy::Fixed, QSizePolicy::Fixed );
}

void GenericBoard::start()
{
	ASSERT( graphic() );
	_end  = false;
	state = Normal;
	GenericTetris::start(); // NB: the timer is started by updateLevel !
}

void GenericBoard::stop()
{
	ASSERT( graphic() );
	timer.stop();
}

void GenericBoard::pause()
{
	ASSERT( graphic() );
	stop();
	showBoard(false);
}

void GenericBoard::showCanvas(QCanvas *c, bool show)
{
	QCanvasItemList l = c->allItems();
	QCanvasItemList::Iterator it;
	for (it=l.begin(); it!=l.end(); ++it) {
		if (show) (*it)->show();
		else (*it)->hide();
	}
	c->update();
}

void GenericBoard::showBoard(bool show)
{
	showCanvas(main, show);
}

void GenericBoard::unpause()
{
	ASSERT( graphic() );
	showBoard(true);
	startTimer();
}

void GenericBoard::updateRemoved(uint newNbRemoved)
{
	GenericTetris::updateRemoved(newNbRemoved);
    emit updateRemovedSignal(newNbRemoved);
}

void GenericBoard::updateScore(uint newScore)
{
	GenericTetris::updateScore(newScore);
    emit updateScoreSignal(newScore);
}

uint GenericBoard::blockWidth() const
{
	return sequences->blockWidth();
}

uint GenericBoard::blockHeight() const
{
	return sequences->blockHeight();
}

int GenericBoard::firstColumnBlock(uint col) const
{
	for (int j=firstClearLine()-1; j>=0; j--)
		if ( matrix()(col, j)!=0 ) return j;
	return -1;
}

//-----------------------------------------------------------------------------
void GenericBoard::_beforeRemove(bool first)
{
	if ( graphic() ) {
		state = ( beforeRemove(first) ? BeforeRemove : Normal );
		if ( state==BeforeRemove ) {
			startTimer();
			return;
		}
	}
	remove();
	_afterRemove(TRUE);
}

void GenericBoard::_afterRemove(bool first)
{
	AfterRemoveResult r = afterRemove(!graphic(), first);
	switch (r) {
	  case Done:
          state = Normal;
          _afterAfterRemove();
          return;
	  case NeedAfterRemove:
          state = AfterRemove;
          startTimer();
          return;
	  case NeedRemoving:
          _beforeRemove(TRUE);
          return;
	}
}

bool GenericBoard::timeout()
{
	ASSERT( graphic() );
	switch (state) {
        case BeforeRemove: _beforeRemove(FALSE); break;
        case AfterRemove:  _afterRemove(FALSE);  break;
        default:           return false;
	}
	main->update();
    return true;
}

bool GenericBoard::startTimer()
{
	ASSERT( graphic() );
	switch (state) {
        case BeforeRemove: timer.start(info.beforeRemoveTime, TRUE);   break;
        case AfterRemove:  timer.start(info.afterRemoveTime, TRUE);    break;
        default:           return false;
	}
    return true;
}
