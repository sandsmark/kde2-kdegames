#ifndef FE_AI_H
#define FE_AI_H

#include "common/ai.h"

class FEAI : public AI
{
 Q_OBJECT

 public:
    FEAI(uint thinkTime, uint orderTime);

 private:
	Board *createAIBoard();
};

#endif
