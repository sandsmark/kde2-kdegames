#include "ai.h"
#include "ai.moc"

#include <klocale.h>

#include "board.h"


const AIElementInfo REM_BLOCKS
    = { "removed blocks",  50, 0, 100, false, 0, 0, 0, false };

const AIElementInfo OCC_LINES
    = { "occupied lines", 2, 0, 100, false, 0, 0, 0, true };
const AIElementInfo PEAK_TO_PEAK
    = { "peak-to-peak", 0, 0, 100, false, 0, 0, 0, true };
const AIElementInfo HOLES
    = { "holes", 2, 0, 100, false, 0, 0, 0, true };
const AIElementInfo MEAN
    = { "mean height", 0, 0, 100, false, 0, 0, 0, true };
const AIElementInfo SPACES
    = { "spaces", 0, 0, 100, false, 0, 0, 0, true };

FEAI::FEAI(uint thinkTime, uint orderTime)
: AI(thinkTime, orderTime)
{
	add(i18n("Number of removed blocks"), REM_BLOCKS, nbRemoved);
	// add puyos + chained
}

Board *FEAI::createAIBoard()
{
	return new FEBoard(FALSE, 0, 0);
}
