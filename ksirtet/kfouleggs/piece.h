#ifndef FE_PIECE_H
#define FE_PIECE_H

#include "common/gpiece.h"

const uint FE_NB_BLOCKS      = 2;
const uint FE_NB_FORMS       = 1;
const uint FE_NB_TYPES       = 4 * 4;
const uint FE_NB_BLOCK_TYPES = 4 + 1;
const uint FE_NB_BLOCK_MODES = 1+4+6+4+1; // all possible connections

const uint FE_UP    = 1;
const uint FE_RIGHT = 2;
const uint FE_DOWN  = 4;
const uint FE_LEFT  = 8;

typedef struct {
	int i[FE_NB_BLOCKS];
	int j[FE_NB_BLOCKS];
} FEForm;

extern const FEForm FE_FORMS[FE_NB_FORMS];
extern const QColor FE_COLORS[FE_NB_BLOCK_TYPES];

class FEPieceInfo : public GPieceInfo
{
 public:
	FEPieceInfo() {}

	uint nbBlocks() const { return FE_NB_BLOCKS; }
	uint nbForms()  const { return FE_NB_FORMS;  }
	uint nbTypes()  const { return FE_NB_TYPES;  }

	const int *i(uint form) const { return FE_FORMS[form].i; }
	const int *j(uint form) const { return FE_FORMS[form].j; }
	uint value(uint type, uint n) const
		{ return (n%2 ? type/4 : type%4); }
	uint form(uint) const         { return 0; }
	uint nbConfigurations(uint type) const
		{ return ((type%4)==(type/4) ? 2 : 4);}

	QPixmap *drawPixmap(uint blockWidth, uint blockHeight,
						uint blockType, uint blockMode, bool lighted) const;

	uint nbBlockTypes() const { return FE_NB_BLOCK_TYPES; }
	uint nbBlockModes() const { return FE_NB_BLOCK_MODES; }
	uint garbageType() const  { return FE_NB_BLOCK_TYPES-1; }
};

#endif
