#include <klocale.h>

#include "common/main.h"
#include "board.h"
#include "piece.h"
#include "ai.h"
#include "lib/mp_interface.h"


const MPGameInfo GAME_INFO = {
        "003",
        4,
        500,
        TRUE,
        0, 0
};

const MPGameInfo &gameInfo()
{
	return GAME_INFO;
}

QString removedLabel()
{
    return i18n("Removed eggs");
}

Board *createBoard(QWidget *parent)
{
	return new FEBoard(TRUE, parent);
}

GPieceInfo *createPieceInfo()
{
	return new FEPieceInfo;
}

AI *createAI()
{
	return new FEAI(0, 200); // times needs tuning ?
}

const MainData MAIN_DATA = {
    "kfouleggs",
    I18N_NOOP("KFoulEggs"),
    I18N_NOOP("KFoulEggs is a clone of the well-known (at least in Japan)"
			  " PuyoPuyo game."),
    "http://kfouleggs.sourceforge.net"
};

const char *highscoresHost()
{
    return "localhost"; // #### FIXME
}

int main(int argc, char **argv)
{
	return generic_main(argc, argv, MAIN_DATA);
}
