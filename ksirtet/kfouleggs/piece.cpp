#include "piece.h"

#include <math.h>

const FEForm FE_FORMS[FE_NB_FORMS] = {
	{ {  0, -1 },
	  {  0,  0 } }
};

const QColor FE_COLORS[FE_NB_BLOCK_TYPES] = {
	QColor(100, 200, 100),
	QColor(100, 200, 200),
	QColor(200, 100, 100),
	QColor(200, 100, 200),
	QColor(200, 200, 200) // white blocks
};

QPixmap *FEPieceInfo::drawPixmap(uint blockWidth, uint blockHeight,
								 uint blockType, uint blockMode,
								 bool lighted) const
{
	ASSERT( blockWidth==blockHeight ); // drawing code assumes that
	uint w = blockWidth;
	QPixmap *pix = new QPixmap(w, w);
    pix->fill(Qt::black);

	QColor col = FE_COLORS[blockType];
	if (lighted) col = col.light();

	// base mode
	double d = (sqrt(2)-2./3)*w;
	QRect cr = QRect(0, 0, d, d);
	cr.moveCenter(QPoint(w/2, w/2));
	QPainter p(pix);
	p.setBrush(col);
	p.setPen( QPen(Qt::NoPen) );
	p.drawEllipse(cr);

	if ( blockMode==0 ) return pix;

	// special mode
	double a  = w/(3*sqrt(2));
	double ra = 2*w/3+1;
	cr = QRect(0, 0, ra, ra);

	// first drawing with egg color
	if ( blockMode & FE_UP    ) p.drawRect(    0,     0, w, a);
	if ( blockMode & FE_RIGHT ) p.drawRect(w-a+1,     0, a, w);
	if ( blockMode & FE_DOWN  ) p.drawRect(    0, w-a+1, w, a);
	if ( blockMode & FE_LEFT  ) p.drawRect(    0,     0, a, w);

	// second drawing with background color
	p.setBrush(Qt::black);
	if ( (blockMode & FE_UP) || (blockMode & FE_LEFT) ) {
		cr.moveCenter(QPoint(0, 0));
		p.drawEllipse(cr);
	}
	if ( (blockMode & FE_RIGHT) || (blockMode & FE_UP) ) {
		cr.moveCenter(QPoint(w-1, 0));
		p.drawEllipse(cr);
	}
	if ( (blockMode & FE_DOWN) || (blockMode & FE_RIGHT) ) {
		cr.moveCenter(QPoint(w-1, w-1));
		p.drawEllipse(cr);
	}
	if ( (blockMode & FE_LEFT) || (blockMode & FE_DOWN) ) {
		cr.moveCenter(QPoint(0, w-1));
		p.drawEllipse(cr);
	}

	return pix;
}
