#ifndef FE_BOARD_H
#define FE_BOARD_H

#include "common/board.h"


class FEBoard : public Board
{
 Q_OBJECT
 public:
    FEBoard(bool graphic, QWidget *parent, const char *name=0);
	void copy(const GenericTetris &);

	void init(const InitData &);

 private:
	// standard methods
	bool afterGlue(bool doAll);
	AfterRemoveResult afterRemove(bool doAll, bool first);
	bool afterGift() { return afterGlue(FALSE); };
	bool beforeRemove(bool first);
	bool neighbour(uint i, uint j, uint n, uint &ni, uint &nj) const;
	void removeSurroundingGarbage(uint i, uint j);
	void remove();
	bool needRemoving();
	void checkBlock(uint i, uint j, uint value, uint &nb, bool set);
	void treatGroup(uint i, uint j, uint value, uint &nb, bool set);

	// graphic methods
	uint drawCode(uint i, uint j) const;
	void computeNeighbours();

	// Multiplayers methods
	uint gift();
	bool putGift(uint);

	bool fragmentation;
	Matrix<int>  groups;
	QArray<uint> rem;
	uint nbPuyos, chained, giftRest;
};

#endif
